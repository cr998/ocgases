package es.paumancar.ocgases;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.paumancar.ocgases.datasync.Connectivity;
import es.paumancar.ocgases.datasync.DatosEnvio;
import es.paumancar.ocgases.datasync.DbUpdate;
import es.paumancar.ocgases.datasync.FirmaHttpPost;
import es.paumancar.ocgases.datasync.MySQLiteHelperSync;
import es.paumancar.ocgases.datasync.RecuperaFichero;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class ActivityMainMenu extends Activity {
	
	private static final int RC_FIRMA_ACTIVITY = 8001;
	
    public static final String AUTHORITY = "es.paumancar.ocgases.datasync.contentprovider";
    public static final String ACCOUNT_TYPE = "es.paumancar.ocgases.datasync.account";
    public static final String ACCOUNT = "ocgases_account";
    
    Account mAccount;
    //ContentResolver mResolver;
	
	SharedPreferences sharedPref;
	
	private int mInterval = 2000; // 52seconds by default, can be changed later
	private Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		mAccount = CreateSyncAccount(this);

		copyAssetFiles();
		
		ContentResolver.setIsSyncable(mAccount, AUTHORITY, 1);
		ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
		
		if (ContentResolver.getSyncAutomatically(mAccount, AUTHORITY)) {
			//Toast.makeText(getApplicationContext(), mAccount.name, Toast.LENGTH_LONG).show();
		}
		
		ContentResolver.addPeriodicSync(mAccount, AUTHORITY, Bundle.EMPTY, 60);
		// Useless to set less than 60 seconds
		
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		
		PreferenceManager.setDefaultValues(this, R.xml.default_preferences, false);
		
		File ruta_sd = Environment.getExternalStorageDirectory();
		File fileDir = new File(ruta_sd.getPath() ,  "ocgases/pdfs/");
		if (!fileDir.exists()){
			fileDir.mkdirs();
		}
		 
		borraDatosAntiguos();
		borraFicherosAntiguos();
		
		Button botonCompras = (Button) findViewById(R.id.buttonCompras);
		Button botonVentas = (Button) findViewById(R.id.buttonReparto);
		Button botonPreferencias = (Button) findViewById(R.id.buttonPreferencias);
		Button botonRecupera = (Button) findViewById(R.id.buttonRecupera);
		Button botonFirma = (Button) findViewById(R.id.buttonFirma);
		Button botonCodigos = (Button) findViewById(R.id.buttonCodigos);
		Button botonUpdate = (Button) findViewById(R.id.buttonUpdate);
		Button botonLocalizaciones = (Button) findViewById(R.id.buttonLocalizaciones);
		Button botonSalir = (Button) findViewById(R.id.buttonSalir);
		
		
		botonCompras.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentCompras = new Intent(getApplicationContext(), ActivityListadoCompras.class);
				startActivity(intentCompras);
				
			}
		});
		
		
		botonVentas.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentVentas = new Intent(getApplicationContext(), ActivityCartasDePorte.class);
				startActivity(intentVentas);
				
			}
		});
		
		botonPreferencias.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final EditText editTextPassword = new EditText(ActivityMainMenu.this);
				LinearLayout layoutTextos = new LinearLayout(ActivityMainMenu.this);
				LinearLayout.LayoutParams layoutForInner = 
						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
								LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutTextos.setLayoutParams(layoutForInner);
				layoutTextos.addView(editTextPassword);
				LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
				editTextPassword.setLayoutParams(params);
				editTextPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityMainMenu.this);
				
				dialog.setTitle("PASSWORD");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	String textoPassword = editTextPassword.getText().toString(); 
				    	MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
						SQLiteDatabase db = myHelper.getWritableDatabase();
						String passActual = myHelper.recuperaPassword(db);
						db.close();
						myHelper.close();
						if (passActual.equals(textoPassword)){
							Intent intentPreferencias = new Intent(getApplicationContext(), ActivityPreferenciasGenerales.class);
							startActivity(intentPreferencias);
						} else {
							Toast.makeText(getApplicationContext(), "Contraseña incorrecta", Toast.LENGTH_LONG).show();
						}
				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
				
			}
		});
		
		botonFirma.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentFirma = new Intent(getApplicationContext(), ActivityCaptureSignature.class);
				startActivityForResult(intentFirma, RC_FIRMA_ACTIVITY);
			}
		});
		
		
		botonCodigos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentCodigos = new Intent(getApplicationContext(), ActivityCodigosBarrasSueltos.class);
				startActivity(intentCodigos);
				
			}
		});
		
		botonRecupera.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				RecuperaFichero recuperaCartasYAlbaranes = new RecuperaFichero(ActivityMainMenu.this);
				String[] datosCartasYAlbaranses = {"CartaDePorte", "AlbaranVentaCabecera"};
				recuperaCartasYAlbaranes.execute(datosCartasYAlbaranses);
				
				RecuperaFichero recuperaAlbaranesCabecera = new RecuperaFichero(ActivityMainMenu.this);
				String[] datosAlbaranesCabecera = {"AlbaranVentaCabecera"};
				recuperaAlbaranesCabecera.execute(datosAlbaranesCabecera);
				
				RecuperaFichero recuperaAlbaranesLineas = new RecuperaFichero(ActivityMainMenu.this);
				String[] datosAlbaranesLineas = {"AlbaranVentaLineas"};
				recuperaAlbaranesLineas.execute(datosAlbaranesLineas);
			}
		});
		
		botonLocalizaciones.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentLocalizaciones = 
						new Intent(getApplicationContext(), ActivityListaLocalizaciones.class);
				startActivity(intentLocalizaciones);
				
			}
		});
		
		botonSalir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		botonUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			       
		        
		        Bundle settingsBundle = new Bundle();
		        settingsBundle.putBoolean(
		                ContentResolver.SYNC_EXTRAS_MANUAL, true);
		        settingsBundle.putBoolean(
		                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
		        /*
		         * Request the sync for the default account, authority, and
		         * manual sync settings
		         */
		        ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
				
			}
		});
		
	    mHandler = new Handler();
		
	}
	
	
	
	@Override
	protected void onPostResume() {
		super.onPostResume();
	    startRepeatingTask();
	}
	


	@Override
	protected void onStop() {
		super.onStop();
		stopRepeatingTask();
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Bundle extras;

		switch (requestCode)
		{
		case RC_FIRMA_ACTIVITY:
			if (RESULT_OK == resultCode)
			{
				// Gets the Base64 encoded PNG signature graphic.
				extras = data.getExtras();
				String base64SignaturePng = extras.getString(ActivityCaptureSignature.BASE64_SIGNATURE_KEY);
				String numeroAlbaran = extras.getString(ActivityCaptureSignature.NUMERO_ALBARAN_KEY);
				String servertHost = Connectivity.servidorActivo(getApplicationContext());
				
//				if (numeroAlbaran.equals("")){
//					numeroAlbaran = "firma_temp";
//				}

				FirmaHttpPost grabaFirma = new FirmaHttpPost(getApplicationContext(), numeroAlbaran, base64SignaturePng);

				List<String[]> datosPostGrabaFirma = grabaFirma.getPostValues();
				DatosEnvio[] datosAEnviar = new DatosEnvio[1];
				datosAEnviar[0] = new DatosEnvio(getApplicationContext(), servertHost, "GrabaFirma", 
						"POST", numeroAlbaran, null, datosPostGrabaFirma, null);
				MySQLiteHelperSync myHelperSync = new MySQLiteHelperSync(getApplicationContext());
				SQLiteDatabase db =  myHelperSync.getWritableDatabase();
				myHelperSync.insertaDatosEnvio(db, datosAEnviar[0]);
				db.close();
				myHelperSync.close();
				DbUpdate.marcaCodigosEnviados(datosAEnviar[0], getApplicationContext());
				//EnviaDatos enviarDatos = new EnviaDatos();
				//enviarDatos.execute(datosAEnviar);
			}
			break;
		}
	}
	
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        Log.w("OCGASES", "Account Type: " + newAccount.type + "  " + newAccount.name);
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
        	Log.w("OCGASES", "OnAddAccountExplicitly OK");
        } else {
        	Log.w("OCGASES", "OnAddAccountExplicitly error");
        }
        
        return newAccount;
    }
    
    
    Runnable mStatusChecker = new Runnable() {
        @Override 
        public void run() {
              try {
                   updateStatus(); //this function can change value of mInterval.
              } finally {
                   // 100% guarantee that this always happens, even if
                   // your update method throws an exception
                   mHandler.postDelayed(mStatusChecker, mInterval);
              }
        }
    };
    
    void startRepeatingTask() {
        mStatusChecker.run(); 
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
    
    void updateStatus(){
    	TextView textViewEnviaCompras = (TextView) findViewById(R.id.textViewMainMenuEnviosCompras);
		TextView textViewEnviaVentas = (TextView) findViewById(R.id.textViewMainMenuEnviosVentas);
		TextView textViewEnviaFirmas = (TextView) findViewById(R.id.textViewMainMenuEnviosFirmas);
		MySQLiteHelperSync myHelperSync = new MySQLiteHelperSync(getApplicationContext());
		SQLiteDatabase db =  myHelperSync.getWritableDatabase();
		int pendientesCompras = myHelperSync.cuentaDatosPendientesPorTipo(db, "GrabaLineasCompra");
		int pendientesVentas = myHelperSync.cuentaDatosPendientesPorTipo(db, "GrabaLineasVenta");
		int pendientesFirmas = myHelperSync.cuentaDatosPendientesPorTipo(db, "GrabaFirma");
		textViewEnviaCompras.setText("PC:" + String.valueOf(pendientesCompras));
		textViewEnviaVentas.setText("AV:" + String.valueOf(pendientesVentas));
		textViewEnviaFirmas.setText("FR:" + String.valueOf(pendientesFirmas));
		db.close();
		myHelperSync.close();
    }
    
    void borraDatosAntiguos(){
    	String diasActivo = sharedPref.getString(TagsPreferencias.DIAS_GUARDADO_DATOS, "2");
    	MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
		SQLiteDatabase db = myHelper.getWritableDatabase();
		myHelper.borraCodigosAntiguos(db, "llenos", "compra", diasActivo);
		myHelper.borraCodigosAntiguos(db, "vacios", "compra", diasActivo);
		myHelper.borraCodigosAntiguos(db, "llenos", "venta", diasActivo);
		myHelper.borraCodigosAntiguos(db, "vacios", "venta", diasActivo);
		db.close();
		myHelper.close();
    }
    
    void borraFicherosAntiguos(){
    	long nowInMillis = Calendar.getInstance().getTimeInMillis();
    	long gapInMillis = 
    			1000*60*60*24*Long.valueOf(sharedPref.getString(TagsPreferencias.DIAS_GUARDADO_DATOS, "2"));
    	long fechaBorradoMillis = nowInMillis - gapInMillis;
    	SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
    	File ruta_sd = Environment.getExternalStorageDirectory();
		File fileDir = new File(ruta_sd.getPath() ,  "ocgases/pdfs/");
    	List<File> pdfFiles = getListFiles(fileDir);
    	for (File file : pdfFiles) {
			String fileName = file.getName();
			String stringDate = fileName.split("_")[1].substring(0, 14);
			long millisecondsFile = 0;
			try {
	    	    Date d = sdf.parse(stringDate);
	    	    millisecondsFile = d.getTime();
	    	    if (millisecondsFile < fechaBorradoMillis){
	    	    	file.delete();
	    	    }
	    	} catch (ParseException e) {
	    	    e.printStackTrace();
	    	}
		}
    	
    }
    
    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {
                if(file.getName().endsWith(".csv")){
                    inFiles.add(file);
                }
            }
        }
        return inFiles;
    }
    
	private void copyAssetFiles() 
	{
		InputStream input = null;			
		OutputStream output = null;
		// Copy the asset files we delivered with the application to a location
		// where the LinePrinter can access them.
		try
		{
			AssetManager assetManager = getApplicationContext().getAssets();
			String[] files = { "printer_profiles.JSON", 
					"LOGOH.jpg",
					"gasServei.jpg",
					"LOGO2.jpg",
					"ocgases_logo2.bmp"};
			
			for (String filename : files)
			{
				input = assetManager.open(filename);			
				File outputFile = new File(getApplicationContext().getExternalFilesDir(null), filename);
			
				output = new FileOutputStream(outputFile);
						
				byte[] buf = new byte[1024];			
				int len;			
				while ((len = input.read(buf)) > 0)			
				{				
					output.write(buf, 0, len);			
				}						
				input.close();			
				input = null;
						
				output.flush();			
				output.close();			
				output = null;
			}
		}
		catch (Exception ex)
		{
				
		}
		finally
		{
			try
			{
				if (input != null)
				{
					input.close();
					input = null;
				}
				
				if (output != null)
				{
					output.close();
					output = null;
				}
			}
			catch (IOException e){}
		}
	}
}
