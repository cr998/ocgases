package es.paumancar.ocgases;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import es.paumancar.ocgases.datasync.Connectivity;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityLocalizacion extends FragmentActivity implements OnMapReadyCallback {

	GoogleMap myMap;
	double latitud;
	double longitud;
	String latitudString;
	String longitudString;
	String[] datosLocalizacion;
	Marker localizacionPartner;
	Marker nuevaLocalizacionPartner;
	boolean hayLoc;
	
	TextView textoLatitudActual;
	TextView textoLongitudActual;
	TextView textoLatitudNueva;
	TextView textoLongitudNueva;
	TextView textoDistancia;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_localizacion);
		
		
		TextView textoNombre = (TextView)  findViewById(R.id.textLocalizacionNombre);
		TextView textoDireccion = (TextView)  findViewById(R.id.textLocalizacionDireccion);
		TextView textoTelefono = (TextView)  findViewById(R.id.textLocalizacionTelefono);
		TextView textoMail = (TextView)  findViewById(R.id.textLocalizacionMail);
		
		textoLatitudActual = (TextView) findViewById(R.id.textLocalizacionLatitudActual);
		textoLongitudActual = (TextView) findViewById(R.id.textLocalizacionLongitudActual);
		textoLatitudNueva = (TextView) findViewById(R.id.textLocalizacionLatitudNueva);
		textoLongitudNueva = (TextView) findViewById(R.id.textLocalizacionLongitudNueva);
		textoDistancia = (TextView) findViewById(R.id.textLocalizacionDistancia);
		
		Button botonActualizaLocalizacion = (Button) findViewById(R.id.buttonLocalizacionActualizar);
		
		Intent intentReceived = getIntent();
		datosLocalizacion = intentReceived.getStringArrayExtra("datosLocalizacion");
		if (isValidDouble(datosLocalizacion[8]) & isValidDouble(datosLocalizacion[9])){
			latitudString = datosLocalizacion[8];
			longitudString = datosLocalizacion[9];
		} else {
			latitudString = "";
			longitudString = "";
		}
		
		hayLoc = ((!latitudString.equals("") & !longitudString.equals(""))?true:false);
		
		textoNombre.setText(datosLocalizacion[2]);
		textoDireccion.setText(datosLocalizacion[3]);
		textoTelefono.setText(datosLocalizacion[4]);
		textoMail.setText(datosLocalizacion[5]);

		textoLatitudActual.setText(latitudString);
		textoLongitudActual.setText(longitudString);
		
		
		
		
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()		    
				.findFragmentById(R.id.fragmentLocalizacionMap);
		mapFragment.getMapAsync(this);
		
		botonActualizaLocalizacion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String latitudString = textoLatitudNueva.getText().toString();
				String longitudString = textoLongitudNueva.getText().toString();
				if (latitudString.equals("")){
					latitudString = String.format("%f",latitud);
				}
				if (longitudString.equals("")){
					longitudString = String.format("%f",longitud);
				}
				InsertaLocalizaciones insertLoc = new InsertaLocalizaciones(datosLocalizacion[10], 
						latitudString, longitudString);
				insertLoc.execute("");
			}
		});
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onMapReady(GoogleMap map) {
		myMap = map;
		myMap.setMyLocationEnabled(true);
		
		
		map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			@Override
			public void onMyLocationChange(Location location) {
				latitud = location.getLatitude();
				longitud = location.getLongitude();
				LatLng myLocation = new LatLng(latitud, longitud);
				if (!hayLoc){
					//textCoordsGps.setText(String.format("lat: %f  long: %f",latitud, longitud));
					myMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
					myMap.animateCamera(CameraUpdateFactory.zoomTo(16), 5000, null);
					textoLatitudNueva.setText(String.format("%f",latitud));
					textoLongitudNueva.setText(String.format("%f", longitud));
				}
			}
		});

		
		if (hayLoc){
			latitud = Double.valueOf(latitudString);
			longitud = Double.valueOf(longitudString);
			LatLng partnerLocation = new LatLng(latitud, longitud);
	        myMap.moveCamera(CameraUpdateFactory.newLatLng(partnerLocation));
	        myMap.animateCamera(CameraUpdateFactory.zoomTo(16), 5000, null);
	        localizacionPartner = myMap.addMarker(new MarkerOptions().position(partnerLocation)
	        .title(datosLocalizacion[2]));
		} 
	    
	     myMap.setOnMapClickListener(new OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng point) {
				String textoBoton = String.valueOf(point.latitude) + ", " + String.valueOf(point.longitude);
				
			}
		});
	     
	     myMap.setOnMapLongClickListener(new OnMapLongClickListener() {
			
			@Override
			public void onMapLongClick(LatLng point) {
				hayLoc = true;
				if (nuevaLocalizacionPartner == null) {
					nuevaLocalizacionPartner = myMap.addMarker(new MarkerOptions().position(point)
					        .title("Nueva loc.: " + datosLocalizacion[2])
					        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
				} else {
					nuevaLocalizacionPartner.setPosition(point);
				}
				float latitud = (float) point.latitude;
				float longitud = (float) point.longitude;
				textoLatitudNueva.setText(String.format("%f",latitud));
				textoLongitudNueva.setText(String.format("%f", longitud));
			}
		});
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			return rootView;
		}
	}
	
	class InsertaLocalizaciones extends AsyncTask<String, String, String> {
		
		String c_bpartner_location_id = "";
		String latitud = "";
		String longitud = "";
		String direccionIP;
		
		public InsertaLocalizaciones(String c_bpartner_location_id, String latitud, String longitud) {
			super();
			this.c_bpartner_location_id = c_bpartner_location_id;
			this.latitud = latitud;
			this.longitud = longitud;
		}

		@Override
		protected String doInBackground(String... params) {
			String resultado = "Error";
			direccionIP = Connectivity.servidorActivo(getApplicationContext());
			InputStream in = new BufferedInputStream(null);
			
			HttpURLConnection conn = null;
			String direccionPost = direccionIP + "/rest/source/?object=GrabaLatitudLongitud&action=custom";
			URL url = null;
			try {
			url = new URL(direccionPost);
			} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}

		    try {
		    	if (url != null) {
		    	   conn = (HttpURLConnection)url.openConnection();
		    	   conn.setReadTimeout(10000);
		    	   conn.setConnectTimeout(15000);
		    	   conn.setRequestMethod("POST");
		    	   conn.setDoInput(true);
		    	   conn.setDoOutput(true);

		    	   OutputStream os = conn.getOutputStream();
		    	   BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		           StringBuilder sb = new StringBuilder();
		           sb.append(URLEncoder.encode("c_bpartner_location_id", "UTF-8"));
		           sb.append("=");
		           sb.append(URLEncoder.encode(c_bpartner_location_id, "UTF-8"));
		           sb.append("&");
		           sb.append(URLEncoder.encode("Latitud", "UTF-8"));
		           sb.append("=");
		           sb.append(URLEncoder.encode(latitud.replace(",", "."), "UTF-8"));
		           sb.append("&");
		           sb.append(URLEncoder.encode("Longitud", "UTF-8"));
		           sb.append("=");
		           sb.append(URLEncoder.encode(longitud.replace(",", "."), "UTF-8"));
		           writer.write(sb.toString());
		    	   writer.flush();
		    	   writer.close();
		    	   os.close();
		    	}

		        int statusCode = conn.getResponseCode();
		        if(statusCode == 200) {
		        	resultado = "Correcto";
		        } else {
		        	InputStream is = conn.getErrorStream();
		        	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		        	StringBuilder sb = new StringBuilder();
	        	    String line = null;
	        	    try {
	        	        while ((line = reader.readLine()) != null) {
	        	            sb.append(line).append('\n');
	        	        }
	        	    } catch (IOException e) {
	        	        e.printStackTrace();
	        	    } finally {
	        	        try {
	        	            is.close();
	        	        } catch (IOException e) {
	        	            e.printStackTrace();
	        	        }
	        	    }
		        }
		    } catch (Exception e) {
				Log.e("OCGASES", e.getMessage());
			} 
			
			return resultado;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result.equals("Correcto")){
				Toast.makeText(getApplicationContext(), 
						"Coordenadas actualizadas correctamente", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(), 
						"Error al actualizar coordenadas", Toast.LENGTH_LONG).show();
			}
		}
		
	}
	
   static Boolean isValidDouble(String value) {
	    try {
	        Double val = Double.valueOf(value);
	        if (val != null)
	            return true;
	        else
	            return false;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}

}
