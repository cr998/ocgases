package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterCodigosPerdidos extends BaseAdapter {

    Context context;
    List<String[]> data;
    String[] cabecerasColumnas;
    private static LayoutInflater inflater = null;
    
    
    public AdapterCodigosPerdidos(Context context, List<String[]> data) {
        this.context = context;
        if (data != null) {
        	this.data = data;
        } else {
        	this.data = new ArrayList<String[]>();
        }
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

	@Override
	public int getCount() {
		return data.size();
	}


    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_codigos_perdidos, null);
        TextView textAlbaran = (TextView) vi.findViewById(R.id.rowTextCodigosPerdidosAlbaran);
        TextView textFecha = (TextView) vi.findViewById(R.id.rowTextCodigosPerdidosFecha);
        TextView textLinea = (TextView) vi.findViewById(R.id.rowTextCodigosPerdidosLinea);
        TextView textDetailCodigosLlenos = (TextView) vi.findViewById(R.id.rowTextCodigosPerdidosLlenos);
        TextView textDetailCodigosVacios = (TextView) vi.findViewById(R.id.rowTextCodigosPerdidosVacios);
        final int positionAux = position;
    	
        vi.setOnClickListener(new OnClickListener() {
        	String[] cabeceraProv = data.get(positionAux);
			@Override
			public void onClick(View v) {
				//TODO
			}
		});
        String fechaCompleta = data.get(position)[3];
        String fechaFormateada = fechaCompleta.substring(6, 8) + "/" + fechaCompleta.substring(4, 6) +
        		"/" + fechaCompleta.substring(0, 4) + 
        		"  " + fechaCompleta.substring(8, 10) + 
        		":" + fechaCompleta.substring(10, 12) + ":" + fechaCompleta.substring(12, 14);
        textAlbaran.setText(data.get(position)[2]);
        textLinea.setText(data.get(position)[0] + "-" +data.get(position)[1]);
        textFecha.setText(fechaFormateada);
        textDetailCodigosLlenos.setText("Llenos:\n" + data.get(position)[4]);
        textDetailCodigosVacios.setText("Vacios:\n" + data.get(position)[5]);
        textAlbaran.setTextColor(Color.BLACK);
        textFecha.setTextColor(Color.BLACK);
        textLinea.setTextColor(Color.BLACK);
        textDetailCodigosLlenos.setTextColor(Color.BLACK);
        textDetailCodigosVacios.setTextColor(Color.BLACK);
        return vi;
	}

}


