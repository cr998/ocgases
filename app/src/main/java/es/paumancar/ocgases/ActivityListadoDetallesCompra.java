package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import es.paumancar.ocgases.datasync.ComprasHttpPost;
import es.paumancar.ocgases.datasync.Connectivity;
import es.paumancar.ocgases.datasync.DatosEnvio;
import es.paumancar.ocgases.datasync.DbUpdate;
import es.paumancar.ocgases.datasync.MySQLiteHelperSync;
import es.paumancar.ocgases.datasync.VentasHttpPost;
import es.paumancar.ocgases.reports.HtmlReport;
import es.paumancar.ocgases.reports.IntermecPrint;
import es.paumancar.ocgases.reports.ReportListLines;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class ActivityListadoDetallesCompra extends Activity {

		SharedPreferences sharedPref;
	   ListView listviewDetallesCompras;
	   String[][] stringAdapterCompras = { {"HEADER1", "data1"},
    		{"HEADER2", "data2"}, {"HEADER3", "data3"} };
	   
	   String[] valoresCabeceraCompra;
	   String[] stringColumnasCompra;
	   
       String etiquetaPrincipal = "PedidoCompraLineas";
       String[] etiquetasDatos = {
    		  "c_order_id",
    		  "c_orderline_id",
    		  "m_product_id",
    		  "qtyordered",
    		  "qtyreturn",
    		  "value",
    		  "name",
    		  "llenas",
    		  "vacias",
    		  "m_attributeset_id"
       };
       TextView textoDetalleLineaCompra;

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.detalles_compras_layout);
	        
	        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
	        textoDetalleLineaCompra = (TextView) findViewById(R.id.textViewDetallesDeComprasDescripcion);
	        
	        listviewDetallesCompras = (ListView) findViewById(R.id.listviewDetallesCompras);
	     
	        Intent intentReceived = getIntent();
	        

	        int filtroId = 0;
	        		// indica la posición del filtro en etiquetasDatos 
         					// Sin filtro -1
	        int nextFiltroId = 0;
	        String valorFiltro = intentReceived.getStringExtra("valorFiltro"); 
	        stringColumnasCompra = intentReceived.getStringArrayExtra("valoresColumnas");
	        valoresCabeceraCompra = intentReceived.getStringArrayExtra("valoresCabecera");
	        
	        Class<?> NextClass = ActivityCodigosBarras.class;
	        //Intent intentNextActivity = nenumeroAlbaranw Intent(getApplicationContext(), CodigosBarrasCompras.class);
	        Context contextActivity = ActivityListadoDetallesCompra.this;
	        Context contextApp = getApplicationContext();
	        String direccionPuertoServidor = Connectivity.servidorActivo(getApplicationContext());
	        DatosLista datosListaCompra = 
	        		new DatosLista(contextApp, 
	        				listviewDetallesCompras, etiquetaPrincipal, etiquetasDatos, 
	        				NextClass, filtroId, valorFiltro, nextFiltroId,
	        				direccionPuertoServidor, 1, textoDetalleLineaCompra, ""); 
	        
	        RecuperaDatos stringDatosCompra = new RecuperaDatos(contextActivity, contextApp);
	        stringDatosCompra.execute(datosListaCompra);
	        
	        
	        Button botonEnviaCompra = (Button) findViewById(R.id.buttonEnviaCompra);
	        botonEnviaCompra.setOnClickListener(new OnClickListener() {
				public void onClick(View view)
				{
					AdapterCompraLineas adapterLineasCompra = 
							(AdapterCompraLineas) listviewDetallesCompras.getAdapter();
					List<String[]> listaLineasDatos = adapterLineasCompra.getData();
					List<String> listaLineas = new ArrayList<String>();
					for (String[] stringLinCod : listaLineasDatos) {
						listaLineas.add(stringLinCod[1]);
					}
					String servertHost = Connectivity.servidorActivo(getApplicationContext());
					String headerId = valoresCabeceraCompra[indiceArray(stringColumnasCompra, "c_order_id")];
					String partnerId = valoresCabeceraCompra[indiceArray(stringColumnasCompra, "c_bpartner_id")];
					//ReportListLines listaLineasReport = new ReportListLines(getApplicationContext(), 
					//		stringColumnasCompra,valoresCabeceraCompra,
					//		etiquetasDatos, listaLineasDatos);
					//HtmlReport htmlReport = new HtmlReport(listaLineasReport.getLinesReport());
					String numeroCompra = 
							valoresCabeceraCompra[indiceArray(stringColumnasCompra, "documentno")];

					ComprasHttpPost PostGrabaLineasCompra = new ComprasHttpPost(getApplicationContext(), 
						headerId, partnerId, listaLineas, numeroCompra);
					List<String[]> datosPostGrabaLineasCompra = PostGrabaLineasCompra.getPostValues();
					DatosEnvio[] datosAEnviar = new DatosEnvio[1];
					datosAEnviar[0] = new DatosEnvio(getApplicationContext(), servertHost, "GrabaLineasCompra", 
							"POST", "", listaLineas, datosPostGrabaLineasCompra, null);
					
					MySQLiteHelperSync myHelperSync = new MySQLiteHelperSync(getApplicationContext());
					SQLiteDatabase db =  myHelperSync.getWritableDatabase();
					myHelperSync.insertaDatosEnvio(db, datosAEnviar[0]);
					db.close();
					myHelperSync.close();
					DbUpdate.marcaCodigosEnviados(datosAEnviar[0], getApplicationContext());
					//TODO Notificar al SyncAdapter

					finish();
				}
			});
	    }
	    
		@Override
		protected void onPostResume() {
			super.onPostResume();
			AdapterCompraLineas adapterLineas = (AdapterCompraLineas) listviewDetallesCompras.getAdapter();
			if (adapterLineas != null){
				adapterLineas.notifyDataSetChanged();
			}
		}
		
		private int indiceArray(String[] stringCabeceras, String valorCabecera){
			int indice = 0;
			for (int i = 0; i < stringCabeceras.length; i++) {
				if (stringCabeceras[i].equals(valorCabecera)){
					indice = i;
					break;
				}
			}
			return indice;
		}
	
	
}
