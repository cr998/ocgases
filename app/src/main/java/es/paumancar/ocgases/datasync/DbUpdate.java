package es.paumancar.ocgases.datasync;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import es.paumancar.ocgases.MySQLiteHelper;

public class DbUpdate {


	
	public static  void marcaCodigosEnviados(DatosEnvio datos, Context context) {

		String tablaEnvio = datos.getTablaEnvio();
		List<String> lineas = datos.getLineas();
		
		if (tablaEnvio.equals("GrabaLineasVenta")) {
	    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
	    	SQLiteDatabase db = myHelper.getWritableDatabase();
	    	for (String stringlinea : lineas) {
	        	myHelper.marcaCodigosLlenosEnviados(db, stringlinea, datos.getDocumento(), "venta");
	        	myHelper.marcaCodigosVaciosEnviados(db, stringlinea, datos.getDocumento(), "venta");
			}
	    	db.close();
	    	myHelper.close();
	    	return;
		}
		
		if (tablaEnvio.equals("GrabaLineasCompra")) {
	    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
	    	SQLiteDatabase db = myHelper.getWritableDatabase();
	    	for (String stringlinea : lineas) {
	        	myHelper.marcaCodigosLlenosEnviados(db, stringlinea, "", "compra");
	        	myHelper.marcaCodigosVaciosEnviados(db, stringlinea, "", "compra");
			}
	    	db.close();
	    	myHelper.close();
	    	return;
		}
		
		if (tablaEnvio.equals("NuevosCodigos")){
			MySQLiteHelper myHelper = new MySQLiteHelper(context);
	    	SQLiteDatabase db = myHelper.getWritableDatabase();
	        myHelper.marcaCodigosLlenosEnviados(db, "codigos_sueltos", "", "provisional");
	        myHelper.marcaCodigosVaciosEnviados(db, "codigos_sueltos", "", "provisional");
	    	db.close();
	    	myHelper.close();
	    	return;
		}
	}

}
