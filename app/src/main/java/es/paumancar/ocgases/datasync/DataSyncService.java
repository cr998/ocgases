package es.paumancar.ocgases.datasync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class DataSyncService extends Service {

    // Storage for an instance of the sync adapter
    private static DataSyncAdapter dataSyncAdapter = null;
    // Object to use as a thread-safe lock
    private static final Object dataSyncAdapterLock = new Object();
 
    @Override
    public void onCreate() {
    	Log.w("OCGASES", "Creating DataSyncService");
        synchronized (dataSyncAdapterLock) {
            if (dataSyncAdapter == null) {
                dataSyncAdapter = new DataSyncAdapter(getApplicationContext(), true);
            }
        }
    }


	@Override
	public IBinder onBind(Intent intent) {
		return dataSyncAdapter.getSyncAdapterBinder();
	}

}
