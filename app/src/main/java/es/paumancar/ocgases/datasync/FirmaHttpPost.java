package es.paumancar.ocgases.datasync;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;

public class FirmaHttpPost {
	Context context;
	String[] numeroAlbaran = {"numero", ""};
	String[] latLong = {"latLong", ""};
	String[] firma = {"firma", ""};

	List<String[]> postValues = new ArrayList<String[]>();

//			&numero=CR2-001313
//			&firma=%2F9j%2F4AAQ
	
	public FirmaHttpPost(Context context, String numeroAlbaran, String firma) {
		super();
		this.context = context;
		this.numeroAlbaran[1] = numeroAlbaran;
		//this.latLong[1] = latLong;
		this.firma[1] = firma;

		postValues.add(this.numeroAlbaran);
		//postValues.add(this.latLong);
		postValues.add(this.firma);
	}


	public List<String[]> getPostValues() {
		return postValues;
	}

	
}