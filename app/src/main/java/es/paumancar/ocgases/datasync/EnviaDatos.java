package es.paumancar.ocgases.datasync;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import es.paumancar.ocgases.MySQLiteHelper;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class EnviaDatos extends AsyncTask<DatosEnvio, String, String> {
	Context context;
	String direccionIP;
	String tablaEnvio;
	String metodo;
	List<String[]> paramsPost;
	List<String> lineas;
	ProgressDialog dialogoProgreso;
	String documento;
	
	@Override
	protected void onPreExecute() {
		Log.w("OCGASES", "Comienza a enviar datos");
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(DatosEnvio... datos) {
		context = datos[0].getContext();
		direccionIP = datos[0].getHostServidor();
		tablaEnvio = datos[0].getTablaEnvio();
		metodo = datos[0].getMetodo();
		paramsPost = datos[0].getDatosPost();
		lineas = datos[0].getLineas();
		documento = datos[0].getDocumento();
		
		String resultado = "";
		
		HttpURLConnection conn = null;
		String direccion = direccionIP + "/rest/source/?object=" + tablaEnvio + "&action=custom";
		URL url = null;
		try {
			url = new URL(direccion);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    try {
	    	if (url != null) {
	    	   conn = (HttpURLConnection)url.openConnection();
	    	   conn.setReadTimeout(10000);
	    	   conn.setConnectTimeout(15000);
	    	   conn.setRequestMethod(metodo);
	    	   conn.setDoInput(true);
	    	   conn.setDoOutput(true);

	    	   OutputStream os = conn.getOutputStream();
	    	   BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
	    	   writer.write(getQuery(paramsPost));
	    	   writer.flush();
	    	   writer.close();
	    	   os.close();
	    	}

	        int statusCode = conn.getResponseCode();
	        resultado = String.valueOf(statusCode);
	        if(statusCode == 200) {
	        	marcaCodigosEnviados();
	        	resultado = "Correcto";
	        } else {
	        	InputStream is = conn.getErrorStream();
	        	 BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	        	    StringBuilder sb = new StringBuilder();

	        	    String line = null;
	        	    try {
	        	        while ((line = reader.readLine()) != null) {
	        	            sb.append(line).append('\n');
	        	        }
	        	    } catch (IOException e) {
	        	        e.printStackTrace();
	        	    } finally {
	        	        try {
	        	            is.close();
	        	        } catch (IOException e) {
	        	            e.printStackTrace();
	        	        }
	        	    }
	        	    resultado = sb.toString();
	        }
	    }
	    catch (Exception e) {
	        Log.e("OCGASES", e.getMessage());
	        resultado = e.getMessage();
	    }
	    finally {
	        conn.disconnect();
	    }

		return resultado;
	}



	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		Log.w("OCGASES", "On Post execute " + result);
		Toast.makeText(context, "Estado envío datos: " + result, Toast.LENGTH_LONG).show();
	}
	
	
	private String getQuery(List<String[]> params) throws UnsupportedEncodingException
	{
	    StringBuilder result = new StringBuilder();
	    boolean first = true;

	    for (String[] pair : params)
	    {
	        if (first)
	            first = false;
	        else
	            result.append("&");
	        	result.append(URLEncoder.encode(pair[0], "UTF-8"));
	        	result.append("=");
	        	result.append(URLEncoder.encode(pair[1], "UTF-8"));
	    }

	    return result.toString();
	}
	
	private void marcaCodigosEnviados() {
		if (tablaEnvio.equals("GrabaLineasVenta")) {
	    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
	    	SQLiteDatabase db = myHelper.getWritableDatabase();
	    	for (String stringlinea : lineas) {
	        	myHelper.marcaCodigosLlenosEnviados(db, stringlinea, documento, "venta");
	        	myHelper.marcaCodigosVaciosEnviados(db, stringlinea, documento, "venta");
			}
	    	db.close();
	    	myHelper.close();
	    	return;
		}
		if (tablaEnvio.equals("NuevosCodigos")){
			MySQLiteHelper myHelper = new MySQLiteHelper(context);
	    	SQLiteDatabase db = myHelper.getWritableDatabase();
	        myHelper.marcaCodigosLlenosEnviados(db, "codigos_sueltos", "", "provisional");
	        myHelper.marcaCodigosVaciosEnviados(db, "codigos_sueltos", "", "provisional");
	    	db.close();
	    	myHelper.close();
	    	return;
		}
	}

}
