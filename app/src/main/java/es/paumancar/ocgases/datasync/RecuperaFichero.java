package es.paumancar.ocgases.datasync;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import es.paumancar.ocgases.MySQLiteHelper;
import es.paumancar.ocgases.XMLParserPedidoCompraCabecera;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class RecuperaFichero extends AsyncTask<String, String, boolean[]> {
	
	SharedPreferences sharedPref;
	private Context contexto;
	
	ProgressDialog dialogProgreso;
	

	boolean actualizaTablaPortes = false;
	boolean actualizaReglasPortes = false;

	public RecuperaFichero(Context contexto) {
		super();
		this.contexto = contexto;
	}


	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialogProgreso = ProgressDialog.show(contexto, "", 
	            "Descargando datos ...", true);
		sharedPref = PreferenceManager.getDefaultSharedPreferences(contexto);
		
	}

	
	@Override
	protected boolean[] doInBackground(String... nombresFichero) {
		boolean[] respuestaCorrecta = {false};
		String direccionIP = Connectivity.servidorActivo(contexto);
		String ficheroString = "";
		String listaFicheros = "";
		for (int i = 0; i < nombresFichero.length; i++) {
			ficheroString = ficheroString + nombresFichero[i];
			listaFicheros = listaFicheros + nombresFichero[i];
			if (i < nombresFichero.length - 1){
				ficheroString = ficheroString + "_";
				listaFicheros = listaFicheros + ",";
			}
		}
		
		InputStream inputStream = new BufferedInputStream(null);
		
		HttpURLConnection con = null;
		String direccionGet = direccionIP + "/rest/source/?object=" + 
		    listaFicheros +  "&action=download";
		URL url = null;
		try {
			url = new URL(direccionGet);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Log.w("OCGASES", "Comienza conexión" + direccionGet);
		
	       try {
		    	
	    	   if (url != null) {
		    	   con = (HttpURLConnection)url.openConnection();
	    		   con.setConnectTimeout(15000);
	    		   con.setReadTimeout(15000);
	    	   }

	           int statusCode = con.getResponseCode();
	           Log.w("OCGASES", "Respuesta conexión" + statusCode);
	            if(statusCode == 200) {

	                inputStream = new BufferedInputStream(con.getInputStream());
	                String saveFilePath = 
	                		contexto.getFilesDir().getAbsolutePath() + "/" + ficheroString + ".xml";
	                
	                File myFile = new File(saveFilePath);
	                FileOutputStream outputStream = new FileOutputStream(myFile);
	     
	                int bytesRead = -1;
	                byte[] buffer = new byte[1024];
	                while ((bytesRead = inputStream.read(buffer)) != -1) {
	                    outputStream.write(buffer, 0, bytesRead);
	                }
	     
	                outputStream.close();
	                inputStream.close();
	                if (listaFicheros.equals("AlbaranVentaLineas")) {
	                	MySQLiteHelper myHelper = new MySQLiteHelper(contexto);
	                    SQLiteDatabase db = myHelper.getWritableDatabase();
	                    myHelper.borraTablaLineasAlbaranActivas(db);
	            		myHelper.borraCodigosAntiguos(db, "llenos", "venta", "-1");
	            		myHelper.borraCodigosAntiguos(db, "vacios", "venta", "-1");
	                    db.close();
	                    myHelper.close();
	                }
	                if (listaFicheros.equals("TablaPortes")){
	                	actualizaTablaPortes = true;
	                }
	                if (listaFicheros.equals("ReglasPortes")){
	                	actualizaReglasPortes = true;
	                }
	                
	                respuestaCorrecta[0] = true;
	            }

	        } catch (Exception e) {
	            Log.e("OCGASES", e.getMessage());

	        }finally {
	            con.disconnect();
	        }

			
		return respuestaCorrecta;
	}


	@Override
	protected void onPostExecute(boolean[] result) {
		dialogProgreso.cancel();
		super.onPostExecute(result);
		boolean respuestaCorrecta = false;
		if (result[0]){
			respuestaCorrecta = true;
			Toast.makeText(contexto, "Datos descargados correctamente", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(contexto, "Error al descargar datos", Toast.LENGTH_SHORT).show();
		}
		if (actualizaTablaPortes & respuestaCorrecta) {
			MySQLiteHelper myHelper = new MySQLiteHelper(contexto);
			SQLiteDatabase db =  myHelper.getWritableDatabase();
			String[] etiquetasTablaPortes = {
					"m_freightcostrule_id", 
			    	"ad_client_id", 
			    	"ad_org_id", 
			    	"isactive", 
			    	"created", 
			    	"createdby", 
			    	"updated", 
			    	"updatedby", 
			    	"description", 
			    	"value", 
			    	"name"
			};
			myHelper.actualizaPortes(db, recuperaDatosFicheroPortes("TablaPortes", etiquetasTablaPortes));
			db.close();
			myHelper.close();	
		}
		if (actualizaReglasPortes & respuestaCorrecta) {
			MySQLiteHelper myHelper = new MySQLiteHelper(contexto);
			SQLiteDatabase db =  myHelper.getWritableDatabase();
			String[] etiquetasReglasPortes = {
					"m_freightcostruleline_id", 
			    	"ad_client_id", 
			    	"ad_org_id", 
			    	"isactive", 
			    	"created", 
			    	"createdby", 
			    	"updated", 
			    	"updatedby", 
			    	"description", 
			    	"fromnumber", 
			    	"tonumber", 
			    	"isbottlecontrolled", 
			    	"isoneinstance", 
			    	"freightamt", 
			    	"m_freightcostrule_id"
			};
			myHelper.actualizaReglasPortes(db, recuperaDatosFicheroPortes("ReglasPortes", etiquetasReglasPortes));
			db.close();
			myHelper.close();	
		}
	}
	
	private List<String[]> recuperaDatosFicheroPortes(String elementoRecupera, String[] etiquetasRecupera){
		List<String[]> resultado = new ArrayList<String[]>();

		String ficheroString = contexto.getFilesDir().getAbsolutePath() + "/" 
				+ elementoRecupera + ".xml";

	       try {
                File ficheroXml = new File(ficheroString);
                InputStream in = new BufferedInputStream(new FileInputStream(ficheroXml));
                
                String[][] elementosCabeceraEtiquetasRecupera = new String[1][etiquetasRecupera.length + 1];
                elementosCabeceraEtiquetasRecupera[0][0] = elementoRecupera;
                for (int i = 0; i < etiquetasRecupera.length; i++) {
					elementosCabeceraEtiquetasRecupera[0][i+1] = 
							etiquetasRecupera[i];
				}
                XMLParserPedidoCompraCabecera parser = 
                		new XMLParserPedidoCompraCabecera(elementosCabeceraEtiquetasRecupera, -1, "");

                resultado = parser.recuperaLista(in)[0];



	        } catch (Exception e) {
	            Log.e("OCGASES", e.getMessage());

	        }

		return resultado;
	}

}
