package es.paumancar.ocgases.datasync;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

public class DataSyncAdapter extends AbstractThreadedSyncAdapter {
	

	  private static final String AUTHORITY = "es.paumancar.ocgases.datasync.contentprovider";
	  
	  private static final String DATOS_PATH = "datos";
	  private static final String DATOS_POST_PATH = "posts";
	  private static final String DATOS_LINEAS_PATH = "lineas";
	  private static final String SERVIDORES_PATH = "servidores";
	
	  private Context context;
	  //private String redPropia = "\"WLAN_OXIGENOCASTANO\"";
	  
	  //TODO Poner la red real de OC

	public DataSyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		this.context = context;
	}

	

	@Override
	public void onPerformSync(Account account, 
			Bundle extras, 
			String authority,
			ContentProviderClient provider, 
			SyncResult syncResult) {
		
		Log.w("OCGASES", "Starting onPerformSync");
		
		List<DatosEnvio> listaDatosEnvio = new ArrayList<DatosEnvio>();
		
		String[] projectionDatos = {"_id", "tabla", "metodo", "time_envio"};
		String[] projectionServidores = { "tipo", "direccion_ip", "puerto", "ssl"};
		
		Cursor curs = null;
		
		try {
			curs = provider.query(getUriByNameId(SERVIDORES_PATH, ""), projectionServidores, null, null, null);
			int numFilas = curs.getCount();
			Log.w("OCGASES", "Numfilas Servers: " +  String.valueOf(numFilas));
			String servidorHostPortLocal = "";
			String servidorHostPortRemoto = "";
			String servidorHostPuerto = "";
			for (int i = 0; i < numFilas; i++) {
				curs.moveToPosition(i);
				String tipoServidor = curs.getString(curs.getColumnIndex("tipo"));
				int sslServer = curs.getInt(curs.getColumnIndex("ssl"));
				Log.w("OCGASES", "Tipo Servidor: " +  tipoServidor);
				Log.w("OCGASES", "SSL Servidor: " +  ((sslServer==1)?"si":"no"));
				String protocoloHttp = (sslServer==1)?"https://":"http://";
				if (tipoServidor.equals("local")){
					servidorHostPortLocal = curs.getString(curs.getColumnIndex("direccion_ip"));
					String portLocal = curs.getString(curs.getColumnIndex("puerto"));
					if (!portLocal.equals("")){
						servidorHostPortLocal = protocoloHttp + servidorHostPortLocal + ":" + portLocal;
					}
				}
				if (tipoServidor.equals("remoto")){
					servidorHostPortRemoto = curs.getString(curs.getColumnIndex("direccion_ip"));
					String portRemoto = curs.getString(curs.getColumnIndex("puerto"));
					if (!portRemoto.equals("")){
						servidorHostPortRemoto = protocoloHttp + servidorHostPortRemoto + ":" + portRemoto;
					}
				}
			}
			servidorHostPuerto = Connectivity.nombreServidorPuerto(context, 
					servidorHostPortLocal, servidorHostPortRemoto);
			Log.w("OCGASES", "Host,puerto:  " + servidorHostPuerto);
			curs = provider.query(getUriByNameId(DATOS_PATH, ""), projectionDatos, null, null, null);
			numFilas = curs.getCount();
			Log.w("OCGASES", "Numfilas: " +  String.valueOf(numFilas));
			for (int i = 0; i < numFilas; i++) {
				curs.moveToPosition(i);
				long idEnvio = curs.getLong(curs.getColumnIndex("_id"));
				DatosEnvio datoEnvioActual = new DatosEnvio(context, 
						servidorHostPuerto, 
						curs.getString(curs.getColumnIndex("tabla")), 
						curs.getString(curs.getColumnIndex("metodo")), 
						"",
						recuperaListaLineas(provider, idEnvio), 
						recuperaListaPost(provider, idEnvio),
						idEnvio);
				listaDatosEnvio.add(datoEnvioActual);
				Log.w("OCGASES", String.valueOf(i) + ", " +  String.valueOf(idEnvio));
			}
		} catch (RemoteException e) {
			Log.w("OCGASES", e.getMessage());
		}
		
		curs.close();
		
		for (DatosEnvio datoEnvio : listaDatosEnvio) {
			String resultadoEnvio = enviaUnicoDato(datoEnvio);
			Log.w("OCGASES", resultadoEnvio);
			if (resultadoEnvio.equals("Correcto")){
				borraEnvio(provider, datoEnvio.getIdEnvio());
			} else {
				sumaReintentosEnvio(provider, datoEnvio.getIdEnvio());
			}
		}
	}
	


	private List<String> recuperaListaLineas(ContentProviderClient provider, long idEnvio) {
		List<String> listaLineas = new ArrayList<String>();
		String idEnvioString = String.valueOf(idEnvio);
		String[] projection = {"id_datos_envia", "linea"};
		Cursor curs = null;
		try {
			curs = provider.query(getUriByNameId(DATOS_LINEAS_PATH, idEnvioString), projection, 
					null, null, null);
			int numFilas = curs.getCount();
			for (int i = 0; i < numFilas; i++) {
				curs.moveToPosition(i);
				listaLineas.add(curs.getString(curs.getColumnIndex("linea")));
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		curs.close();
		return listaLineas;
	}
	

	private List<String[]> recuperaListaPost(ContentProviderClient provider, long idEnvio) {
		List<String[]> listaPost = new ArrayList<String[]>();
		String idEnvioString = String.valueOf(idEnvio);
		String[] projection = {"id_datos_envia", "etiqueta_post", "valor_post"};
		Cursor curs = null;
		try {
			curs = provider.query(getUriByNameId(DATOS_POST_PATH, idEnvioString), projection, 
					null, null, null);
			int numFilas = curs.getCount();
			for (int i = 0; i < numFilas; i++) {
				curs.moveToPosition(i);
				String[] valores = new String[2];
				valores[0] = curs.getString(curs.getColumnIndex("etiqueta_post"));
				valores[1] = curs.getString(curs.getColumnIndex("valor_post"));
				listaPost.add(valores);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		curs.close();
		return listaPost;
	}
	
	private Uri getUriByNameId(String namePath, String id){
		String firstPartName = "content://" + AUTHORITY + "/" + namePath;
		if (!id.equals("")){
			firstPartName = firstPartName + "/" + id;
		}
		return Uri.parse(firstPartName);
	}
	

	
	
	private String enviaUnicoDato(DatosEnvio datoEnvio) {
		context = datoEnvio.getContext();
		String direccionIP = datoEnvio.getHostServidor();
		String tablaEnvio = datoEnvio.getTablaEnvio();
		String metodo = datoEnvio.getMetodo();
		List<String[]> paramsPost = datoEnvio.getDatosPost();
		//List<String> lineas = datoEnvio.getLineas();
		
		String resultado = "";
		if (direccionIP.equals("") | 
				direccionIP.equals("http://") | 
				direccionIP.equals("https://") |
				direccionIP.equals(null)){
			return resultado;
		}
		
		HttpURLConnection conn = null;
		String direccion = direccionIP + "/rest/source/?object=" + tablaEnvio + "&action=custom";
		URL url = null;
		try {
			url = new URL(direccion);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		
	    try {
	    	if (url != null) {
	    	   conn = (HttpURLConnection)url.openConnection();
	    	   conn.setReadTimeout(10000);
	    	   conn.setConnectTimeout(15000);
	    	   conn.setRequestMethod(metodo);
	    	   conn.setDoInput(true);
	    	   conn.setDoOutput(true);

	    	   OutputStream os = conn.getOutputStream();
	    	   BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
	    	   writer.write(getQuery(paramsPost));
	    	   writer.flush();
	    	   writer.close();
	    	   os.close();
	    	}

	        int statusCode = conn.getResponseCode();
	        resultado = String.valueOf(statusCode);
	        if(statusCode == 200) {
	        	resultado = "Correcto";
	        } else {
	        	InputStream is = conn.getErrorStream();
	        	 BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	        	    StringBuilder sb = new StringBuilder();

	        	    String line = null;
	        	    try {
	        	        while ((line = reader.readLine()) != null) {
	        	            sb.append(line).append('\n');
	        	        }
	        	    } catch (IOException e) {
	        	        e.printStackTrace();
	        	    } finally {
	        	        try {
	        	            is.close();
	        	        } catch (IOException e) {
	        	            e.printStackTrace();
	        	        }
	        	    }
	        	    resultado = sb.toString();
	        }
	    }
	    catch (Exception e) {
	        Log.e("OCGASES", e.getMessage());
	        resultado = e.getMessage();
	    }
	    finally {
	        conn.disconnect();
	    }

		return resultado;
	}
	
	private String getQuery(List<String[]> params) throws UnsupportedEncodingException
	{
	    StringBuilder result = new StringBuilder();
	    boolean first = true;

	    for (String[] pair : params)
	    {
	        if (first)
	            first = false;
	        else
	            result.append("&");

	        result.append(URLEncoder.encode(pair[0], "UTF-8"));
	        result.append("=");
	        result.append(URLEncoder.encode(pair[1], "UTF-8"));
	    }

	    return result.toString();
	}
	

	private void borraEnvio(ContentProviderClient provider, Long idEnvio) {
		try {
			String stringIdEnvio = String.valueOf(idEnvio);
			provider.delete(getUriByNameId(DATOS_PATH, stringIdEnvio), null, null);
			provider.delete(getUriByNameId(DATOS_POST_PATH, stringIdEnvio), null, null);
			provider.delete(getUriByNameId(DATOS_LINEAS_PATH, stringIdEnvio), null, null);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
	}
	

	private void sumaReintentosEnvio(ContentProviderClient provider, Long idEnvio) {
		String stringIdEnvio = String.valueOf(idEnvio);
		int reintentos = 0;
		String[] projection = {"_id", "reintentos"};
		Cursor curs = null;
		try {
			curs = provider.query(getUriByNameId(DATOS_PATH, stringIdEnvio), 
					projection, null, null, null);
			curs.moveToPosition(0);
			reintentos = curs.getInt(curs.getColumnIndex("reintentos"));
			reintentos ++;
			ContentValues values = new ContentValues();
			values.put("reintentos", reintentos);
			provider.update(getUriByNameId(DATOS_PATH, stringIdEnvio), values, null, null);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
	}



}
