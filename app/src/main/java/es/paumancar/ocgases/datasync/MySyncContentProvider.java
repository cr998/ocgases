package es.paumancar.ocgases.datasync;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class MySyncContentProvider extends ContentProvider {
	
	  // database
	  private MySQLiteHelperSync syncHelper;

	  // used for the UriMacher
	  
	  private static final int DATOS = 10;
	  private static final int DATO_ID = 11;
	  private static final int POSTS = 20;
	  private static final int POST_ID = 21;
	  private static final int LINEAS = 30;
	  private static final int LINEA_ID = 31;
	  private static final int SERVIDORES = 90;
	  private static final int SERVIDOR_ID = 91;

	  private static final String AUTHORITY = "es.paumancar.ocgases.datasync.contentprovider";

	  //tablas
	  private static final String TABLA_DATOS = "table_datos_envia";
	  private static final String TABLA_POSTS = "table_datos_envia_post";
	  private static final String TABLA_LINEAS = "table_datos_envia_lineas";
	  private static final String TABLA_SERVIDORES = "table_servidores";
	  
	  private static final String DATOS_PATH = "datos";
	  private static final String DATOS_POST_PATH = "posts";
	  private static final String DATOS_LINEAS_PATH = "lineas";
	  private static final String DATOS_SERVIDORES_PATH = "servidores";
	  
	  //URIS de tablas
	  public static final Uri CONTENT_URI_DATOS = Uri.parse("content://" + AUTHORITY
	      + "/" + DATOS_PATH);
	  public static final Uri CONTENT_URI_DATOS_POST = Uri.parse("content://" + AUTHORITY
		      + "/" + DATOS_POST_PATH);
	  public static final Uri CONTENT_URI_DATOS_LINEAS = Uri.parse("content://" + AUTHORITY
		      + "/" + DATOS_LINEAS_PATH);
	  public static final Uri CONTENT_URI_SERVIDORES = Uri.parse("content://" + AUTHORITY
		      + "/" + DATOS_SERVIDORES_PATH);

	  //Tipos
	  public static final String CONTENT_DATOS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
	      + "/datos";
	  public static final String CONTENT_DATOS_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
	      + "/dato";
	  public static final String CONTENT_POST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
		      + "/posts";
	  public static final String CONTENT_POST_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
		      + "/post";
	  public static final String CONTENT_LINEAS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
		      + "/lineas";
	  public static final String CONTENT_LINEAS_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
		      + "/linea";
	  public static final String CONTENT_SERVIDORES_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
		      + "/servidores";
	  public static final String CONTENT_SERVIDORES_ITEM_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
		      + "/servidor";

	  private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	  static {
	    sURIMatcher.addURI(AUTHORITY, DATOS_PATH, DATOS);
	    sURIMatcher.addURI(AUTHORITY, DATOS_PATH + "/#", DATO_ID);
	    sURIMatcher.addURI(AUTHORITY, DATOS_POST_PATH, POSTS);
	    sURIMatcher.addURI(AUTHORITY, DATOS_POST_PATH + "/#", POST_ID);
	    sURIMatcher.addURI(AUTHORITY, DATOS_LINEAS_PATH, LINEAS);
	    sURIMatcher.addURI(AUTHORITY, DATOS_LINEAS_PATH + "/#", LINEA_ID);
	    sURIMatcher.addURI(AUTHORITY, DATOS_SERVIDORES_PATH, SERVIDORES);
	    sURIMatcher.addURI(AUTHORITY, DATOS_SERVIDORES_PATH + "/#", SERVIDOR_ID);
	  }

	

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		 
	    SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

	    // check if the caller has requested a column which does not exists
	    //checkColumns(projection);
	    

	    int uriType = sURIMatcher.match(uri);
	    switch (uriType) {
	    case DATOS:
	    	queryBuilder.setTables(TABLA_DATOS);
	    	break;
	    case DATO_ID:
	    	queryBuilder.setTables(TABLA_DATOS);
	    	queryBuilder.appendWhere("_id" + "=" + uri.getLastPathSegment());
	    	break;
	    case POSTS:
	    	queryBuilder.setTables(TABLA_POSTS);
	    	break;
	    case POST_ID:
	    	queryBuilder.setTables(TABLA_POSTS);
	    	queryBuilder.appendWhere("id_datos_envia" + "=" + uri.getLastPathSegment());
	    	break;
	    case LINEAS:
	    	queryBuilder.setTables(TABLA_LINEAS);
	    	break;
	    case LINEA_ID:
	    	queryBuilder.setTables(TABLA_LINEAS);
	    	queryBuilder.appendWhere("id_datos_envia" + "=" + uri.getLastPathSegment());
	    	break;
	    case SERVIDORES:
	    	queryBuilder.setTables(TABLA_SERVIDORES);
	    	break;
	    case SERVIDOR_ID:
	    	queryBuilder.setTables(TABLA_SERVIDORES);
	    	queryBuilder.appendWhere("tipo" + "=" + uri.getLastPathSegment());
	    	break;
	    default:
	    	throw new IllegalArgumentException("Unknown URI: " + uri);
	    }
	    syncHelper = new MySQLiteHelperSync(getContext());
	    SQLiteDatabase db = syncHelper.getReadableDatabase();
	    Cursor cursor = queryBuilder.query(db, projection, selection,
	        selectionArgs, null, null, sortOrder);
	    // make sure that potential listeners are getting notified
	    cursor.setNotificationUri(getContext().getContentResolver(), uri);
//	    db.close();
//	    syncHelper.close();
	    return cursor;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
	    int uriType = sURIMatcher.match(uri);
	    syncHelper = new MySQLiteHelperSync(getContext());
	    SQLiteDatabase sqlDB = syncHelper.getWritableDatabase();
	    int rowsDeleted = 0;
	    String id = "";
	    switch (uriType) {
	    case DATOS:
	    	rowsDeleted = sqlDB.delete(TABLA_DATOS, selection, selectionArgs);
	    	break;
	    case DATO_ID:
	    	id = uri.getLastPathSegment();
	    	if (TextUtils.isEmpty(selection)) {
	    		rowsDeleted = sqlDB.delete(TABLA_DATOS, "_id =" + id, null);
	    	} else {
	    		rowsDeleted = sqlDB.delete(TABLA_DATOS, "_id =" + id + " and " + selection, selectionArgs);
	    	}
	    	break;
	    case POSTS:
		    rowsDeleted = sqlDB.delete(TABLA_POSTS, selection, selectionArgs);
		    break;
		case POST_ID:
		    id = uri.getLastPathSegment();
		    if (TextUtils.isEmpty(selection)) {
		    	rowsDeleted = sqlDB.delete(TABLA_POSTS, "id_datos_envia =" + id, null);
		    } else {
		        rowsDeleted = sqlDB.delete(TABLA_POSTS, "id_datos_envia =" + id + " and " + selection, 
		        		selectionArgs);
		    }
		    break;
	    case LINEAS:
		    rowsDeleted = sqlDB.delete(TABLA_LINEAS, selection, selectionArgs);
		    break;
		case LINEA_ID:
		    id = uri.getLastPathSegment();
		    if (TextUtils.isEmpty(selection)) {
		    	rowsDeleted = sqlDB.delete(TABLA_LINEAS, "id_datos_envia =" + id, null);
		    } else {
		        rowsDeleted = sqlDB.delete(TABLA_LINEAS, "id_datos_envia =" + id + " and " + selection, 
		        		selectionArgs);
		    }
		    break;
		      	
	    default:
	      throw new IllegalArgumentException("Unknown URI: " + uri);
	    }
	    getContext().getContentResolver().notifyChange(uri, null);
//	    sqlDB.close();
//	    syncHelper.close();
	    return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
	    int uriType = sURIMatcher.match(uri);
	    syncHelper = new MySQLiteHelperSync(getContext());
	    SQLiteDatabase sqlDB = syncHelper.getWritableDatabase();
	    int rowsUpdated = 0;
	    String id = "";
	    switch (uriType) {
	    case DATOS:
	    	rowsUpdated = sqlDB.update(TABLA_DATOS, values, selection, selectionArgs);
	    	break;
	    case DATO_ID:
	    	id = uri.getLastPathSegment();
	    	if (TextUtils.isEmpty(selection)) {
	    		rowsUpdated = sqlDB.update(TABLA_DATOS, values, "_id =" + id, null);
	    	} else {
	    		rowsUpdated = sqlDB.update(TABLA_DATOS, values, "_id =" + id + " and " + selection, 
	    				selectionArgs);
	    	}
	    	break;
	    case POSTS:
	    	rowsUpdated = sqlDB.update(TABLA_POSTS, values, selection, selectionArgs);
		    break;
		case POST_ID:
		    id = uri.getLastPathSegment();
		    if (TextUtils.isEmpty(selection)) {
		    	rowsUpdated = sqlDB.update(TABLA_POSTS, values, "id_datos_envia =" + id, null);
		    } else {
		    	rowsUpdated = sqlDB.update(TABLA_POSTS, values, "id_datos_envia =" + id + " and " + selection, 
		        		selectionArgs);
		    }
		    break;
	    case LINEAS:
	    	rowsUpdated = sqlDB.update(TABLA_LINEAS, values, selection, selectionArgs);
		    break;
		case LINEA_ID:
		    id = uri.getLastPathSegment();
		    if (TextUtils.isEmpty(selection)) {
		    	rowsUpdated = sqlDB.update(TABLA_LINEAS, values, "id_datos_envia =" + id, null);
		    } else {
		    	rowsUpdated = sqlDB.update(TABLA_LINEAS, values, "id_datos_envia =" + id + " and " + selection, 
		        		selectionArgs);
		    }
	    case SERVIDORES:
	    	rowsUpdated = sqlDB.update(TABLA_SERVIDORES, values, selection, selectionArgs);
		    break;
		case SERVIDOR_ID:
		    id = uri.getLastPathSegment();
		    if (TextUtils.isEmpty(selection)) {
		    	rowsUpdated = sqlDB.update(TABLA_SERVIDORES, values, "tipo =" + id, null);
		    } else {
		    	rowsUpdated = sqlDB.update(TABLA_SERVIDORES, values, "tipo =" + id + " and " + selection, 
		        		selectionArgs);
		    }
		    break;
		      	
	    default:
	      throw new IllegalArgumentException("Unknown URI: " + uri);
	    }
	    getContext().getContentResolver().notifyChange(uri, null);
//	    sqlDB.close();
//	    syncHelper.close();
	    return rowsUpdated;
	}
	

}
