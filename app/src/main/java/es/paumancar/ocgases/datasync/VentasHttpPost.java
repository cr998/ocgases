package es.paumancar.ocgases.datasync;


import java.util.ArrayList;
import java.util.List;

import es.paumancar.ocgases.MySQLiteHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class VentasHttpPost {
	Context context;
	String[] headerId = {"headerId", ""};
	String[] partner = {"partner", ""};
	String[] htmlReport = {"htmlReport", ""};
	List<String> listaLineas; 
	String[] numeroAlbaran = {"numero", ""};
	String[] latLong = {"latLong", ""};
	String[] latitud = {"Latitud", ""};
	String[] longitud = {"Longitud", ""};
	String[] firma = {"firma", ""};
	String[] nombreFirma = {"nombreFirma", ""};
	
	// EN ESTA CLASE PONEMOS LOS CODIGOS RECIBIDOS COMO VACIOS Y LOS ENTREGADOS COMO LLENOS
	// EN LA DE COMPRAS SERIA AL REVES
	
	List<String[]> postValues = new ArrayList<String[]>();

	
//			&headerId=1024018
//			&partner=1002857
//			&htmlReport=PCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURCBYSFRNTCAxLjAgVHJhbnNpdGlvbmFsLy9FTiIKImh0dHA6Ly93d3cudzMub3JnL1RSL3hodG1sMS9EVEQveGh0bWwxLXRyYW5zaXRpb25hbC5kdGQiPgo8aHRtbD48aGVhZD48bWV0YSBodHRwLWVxdWl2PSJjb250ZW50LXR5cGUiIGNvbnRlbnQ9InRleHQvaHRtbDsgY2hhcnNldD11dGYtOCIgLz48L2hlYWQPGJvZHkPHByZT4KPC9wcmUPGltZyBzcmM9IkxPR08yLmpwZyIgLz48YnIgLz48YnIgLz48cHJlPgogCiAKPHN0cm9uZz5UQUxMRVJFUyBZIFNFUlZJQ0lPUyBNRVRBTEJBUiwgUy5MLjwvc3Ryb25nPgpCNzQzOTAyMTIgIApjL0xhIEVzdGFjaW9uLCBzL24gLSBCYXJyb3MgLSBMYW5ncmVvCgpBbGJhcsOhbjogQ1IyLTAwMTMxMyAtIEZlY2hhOiAxMC8yMS8xNSAtIFBlZGlkbzogCiAgICBDw7NkaWdvICAgICAgICAgICAgICAgICAgICAgIERlc2NyaXBjacOzbiBVZHMuICBSY3MgICAgUHJlY2lvICAgICAgIER0byAgIEltcG9ydGUKLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KICAgICA0MDQyMSAgICAgICAgICBBUkNBTCAyMSBBTFRPUCBCNTAgLTExbSAgICAyICAgIDIgICAgIDAuMDAwICAgICAgMC4wMCAgICAgIDAuMDAKRW50cmVnYWRvOiBhc2RmYWNmZHIsY3FldzQyM2NxdwpSZWNpYmlkbzogZGFzZjMzcWV3LHF3ZXJxZHdlCgoKICAgICAgICA5OSAgICAgICAgICAgICAgICAgU0VSVklDSU8gRU5UUkVHQSAgICAyICAgIDIgICAgIDAuMDAwICAgICAgMC4wMCAgICAgIDAuMDAKLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KCkNvbmRpY2lvbmVzIGVjb25vbWljYXMgZW4gZmFjdHVyYSBhIGZpbiBkZSBtZXMgZGUgQWlyTGlxdWlkIEVzcGHDsWEKUmV2aXNhZG8gcG9yOiBBbnRvbmlvCjwvcHJlPjxpbWcgc3JjPSJDUjItMDAxMzEzLmpwZyIgLz48cHJlPjxiciAvPgpEZSBhY3VlcmRvIGNvbiBsbyBlc3RhYmxlY2lkbyBlbiBsYSBMZXkgT3JnYW5pY2EgMTUvMTk5OSwgZGUgMTMgZGUgZGljaWVtYnJlLCBkZSBQcm90ZWNjaW9uIGRlIERhdG9zIGRlIENhcmFjdGVyIFBlcnNvbmFsLCBPWElHRU5PIENBU1RBTk8sIFMuTC4sIGxlIGluZm9ybWEgcXVlIGxvcyBkYXRvcyByZWNvZ2lkb3MgZHVyYW50ZSBsYSByZWxhY2lvbiBuZWdvY2lhbCAoaW5jbHVpZGEgbGEgZmlybWEgZGlnaXRhbGl6YWRhKSBzZXJhbiBpbmNvcnBvcmFkb3MgYSB1biBmaWNoZXJvIGRlbCBxdWUgZXMgdGl0dWxhciBsYSBlbXByZXNhIHkgY3V5YSBmaW5hbGlkYWQgZXMgZ2VzdGlvbmFyIGVsIHNlcnZpY2lvIHNvbGljaXRhZG8sIHRyYXRhbWllbnRvIHF1ZSBhdXRvcml6YS4gUG9kcmEgZWplcmNpdGFyIGdyYXR1aXRhbWVudGUgc3VzIGRlcmVjaG9zIGRlIGFjY2VzbywgcmVjdGlmaWNhY2lvbiwgY2FuY2VsYWNpb24geSBvcG9zaWNpb24gYWwgdHJhdGFtaWVudG8gZGUgbG9zIGRhdG9zIGRlIGNhcmFjdGVyIHBlcnNvbmFsIGRlIGxvcyBxdWUgZXMgcmVzcG9uc2FibGUgT1hJR0VOTyBDQVNUQU5PLCBTLkwuLCBtZWRpYW50ZSBlc2NyaXRvIGRpcmlnaWRvIGFsIFJlc3BvbnNhYmxlIGRlIFNlZ3VyaWRhZCBxdWUgcG9kcmEgc2VyIHByZXNlbnRhZG8gZGlyZWN0YW1lbnRlIG8gYmllbiBzZXIgcmVtaXRpZG8gcG9yIGNvcnJlbyBjZXJ0aWZpY2Fkby9jb3JyZW8gZWxlY3Ryb25pY28gYSBsYSBkaXJlY2Npb24gZGUgT1hJR0VOTyBDQVNUQU5PLCBTLkwuIChQb2wuIEFzaXBvLiBDLyBBLCBwYXJjZWxhIDEtNC4gMzM0MjQgTGxhbmVyYS4gYW5nZWxAb3hpZ2Vub2Nhc3Rhbm8uZXMpLiBBZGVtYXMsIGxlIGluZm9ybWEgcXVlIGFsZ3Vub3MgZGUgc3VzIGRhdG9zIHNlcmFuIGNlZGlkb3MgYSBsYSBBZG1pbmlzdHJhY2lvbiBUcmlidXRhcmlhLCBhIGxhcyBmdWVyemFzIHkgY3VlcnBvcyBkZSBzZWd1cmlkYWQgeSBlbnRpZGFkZXMgbmVjZXNhcmlhcyBwYXJhIGxhIHByZXN0YWNpb24gZGVsIHNlcnZpY2lvIHNvbGljaXRhZG8gY29tbyBBaXIgTGlxdWlkZSBFc3BhbmEuIEVuIGN1YWxxdWllciBjYXNvLCBwb2RyYSBlamVyY2l0YXIgbG9zIGRlcmVjaG9zIGRlIGFjY2VzbywgcmVjdGlmaWNhY2lvbiwgY2FuY2VsYWNpb24geSBvcG9zaWNpb24gbWVkaWFudGUgbGEgcmVtaXNpb24gZGUgdW4gZXNjcml0byBhIGxhIGRpcmVjY2lvbiBhbnRlcyBpbmRpY2FkYS4KCgoKCgoKPC9wcmUPC9ib2R5PjwvaHRtbD4%3D
//			&e1111343=
//			&r1111343=
//			&e1111340=asdfacfdr,cqew423cqw
//			&r1111340=dasf33qew,qwerqdwe
//			&lineas=1111343,1111340
//			&numero=CR2-001313
//			&firma=%2F9j%2F4AAQ
	
	public VentasHttpPost(Context context, String headerId, String partner, String htmlReport,
			List<String> listaLineas, String numeroAlbaran, String latLong, String firma, 
			String nombreFirma) {
		super();
		String[] latLongArray = latLong.split(":");
		this.context = context;
		this.headerId[1] = headerId;
		this.partner[1] = partner;
		this.htmlReport[1] = htmlReport;
		this.listaLineas = listaLineas;
		this.numeroAlbaran[1] = numeroAlbaran;
		this.latitud[1] = latLongArray[0];
		this.longitud[1] = latLongArray[1];
		this.latLong[1] = latLong;
		this.firma[1] = firma;
		this.nombreFirma[1] = nombreFirma;
		
		postValues.add(this.headerId);
		postValues.add(this.partner);
		postValues.add(this.htmlReport);
		postValues.add(this.numeroAlbaran);
		postValues.add(this.latitud);
		postValues.add(this.longitud);
		postValues.add(this.latLong);
		postValues.add(this.firma);
		postValues.add(this.nombreFirma);
		addLineasYCodigos();
	}


	private void addLineasYCodigos() {
		int dimLista = listaLineas.size();
		if (dimLista == 0) return;
		MySQLiteHelper myHelper = (MySQLiteHelper) new MySQLiteHelper(context);
		SQLiteDatabase db = myHelper.getReadableDatabase();
		String stringLineas = "";
		int count = 0;
		for (String stringsLinCod : listaLineas) {
			stringLineas = stringLineas + stringsLinCod;
			String stringCodigosRecibidos = myHelper.getStringCodigosVaciosPost(db, stringsLinCod, "venta"); 	//VACIAS
			String stringCodigosEnviados = myHelper.getStringCodigosLlenosPost(db, stringsLinCod, "venta");	//LENAS
			String stringIncidencias = myHelper.getStringIncidenciasPost(db, stringsLinCod, "venta"); //INCIDENCIAS
			if (count < (dimLista-1)) {
				stringLineas = stringLineas + ",";
			}
			String[] codigosRecibidosAdd = {"r" + stringsLinCod, stringCodigosRecibidos};
			postValues.add(codigosRecibidosAdd);
			String[] codigosEnviadosAdd = {"e" + stringsLinCod, stringCodigosEnviados};
			postValues.add(codigosEnviadosAdd);
			if (!stringIncidencias.equals("")){
				String[] incidenciasAdd = {"i" + stringsLinCod, stringIncidencias};
				postValues.add(incidenciasAdd);
			}
			count++;
		}
		String[] lineasAdd = {"lineas", stringLineas};
		postValues.add(lineasAdd);
		db.close();
		myHelper.close();
	}


	public List<String[]> getPostValues() {
		return postValues;
	}

	
}