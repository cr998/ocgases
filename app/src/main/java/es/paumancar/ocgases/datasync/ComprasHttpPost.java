package es.paumancar.ocgases.datasync;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import es.paumancar.ocgases.MySQLiteHelper;

public class ComprasHttpPost {

	Context context;
	String[] headerId = {"headerId", ""};
	String[] partnerId = {"partner", ""};
	List<String> listaLineas; ;
	//String[] latLong = {"latLong", ""};
	
	// EN ESTA CLASE PONEMOS LOS CODIGOS RECIBIDOS COMO LLENOS Y LOS ENTREGADOS COMO VACIOS

	
	List<String[]> postValues = new ArrayList<String[]>();

	
	public ComprasHttpPost(Context context, String headerId, String partnerId,
			List<String> listaLineas, String latLong) {
		super();
		this.context = context;
		this.headerId[1] = headerId;
		this.partnerId[1] = partnerId;
		this.listaLineas = listaLineas;
		//this.latLong[1] = latLong;
		
		postValues.add(this.headerId);
		postValues.add(this.partnerId);
		//postValues.add(this.latLong);
		addLineasYCodigos();
	}


	private void addLineasYCodigos() {
		int dimLista = listaLineas.size();
		if (dimLista == 0) return;
		MySQLiteHelper myHelper = (MySQLiteHelper) new MySQLiteHelper(context);
		SQLiteDatabase db = myHelper.getReadableDatabase();
		String stringLineas = "";
		int count = 0;
		for (String stringsLinCod : listaLineas) {
			stringLineas = stringLineas + stringsLinCod;
			
			//TODO Esto voy a cambiarlo, aunque creo que está mal
			// Deberían ser recibidos llenos y entregados vacíos.
			String stringCodigosRecibidos = myHelper.getStringCodigosVaciosPost(db, stringsLinCod, "compra"); 	//VACIAS
			String stringCodigosEnviados = myHelper.getStringCodigosLlenosPost(db, stringsLinCod, "compra");	//LENAS
			String stringIncidencias = myHelper.getStringIncidenciasPost(db, stringsLinCod, "compra"); //INCIDENCIAS
			if (count < (dimLista-1)) {
				stringLineas = stringLineas + ",";
			}
			String[] codigosRecibidosAdd = {"r" + stringsLinCod, stringCodigosRecibidos};
			postValues.add(codigosRecibidosAdd);
			String[] codigosEnviadosAdd = {"e" + stringsLinCod, stringCodigosEnviados};
			postValues.add(codigosEnviadosAdd);
			if (!stringIncidencias.equals("")){
				String[] incidenciasAdd = {"i" + stringsLinCod, stringIncidencias};
				postValues.add(incidenciasAdd);
			}
			count++;
		}
		String[] lineasAdd = {"lineas", stringLineas};
		postValues.add(lineasAdd);
		db.close();
		myHelper.close();
	}


	public List<String[]> getPostValues() {
		return postValues;
	}

	

}
