package es.paumancar.ocgases.datasync;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import es.paumancar.ocgases.MySQLiteHelper;

public class CodigosHttpPost {

	Context context;
	String[] entregados = {"entregados", ""};
	String[] recibidos = {"recibidos", ""};

	List<String[]> postValues = new ArrayList<String[]>();

	
	public CodigosHttpPost(Context context, boolean recibidos_son_llenos) {
		super();
		this.context = context;
		
		
		MySQLiteHelper myHelper = (MySQLiteHelper) new MySQLiteHelper(context);
		SQLiteDatabase db = myHelper.getReadableDatabase();			
		String stringCodigosRecibidos;
		String stringCodigosEntregados;
		if (recibidos_son_llenos){
			stringCodigosRecibidos = myHelper.getStringCodigosLlenosPost(db, "codigos_sueltos", "provisional"); 	//VACIAS
			stringCodigosEntregados = myHelper.getStringCodigosVaciosPost(db, "codigos_sueltos", "provisional");
		} else {
			stringCodigosEntregados = myHelper.getStringCodigosLlenosPost(db, "codigos_sueltos", "provisional"); 	//VACIAS
			stringCodigosRecibidos = myHelper.getStringCodigosVaciosPost(db, "codigos_sueltos", "provisional");			
		}
		db.close();
		myHelper.close();
		
		this.entregados[1] = stringCodigosEntregados.replace(",", "\n");
		this.recibidos[1] = stringCodigosRecibidos.replace(",", "\n");

		postValues.add(this.entregados);
		postValues.add(this.recibidos);
	}


	public List<String[]> getPostValues() {
		return postValues;
	}

	
}
