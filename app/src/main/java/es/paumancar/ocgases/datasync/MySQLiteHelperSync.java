package es.paumancar.ocgases.datasync;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.webkit.WebView.FindListener;
import android.widget.TextView;


public class MySQLiteHelperSync extends SQLiteOpenHelper {
	
	private Context context;
	
	private static final String DATABASE_NAME = "ocgases_sync.db";
	private static final int DATABASE_VERSION = 2;
	

	
private static final String CREATE_TABLE_SERVIDORES = "create table " +
		"table_servidores (  "
		+ "_id integer primary key autoincrement, " +   //Puede ser local o remoto
		"tipo text not null, " +
		"direccion_ip text, " +
		"puerto text, " +
		"ssl integer default 0" +
		");";
		
	private static final String CREATE_TABLE_DATOS_ENVIA = "create table " +
			"table_datos_envia (  "
			+ "_id integer primary key autoincrement, " +
			"tabla text not null, " +
			"metodo text not null, " +
			"time_envio text not null, " +
			"reintentos integer default 0 " +
			");";
	

		
	private static final String CREATE_TABLE_DATOS_ENVIA_POST = "create table " +
			"table_datos_envia_post (  "
			+ "_id integer primary key autoincrement, " +
			"id_datos_envia integer not null, " +
			"etiqueta_post text, " +
			"valor_post text " +
			");";
	
		
	private static final String CREATE_TABLE_DATOS_ENVIA_LINEAS = "create table " +
			"table_datos_envia_lineas (  "
			+ "_id integer primary key autoincrement, " +
			"id_datos_envia integer not null, " +
			"linea text " +
			");";
		
	public MySQLiteHelperSync(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		
	}
	

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_TABLE_SERVIDORES);
		database.execSQL(CREATE_TABLE_DATOS_ENVIA);
		database.execSQL(CREATE_TABLE_DATOS_ENVIA_POST);
		database.execSQL(CREATE_TABLE_DATOS_ENVIA_LINEAS);
		insertaDatosInicialesServidor(database);
	}
	
	/* GUARDAR DATOS */
	private void insertaDatosInicialesServidor(SQLiteDatabase db){
			ContentValues valuesLoc = new ContentValues();
			valuesLoc.put("tipo", "local");
			valuesLoc.put("direccion_ip", "");
			valuesLoc.put("puerto", "80");
			db.insert("table_servidores", null, valuesLoc);
			ContentValues valuesRem = new ContentValues();
			valuesRem.put("tipo", "remoto");
			valuesRem.put("direccion_ip", "");
			valuesRem.put("puerto", "80");
			db.insert("table_servidores", null, valuesRem);
	}
	

	
	public void insertaDatosEnvio(SQLiteDatabase db, DatosEnvio datos){
		String tablaEnvio = datos.getTablaEnvio();
		String metodo = datos.getMetodo();
		List<String[]> datosPost = datos.getDatosPost();
		List<String> lineas = datos.getLineas();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss"); 
		String dateString = formatter.format(new Date(calendar.getTimeInMillis()));
		ContentValues values = new ContentValues();
		values.put("tabla", tablaEnvio);
		values.put("metodo", metodo);
    	values.put("time_envio", dateString);
		long id_dato = db.insert("table_datos_envia", null, values);
		if (id_dato != -1){
			insertaValoresPost(db, id_dato, datosPost);
			insertaValoresLineas(db, id_dato, lineas);
		}
	}

	/* RECUPERAR DATOS */
	
	public String recuperaServidorPuerto(SQLiteDatabase db, String tipo ){
		String servidorPuerto = "";
		String consultaServidor = "select tipo, direccion_ip, puerto from table_servidores "
				+ "where (tipo ='" + tipo + "')";
		Cursor curs = db.rawQuery(consultaServidor, null);
		curs.moveToPosition(0);
		servidorPuerto = servidorPuerto + curs.getString(curs.getColumnIndex("puerto"));
		String puerto = curs.getString(curs.getColumnIndex("puerto"));
		if (!puerto.equals("")){
			servidorPuerto = servidorPuerto + ":" + puerto;
		}
		return servidorPuerto;
	}
	
	public List<DatosEnvio> recuperaDatosEnvio(SQLiteDatabase db){
		List<DatosEnvio> listaDatosEnvio = new ArrayList<DatosEnvio>();
		String consultaDatosEnvio = "select _id, tabla, metodo, time_envio from "
				+ "table_datos_envia";
		Cursor curs = db.rawQuery(consultaDatosEnvio, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			long idEnvio = curs.getInt(curs.getColumnIndex("_id"));
			DatosEnvio datoEnvioActual = new DatosEnvio(null, null, 
					curs.getString(curs.getColumnIndex("tabla")), 
					curs.getString(curs.getColumnIndex("metodo")), 
					"",
					recuperaListaLineas(db, idEnvio), 
					recuperaListaPost(db, idEnvio),
					idEnvio);
			listaDatosEnvio.add(datoEnvioActual);
		}
		curs.close();
		return listaDatosEnvio;
	}
	
	public int cuentaDatosPendientesPorTipo(SQLiteDatabase db, String tabla){
		int contador = 0;
		String consultaCuenta = "select _id, tabla from "
				+ "table_datos_envia"
				+ " where (tabla = '" + tabla + "')";
		Cursor curs = db.rawQuery(consultaCuenta, null);
		contador = curs.getCount();
		return contador;
	}

	
	/* BORRAR DATOS */

	public void borraDatoEnviado(SQLiteDatabase db, long id_dato){
		String[] idDato = new String[1];
		idDato[0] = String.valueOf(id_dato);
		db.delete("table_datos_envia", "_id = ?", idDato);
		db.delete("table_datos_envia_post", "id_datos_envia = ?", idDato);
		db.delete("table_datos_envia_lineas", "id_datos_envia = ?", idDato);
	}
	
	public void borraTodosDatosEnviados(SQLiteDatabase db){
		db.delete("table_datos_envia", "1 = 1", null);
		db.delete("table_datos_envia_post", "1 = 1", null);
		db.delete("table_datos_envia_lineas", "1 = 1", null);
	}
	
	
	/* ACTUALIZAR DATOS */
	
	
	public void actualizaDatosServidor(SQLiteDatabase db, String tipo ,String servidor, 
			String puerto, int ssl){
		String[] tipoArray = { tipo };
		ContentValues values = new ContentValues();
		values.put("direccion_ip", servidor);
		values.put("puerto", puerto);
		values.put("ssl", ssl);
		db.update("table_servidores", values, "tipo = ?", tipoArray);
	}

	public void incrementaEnvios(SQLiteDatabase db, long id_dato){
		String[] idDatoString = new String[1];
		idDatoString[0] = String.valueOf(id_dato);
		String consultaDatosEnvio = "select _id, reintentos from "
				+ "table_datos_envia"
				+ " where (_id = '" + idDatoString[0] + "')";
		Cursor curs = db.rawQuery(consultaDatosEnvio, null);
		curs.moveToPosition(0);
		int numeroReintentos = curs.getInt(curs.getColumnIndex("reintentos"));
		numeroReintentos++;
		ContentValues values = new ContentValues();
		values.put("reintentos", numeroReintentos);
		db.update("table_datos_envia", values, "_id = ?", idDatoString);
	}
	
	//**  FUNCIONES PRIVADAS **//

	private void insertaValoresLineas(SQLiteDatabase db, long id_dato,
			List<String> lineas) {
		if (lineas != null){
			for (String cadaLinea : lineas) {
				ContentValues values = new ContentValues();
				values.put("id_datos_envia", id_dato);
				values.put("linea", cadaLinea);
				db.insert("table_datos_envia_lineas", null, values);
			}
		}
	}


	private void insertaValoresPost(SQLiteDatabase db, long id_dato,
			List<String[]> datosPost) {
		for (String[] cadaDatoPost : datosPost) {
			ContentValues values = new ContentValues();
			values.put("id_datos_envia", id_dato);
			values.put("etiqueta_post", cadaDatoPost[0]);
			values.put("valor_post", cadaDatoPost[1]);
			db.insert("table_datos_envia_post", null, values);
		}
	}

	private List<String[]> recuperaListaPost(SQLiteDatabase db, long idEnvio) {
		List<String[]> listaPost = new ArrayList<String[]>();
		String idEnvioString = String.valueOf(idEnvio);
		String consultaPost = "select _id_datos_envia, etiqueta_post, valor_post "
				+ " where (id_datos_envia = '" + idEnvioString + "')";
		Cursor curs = db.rawQuery(consultaPost, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			String[] valores = new String[2];
			valores[0] = curs.getString(curs.getColumnIndex("etiqueta_post"));
			valores[1] = curs.getString(curs.getColumnIndex("valor_post"));
			listaPost.add(valores);
		}
		curs.close();
		return listaPost;
	}
	
	private List<String> recuperaListaLineas(SQLiteDatabase db, long idEnvio) {
		List<String> listaLineas = new ArrayList<String>();
		String idEnvioString = String.valueOf(idEnvio);
		String consultaLineas = "select _id_datos_envia, linea "
				+ " where (id_datos_envia = '" + idEnvioString + "')";
		Cursor curs = db.rawQuery(consultaLineas, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			listaLineas.add(curs.getString(curs.getColumnIndex("linea")));
		}
		curs.close();
		return listaLineas;
	}

	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelperSync.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS table_servidores");
		db.execSQL("DROP TABLE IF EXISTS table_datos_envia");
		db.execSQL("DROP TABLE IF EXISTS table_datos_envia_post");
		db.execSQL("DROP TABLE IF EXISTS table_datos_envia_lineas");

		onCreate(db);
	}


	
	
}