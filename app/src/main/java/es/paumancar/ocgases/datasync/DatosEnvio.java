package es.paumancar.ocgases.datasync;


import java.util.List;
import android.content.Context;

public class DatosEnvio {
	Context context;
	String hostServidor;
	String tablaEnvio;
	String metodo;  //GET o POST
	List<String[]> datosPost; //String array con dos datos: idPost, valorPost
	List<String> lineas;
	Long idEnvio;
	String documento;
	
	public DatosEnvio(Context context, String hostServidor, String tablaEnvio, String metodo, 
			String documento, List<String> lineas, List<String[]> datosPost, Long idEnvio) {
		super();
		this.hostServidor = hostServidor;
		this.tablaEnvio = tablaEnvio;
		this.metodo = metodo;
		this.datosPost = datosPost;
		this.context = context;
		this.lineas = lineas;
		this.idEnvio = idEnvio;
		this.documento = documento;
	}
	public String getHostServidor() {
		return hostServidor;
	}
	public void setHostServidor(String hostServidor) {
		this.hostServidor = hostServidor;
	}
	public String getTablaEnvio() {
		return tablaEnvio;
	}
	public void setTablaEnvio(String tablaEnvio) {
		this.tablaEnvio = tablaEnvio;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	public List<String[]> getDatosPost() {
		return datosPost;
	}
	public void setDatosPost(List<String[]> datosPost) {
		this.datosPost = datosPost;
	}
	public Context getContext() {
		return context;
	}
	public void setContext(Context context) {
		this.context = context;
	}
	public List<String> getLineas() {
		return lineas;
	}
	public void setLineas(List<String> lineas) {
		this.lineas = lineas;
	}
	public Long getIdEnvio() {
		return idEnvio;
	}
	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	
}
