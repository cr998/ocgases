package es.paumancar.ocgases;


import es.paumancar.ocgases.datasync.Connectivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.view.View.OnClickListener;

@SuppressLint("NewApi")
public class ActivityListadoCompras extends Activity {
	
	SharedPreferences sharedPref;
	
  	ListView listviewCompras;
  	String[][] stringAdapterCompras = { {"HEADER1", "data1"},
   		{"HEADER2", "data2"}, {"HEADER3", "data3"} };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.compras_layout);
        
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        listviewCompras = (ListView) findViewById(R.id.listviewCompras);
  
        //listviewCompras.setAdapter(new AdapterCompras(this, stringAdapterCompras ));
        
        
/*            String etiquetaPrincipal = "CartaDePorte";
            String[] etiquetasDatos = {
            		  "m_inout_porte_id",
            		  "documentno",
            		  "numero",
            		  "cliente",
            		  "direccion"
            };*/

        
        
/*            String etiquetaPrincipal = "AlbaranVentaCabecera";
            String[] etiquetasDatos = {
            		  "m_inout_id",
            		  "documentno",
            		  "fecha",
            		  "totallines",
            		  "grandtotal"
            };
            */
        String etiquetaPrincipal = "PedidoCompraCabecera";
        String[] etiquetasDatos = {
        		  "c_order_id",
        		  "documentno",
        		  "created",
        		  "nombreproveedor",
        		  "dateordered",
        		  "totallines",
        		  "c_bpartner_id"
        };
    
        
        int filtroId = -1; 	// indicates filter position in etiquetasDatos 
							// without filter -1
        int nextFiltroId = 0;
        String valorFiltro = ""; 

        Class<?> NextClass = ActivityListadoDetallesCompra.class;
        //Intent intentNexActivity = new Intent(getApplicationContext(), ActivityListadoDetallesCompra.class);
        Context contextActivity = ActivityListadoCompras.this;
        Context contextApp = getApplicationContext();
        String direccionPuertoServidor = Connectivity.servidorActivo(getApplicationContext());
        DatosLista datosListaCompra = 
        		new DatosLista(contextApp, 
        				listviewCompras, etiquetaPrincipal, etiquetasDatos, 
        				NextClass, filtroId, valorFiltro,  nextFiltroId, 
        				direccionPuertoServidor, 0, null, "" );
        
        RecuperaDatos stringDatosCompra = new RecuperaDatos(contextActivity, contextApp);
        stringDatosCompra.execute(datosListaCompra);
        
    }
	

}
