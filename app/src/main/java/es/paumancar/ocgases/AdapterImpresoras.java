package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class AdapterImpresoras extends BaseAdapter {

    Context context;
    List<String[]> listaImpresoras;
    private static LayoutInflater inflater = null;
    

    public AdapterImpresoras(Context context, List<String[]> listaImpresoras) {
        // TODO Auto-generated constructor stub
        this.context = context;
        if (listaImpresoras != null) {
        	this.listaImpresoras = listaImpresoras;
        } else {
        	this.listaImpresoras = new ArrayList<String[]>();
        }
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listaImpresoras.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listaImpresoras.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_printers, null);
        RadioButton botonActivo = (RadioButton) vi.findViewById(R.id.radioButtonPrinterActiva);
        TextView textDireccion = (TextView) vi.findViewById(R.id.rowTextPrinterDireccion);
        TextView textNombre = (TextView) vi.findViewById(R.id.rowTextPrinterNombre);
        TextView textEtiqueta = (TextView) vi.findViewById(R.id.rowTextPrinterEtiqueta);
        Button botonDelete = (Button) vi.findViewById(R.id.rowButtonPrinterDelete);
        Button botonEdit = (Button) vi.findViewById(R.id.rowButtonPrinterEdit);

        botonActivo.setChecked(listaImpresoras.get(position)[4].equals("0")?false:true);
        textEtiqueta.setText(listaImpresoras.get(position)[1]);
        textNombre.setText(listaImpresoras.get(position)[2]);
        textDireccion.setText(listaImpresoras.get(position)[3]);
		final int id_printer = Integer.valueOf(listaImpresoras.get(position)[0]);
		final int activa = Integer.valueOf(listaImpresoras.get(position)[4]);
		
		botonActivo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean esActivo = ((RadioButton) v).isChecked();
				if (esActivo) {
					MySQLiteHelper myHelper = new MySQLiteHelper(context);
					SQLiteDatabase db = myHelper.getWritableDatabase();
					myHelper.desactivaImpresoras(db);
					myHelper.activaImpresora(db, id_printer);
					listaImpresoras = myHelper.recuperaImpresoras(db);
					db.close();
					myHelper.close();
					actualizaDatos();
				}
			}
		});

		
		botonEdit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MySQLiteHelper myHelper = new MySQLiteHelper(context);
				SQLiteDatabase db = myHelper.getWritableDatabase();
				String[] lineaImpresora = myHelper.recuperaImpresoraPorId(db, id_printer);
				db.close();
				myHelper.close();
				
				final TextView TextEtiqueta = new TextView(context);
				final TextView TextNombre = new TextView(context);
				final TextView TextDireccion = new TextView(context);
				final EditText editTextEtiqueta = new EditText(context);
				final EditText editTextNombre = new EditText(context);
				final EditText editTextDireccion = new EditText(context);
				TextEtiqueta.setText("Etiqueta:   ");
				TextNombre.setText("Nombre:   ");
				TextDireccion.setText("Dirección: ");
				editTextEtiqueta.setText(lineaImpresora[1]);
				editTextNombre.setText(lineaImpresora[2]);
				editTextDireccion.setText(lineaImpresora[3]);
				editTextEtiqueta.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				editTextNombre.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				editTextDireccion.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				
				LinearLayout layoutTextos = new LinearLayout(context);
				LinearLayout.LayoutParams layoutForInner = 
						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
								LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutTextos.setLayoutParams(layoutForInner);
				layoutTextos.setOrientation(LinearLayout.VERTICAL);
				
				//LayoutParams params5Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 5f);
				LayoutParams params3Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3f);
				LayoutParams params7Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 7f);
				
				
				LinearLayout layoutEtiqueta = new LinearLayout(context);
				layoutEtiqueta.setLayoutParams(layoutForInner);
				layoutEtiqueta.setOrientation(LinearLayout.HORIZONTAL);
				TextEtiqueta.setLayoutParams(params3Weight);
				editTextEtiqueta.setLayoutParams(params7Weight);
				layoutEtiqueta.addView(TextEtiqueta);
				layoutEtiqueta.addView(editTextEtiqueta);
				
				LinearLayout layoutNombre = new LinearLayout(context);
				layoutNombre.setLayoutParams(layoutForInner);
				layoutNombre.setOrientation(LinearLayout.HORIZONTAL);
				TextNombre.setLayoutParams(params3Weight);
				editTextNombre.setLayoutParams(params7Weight);
				layoutNombre.addView(TextNombre);
				layoutNombre.addView(editTextNombre);
				
				LinearLayout layoutDireccion = new LinearLayout(context);
				layoutDireccion.setLayoutParams(layoutForInner);
				layoutDireccion.setOrientation(LinearLayout.HORIZONTAL);
				TextDireccion.setLayoutParams(params3Weight);
				editTextDireccion.setLayoutParams(params7Weight);
				layoutDireccion.addView(TextDireccion);
				layoutDireccion.addView(editTextDireccion);
				
				layoutTextos.addView(layoutEtiqueta);
				layoutTextos.addView(layoutNombre);
				layoutTextos.addView(layoutDireccion);

				AlertDialog.Builder dialog = new AlertDialog.Builder(context);
				
				dialog.setTitle("EDITA IMPRESORA");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	String textoDireccion = editTextDireccion.getText().toString(); 
				    	String textoNombre = editTextNombre.getText().toString(); 
				    	String textoEtiqueta = editTextEtiqueta.getText().toString(); 
				    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
						SQLiteDatabase db = myHelper.getWritableDatabase();
						myHelper.actualizaImpresora(db, id_printer, textoEtiqueta, textoNombre,
								textoDireccion, activa);
						listaImpresoras = myHelper.recuperaImpresoras(db);
						db.close();
						myHelper.close();
						dialog.cancel();
						actualizaDatos();
				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
				
			}
		});
		
		
        botonDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MySQLiteHelper myHelper = new MySQLiteHelper(context);
				SQLiteDatabase db = myHelper.getWritableDatabase();
				myHelper.borraImpresora(db, id_printer);
				listaImpresoras = myHelper.recuperaImpresoras(db);
			    db.close();
			    myHelper.close();
				actualizaDatos();
			}
		});
        return vi;
    }
    
    private void actualizaDatos(){
    	this.notifyDataSetChanged();
    }
    

}
