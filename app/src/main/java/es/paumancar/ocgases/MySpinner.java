package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import es.paumancar.ocgases.MyWheelView.CartasWheelAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources.Theme;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MySpinner extends Spinner {
	
	public Context mcontext = getContext();
	private List<String[]> listaFiltrada;
	private static LayoutInflater inflater = null;

	public MySpinner(Context context) {
		super(context);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}

	public MySpinner(Context context, int mode) {
		super(context, mode);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}

	public MySpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}

	public MySpinner(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("NewApi")
	public MySpinner(Context context, AttributeSet attrs, int defStyleAttr,
			int mode) {
		super(context, attrs, defStyleAttr, mode);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("NewApi")
	public MySpinner(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes, int mode) {
		super(context, attrs, defStyleAttr, defStyleRes, mode);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("NewApi")
	public MySpinner(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes, int mode, Theme popupTheme) {
		super(context, attrs, defStyleAttr, defStyleRes, mode, popupTheme);
		 inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		// TODO Auto-generated constructor stub
	}
	
	public void setMyAdapter (List<String[]> listaCartas){
		this.listaFiltrada = eliminaDuplicadosLista(listaCartas, 0);
		this.setAdapter(new CartasSpinnerAdapter(mcontext, listaFiltrada, 2));
	}
	
	public String getIdCarta(int index){
		return listaFiltrada.get(index)[0];
	}
	
   class CartasSpinnerAdapter extends BaseAdapter {
        private List <String[]> cartas;

        
        private int columna;

        
        /**
         * Constructor
         */
        protected  CartasSpinnerAdapter(Context context, List <String[]> cartas, int columna) {
            this.cartas = cartas;
            this.columna = columna;
            //setItemTextResource(R.id.wheel_name);
        }
        
        @Override
        public String[] getItem(int position) {
            return cartas.get(position);
        }


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (vi == null)
	            vi = inflater.inflate(R.layout.list_row_ventas_spinner, null);
	        TextView textRow = (TextView) vi.findViewById(R.id.rowTextDetailVentasSpinner);
	        textRow.setText(cartas.get(position)[columna]);
			return vi;
		}

		@Override
        public int getCount() {
            return cartas.size();
        }

        protected String getItemText(int index) {
            return  cartas.get(index)[columna];
        }

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

    }
   
   
   /********************************************
    *            FUNCIONES
    *
    ******************************************/
	private List <String[]> eliminaDuplicadosLista ( List <String[]> listaCompleta, int indice) {
		List <String[]> listaFiltrada = new ArrayList<String[]>();
		if (listaCompleta == null) return listaFiltrada;
		for (Iterator<String[]> iteratorC = listaCompleta.iterator(); iteratorC.hasNext();) {
			String[] stringListaCompleta = (String[]) iteratorC.next();
			boolean repetido = false;
			for (Iterator<String[]> iteratorF = listaFiltrada.iterator(); iteratorF.hasNext();) {
				String[] stringListaFiltrada = (String[]) iteratorF.next();
				if (stringListaFiltrada[indice].equals(stringListaCompleta[indice])){
					repetido = true;
					break;
				}
			}
			if (!repetido) {
				listaFiltrada.add(stringListaCompleta);
			}
		}
		return listaFiltrada;
	}
	

}
