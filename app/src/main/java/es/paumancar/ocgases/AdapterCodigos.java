package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class AdapterCodigos  extends BaseAdapter {

    Context context;
    List<String[]> data;
    String m_inoutline_id;
    String compra_venta;
    int llenos_o_vacios;
    boolean activarCantidad;
    private TextView textoCantidadCodigos;
    private static LayoutInflater inflater = null;
    
    
    private final static int CODIGOS_LLENOS = 0;
    private final static int CODIGOS_VACIOS = 1;

    public AdapterCodigos(Context context, List<String[]> data, 
    		String m_inoutline_id, String compra_venta, int llenos_o_vacios, 
    		boolean activarCantidad, TextView textoCantidad) {
        this.context = context;
        if (data != null) {
        	this.data = data;
        } else {
        	this.data = new ArrayList<String[]>();
        }
        this.m_inoutline_id = m_inoutline_id;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.compra_venta = compra_venta;
        this.llenos_o_vacios = llenos_o_vacios;
        this.activarCantidad = activarCantidad;
        this.textoCantidadCodigos = textoCantidad;
		textoCantidadCodigos.setText(String.valueOf(getCount()));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_codigos_row, null);
        TextView textHeader = (TextView) vi.findViewById(R.id.rowTextCodigoCompra);
        final Button botonDelete = (Button) vi.findViewById(R.id.rowButtonCodigosDelete);
        final Button botonIncidencia = (Button) vi.findViewById(R.id.rowButtonCodigosIncidencia);
        final Button botonCantidad = (Button) vi.findViewById(R.id.rowButtonCodigosCantidad);
        String codigoString = data.get(position)[1];
        botonCantidad.setText(data.get(position)[0]);
        textHeader.setText(codigoString);
        MySQLiteHelper myHelper = new MySQLiteHelper(context);
        SQLiteDatabase db = myHelper.getReadableDatabase();
        int cantidadIncidencias = myHelper.cuentaIncidenciasCodigo(db, m_inoutline_id, 
        		codigoString, compra_venta, llenos_o_vacios);
        db.close();
        myHelper.close();
        String textoBotonIncidencias = "I:" +  String.valueOf(cantidadIncidencias);
        botonIncidencia.setText(textoBotonIncidencias);
        
		final int thisPosition = position;
		if(!activarCantidad){
			botonCantidad.setVisibility(View.GONE);
		} else {
			botonIncidencia.setVisibility(View.GONE);
		}
		
		botonIncidencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MySQLiteHelper myHelper = new MySQLiteHelper(context);
				SQLiteDatabase db = myHelper.getWritableDatabase();
				final List<Integer> listaIncidenciasActivas = myHelper.recuperaIncidenciasCodigo(db, m_inoutline_id, 
						data.get(thisPosition)[1], compra_venta, llenos_o_vacios);
				String[] items = myHelper.recuperaTablaIncidencias();
				db.close();
				myHelper.close();
				boolean[] itemsActivos = new boolean[items.length];
				for (int i = 0; i < items.length; i++) {
					for (int posIncidencia : listaIncidenciasActivas) {
						if (items[posIncidencia].substring(0,2).equals(items[i].substring(0,2))){
							itemsActivos[i] = true;
							break;
						}
					} 
				}
				

				AlertDialog dialog = new AlertDialog.Builder(context)
				.setTitle("LISTA DE INCIDENCIAS")
				.setMultiChoiceItems(items, itemsActivos, new DialogInterface.OnMultiChoiceClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
				        if ((isChecked) & !listaIncidenciasActivas.contains(indexSelected)) {
				        	listaIncidenciasActivas.add(indexSelected);
				        } else if (listaIncidenciasActivas.contains(indexSelected)) {
				        	int posicion = listaIncidenciasActivas.indexOf(indexSelected);
				        	listaIncidenciasActivas.remove(posicion);
				        }
				    }
				})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	String codigoString = data.get(thisPosition)[1];
				    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
						SQLiteDatabase db = myHelper.getWritableDatabase();
						myHelper.grabaIncidenciasCodigo(db, m_inoutline_id, codigoString, 
								compra_venta, llenos_o_vacios, listaIncidenciasActivas);
						int cantidadIncidencias = myHelper.cuentaIncidenciasCodigo(db, m_inoutline_id, 
					        		codigoString, compra_venta, llenos_o_vacios);
					    String textoBotonIncidencias = "I:" +  String.valueOf(cantidadIncidencias);
					    botonIncidencia.setText(textoBotonIncidencias);
						db.close();
						myHelper.close();
						dialog.cancel();
				    }
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				}).create();
				dialog.show();
				
			}
		});
		
		
		botonCantidad.setOnClickListener(new OnClickListener() {	
			
			@Override
			public void onClick(View v) {
				Button esteBoton = (Button) v;
				final EditText editTextCantidad = new EditText(context);
				LinearLayout layoutTextos = new LinearLayout(context);
				layoutTextos.addView(editTextCantidad);
				AlertDialog.Builder dialog = new AlertDialog.Builder(context);
				
				dialog.setTitle("CANTIDAD");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	try {
					    	String textoCantidad = editTextCantidad.getText().toString();
					    	int cantidadAux = Integer.valueOf(textoCantidad);
					    	String codigoBarras = data.get(thisPosition)[1];
					    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
							SQLiteDatabase db = myHelper.getWritableDatabase();
							List<String[]> listaCodigos = new ArrayList<String[]>();
							String[] codAux = {textoCantidad, codigoBarras};
							listaCodigos.add(codAux);
							switch (llenos_o_vacios) {
							case CODIGOS_LLENOS:
								myHelper.actualizaCantidadCodigosLlenos(db, m_inoutline_id, 
										codigoBarras, textoCantidad, compra_venta);
								break;
							case CODIGOS_VACIOS:
								myHelper.actualizaCantidadCodigosVacios(db, m_inoutline_id, 
										codigoBarras, textoCantidad, compra_venta);
								break;
							default:
								break;
							}
							db.close();
							myHelper.close();
							dialog.cancel();
							botonCantidad.setText(textoCantidad);
				    	}
				    	catch(NumberFormatException e){
				    		 dialog.cancel();
				    	}

				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
				editTextCantidad.setText(esteBoton.getText().toString());
				editTextCantidad.setSelection(editTextCantidad.getText().length());
				
			}
		});
		
        botonDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MySQLiteHelper myHelper = new MySQLiteHelper(context);
				SQLiteDatabase db = myHelper.getWritableDatabase();
				switch (llenos_o_vacios) {
				case CODIGOS_LLENOS:
					myHelper.borraCodigoLleno(db, m_inoutline_id, data.get(thisPosition)[1], compra_venta);
				    data = myHelper.recuperaCodigosLlenos(db, m_inoutline_id, compra_venta);
					break;
				
				case CODIGOS_VACIOS:
					myHelper.borraCodigoVacio(db, m_inoutline_id, data.get(thisPosition)[1], compra_venta);
				    data = myHelper.recuperaCodigosVacios(db, m_inoutline_id, compra_venta);
					break;

				default:
					break;
				}

			    db.close();
			    myHelper.close();
				//data.remove(thisPosition);
				notifyDataSetChanged(); 
				textoCantidadCodigos.setText(String.valueOf(getCount()));
			}
		});
        return vi;
    }
    
   
}
