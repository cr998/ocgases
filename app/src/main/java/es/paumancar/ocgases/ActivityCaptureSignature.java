package es.paumancar.ocgases;



import java.io.IOException;
import java.util.ArrayList;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.honeywell.decodemanager.DecodeManager;
import com.honeywell.decodemanager.barcode.DecodeResult;

import es.paumancar.ocgases.reports.PrintFormats;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Implements the UI for the user to sign and save the signature.
 */
@SuppressLint("NewApi")
public class ActivityCaptureSignature extends Activity{
	
	
	private final int ID_SCANSETTING = 0x12;
	//private final int ID_CLEAR_SCREEN = 0x13;

	private DecodeManager mDecodeManager = null;
	
    private final int SCANKEY = 0x94;
	private final int SCANTIMEOUT = 5000;
	long mScanAccount = 0;
	private boolean mbKeyDown = true;

	SharedPreferences sharedPref;
	
    private static final String TAG = "BarcodeMain";
	

    boolean escanearConCCD = false;
    
    Button botonEscanea;
	
	
	public static final String BASE64_SIGNATURE_KEY = "com.intermec.lineprint.sample.Base64Signature";
	public static final String NUMERO_ALBARAN_KEY = "es.paumancar.ocgases.numalbkey";
	public static final int RC_BARCODE_CAPTURE = 9001;
	
	private SignatureView signView;
	private EditText editNumeroAlbaran;

	private String codigoDevuelto;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_capture_signature);
		signView = (SignatureView) findViewById(R.id.viewCaptureSignature);
		editNumeroAlbaran = (EditText) findViewById(R.id.editCaptSignNumeroAlbaran);
		botonEscanea = (Button) findViewById(R.id.buttonCaptSignScanBarcode);
		
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        
        final LinearLayout layoutAceptLOPD = (LinearLayout) findViewById(R.id.linearLayoutWebViewLOPDSign);
        final LinearLayout layoutSignView = (LinearLayout) findViewById(R.id.linearLayoutCaptSign);
        final Button buttonAceptLOPD = (Button) findViewById(R.id.buttonLOPDAccept);
        final Button buttonCancelLOPD = (Button) findViewById(R.id.buttonLOPDCancel);
        final LinearLayout layoutButtonsSignClear = (LinearLayout) findViewById(R.id.linearLayoutCaptSignClear);
        final SignatureView signView = (SignatureView) findViewById(R.id.viewCaptureSignature);
        final LinearLayout layoutCaptureSign = (LinearLayout) findViewById(R.id.linearLayoutAlbViewCaptureSignature);
        
        layoutSignView.setVisibility(View.INVISIBLE);
        
        final WebView webViewLOPD = (WebView) findViewById(R.id.webViewLOPDSign);
		
		WebSettings settings = webViewLOPD.getSettings();
		settings.setUseWideViewPort(false);
		String leyOPDHtml = PrintFormats.TEXTO_LOPD_LSSI_1;
		leyOPDHtml = leyOPDHtml.replaceAll("á", "&aacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("é", "&eacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("í", "&iacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("ó", "&oacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("ú", "&uacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Á", "&Aacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("É", "&Eacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Í", "&Iacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Ó", "&Oacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Ú", "&Uacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("ñ", "&ntilde;");
		leyOPDHtml = leyOPDHtml.replaceAll("Ñ", "&Ntilde;");
		webViewLOPD.loadData(leyOPDHtml, "text/html", "UTF-8");
		
		buttonAceptLOPD.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				layoutAceptLOPD.setVisibility(View.INVISIBLE);
				layoutSignView.setVisibility(View.VISIBLE);
			}
		});
		
		buttonCancelLOPD.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				layoutAceptLOPD.setVisibility(View.INVISIBLE);
				layoutSignView.setVisibility(View.VISIBLE);
				signView.setVisibility(View.INVISIBLE);
				layoutButtonsSignClear.setVisibility(View.INVISIBLE);
			}
		});
		
		initializeUI();
		
	}
	
	
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		switch (keyCode) {
		case KeyEvent.KEYCODE_ENTER:
			if (escanearConCCD){
				try {
					if (mbKeyDown) {
						DoScan();
						mbKeyDown = false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return true;
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			return true;			
        case KeyEvent.KEYCODE_UNKNOWN:	
        	if(event.getScanCode() == SCANKEY || event.getScanCode() == 87 || event.getScanCode() == 88) {
        		if (escanearConCCD){
	        		try {
						if (mbKeyDown) {
							DoScan();
							mbKeyDown = false;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
        		}
        	}
        	return true;
		default:
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_ENTER:
			if (escanearConCCD){
				try {
					mbKeyDown = true;
					if (escanearConCCD){
						cancelScan();
					} else {
						//iniciaCamara();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return true;
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			return true;
			
        case KeyEvent.KEYCODE_UNKNOWN:	
        	if(event.getScanCode() == SCANKEY  || event.getScanCode() == 87 || event.getScanCode() == 88) {
        		if (escanearConCCD){
	        		try {
	    				mbKeyDown = true;
	    				cancelScan();
	    			} catch (Exception e) {
	    				e.printStackTrace();
	    			}
        		} else {
					iniciaCamara();
				}
        	}
        	return true;
		default:
			return super.onKeyUp(keyCode, event);
		}
	}
    
    
	@Override
	protected void onResume() {
		super.onResume();
        String escanerDefecto = sharedPref.getString(TagsPreferencias.ESCANER, "");
        escanearConCCD = escanerDefecto.equals(TagsPreferencias.ESCANER_VALOR_LECTOR)?true:false;
        if (mDecodeManager != null) {
			Log.e(TAG, "Existe DecodeManager onResume");
		}
		if (escanearConCCD){
			if (mDecodeManager == null) {
				mDecodeManager = new DecodeManager(this,ScanResultHandler);
			}
			
			SoundManager.getInstance();
			SoundManager.initSounds(getBaseContext());
			SoundManager.loadSounds();
		} else {
			if (mDecodeManager != null) {
				try {
					mDecodeManager.release();
					mDecodeManager = null;
				} catch (IOException e) {
					Log.e(TAG, "Error al cerrar DecodeManager onResume con Camara");
					e.printStackTrace();
				}
			}
		}
	}

	

	@Override
	protected void onPause() {
		super.onPause();
		if (mDecodeManager != null) {
			try {
				mDecodeManager.release();
				mDecodeManager = null;
			} catch (IOException e) {
				Log.e(TAG, "Error al cerrar DecodeManager onPause");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (escanearConCCD){
			SoundManager.cleanup();
		}
		if (mDecodeManager != null) {
			try {
				mDecodeManager.release();
				mDecodeManager = null;
			} catch (IOException e) {
				Log.e(TAG, "Error al cerrar DecodeManager onDestroy");
				e.printStackTrace();
			}
		}
	}



	
	public void onCaptSignSaveButtonClicked (View view)
	{
		String base64Jpg = signView.getBase64EncodedJPG();

		Intent intent = new Intent();
		if ((base64Jpg != null))
		{
			intent.putExtra(BASE64_SIGNATURE_KEY, base64Jpg);
			intent.putExtra(NUMERO_ALBARAN_KEY, editNumeroAlbaran.getText().toString());
			setResult(RESULT_OK,intent);
		} else {
			setResult(RESULT_CANCELED,intent);
		}


		this.finish();
	}

	public void onCaptSignClearButtonClicked (View view)
	{
		signView.clear();
	}

	public void onCaptSignCancelButtonClicked (View view)
	{
		Intent intent = new Intent();
		setResult(RESULT_CANCELED,intent);
		this.finish();
	}
	
	   
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case RC_BARCODE_CAPTURE:
			if (CommonStatusCodes.SUCCESS == resultCode)
			{
				if (data != null) {
					Barcode barcode = data.getParcelableExtra(ActivityBarcodeCapture.BarcodeObject);
	                codigoDevuelto = barcode.displayValue;
					editNumeroAlbaran.setText(codigoDevuelto);
				}
			}
			break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		if (escanearConCCD){
			menu.add(0, ID_SCANSETTING, 0, R.string.SymbologySettingMenu);
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case ID_SCANSETTING: {
			mDecodeManager.getSymConfigActivityOpeartor().start();
			 return true;
		}

		default:
			return false;
		}
	}
	
	

	private void initializeUI() {
		//final Button button = (Button) findViewById(R.id.scanbutton);
		//mDecodeResultEdit = (EditText) findViewById(R.id.edittext_scanresult);
		botonEscanea.setOnTouchListener(new Button.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				final int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					//button.setBackgroundResource(R.drawable.android_pressed);
					if (escanearConCCD){
						try {
							DoScan();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					break;
				case MotionEvent.ACTION_UP:
					if (escanearConCCD){
						try {
							//button.setBackgroundResource(R.drawable.android_normal);
							cancelScan();
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						iniciaCamara();
					}
					break;
				}
				return true;
			}
		});
	}

	private void DoScan() throws Exception {
		try {
			mDecodeManager.doDecode(SCANTIMEOUT);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	private void iniciaCamara(){
		Intent intent = new Intent(getApplicationContext(), ActivityBarcodeCapture.class);
        intent.putExtra(ActivityBarcodeCapture.AutoFocus, true);
        intent.putExtra(ActivityBarcodeCapture.UseFlash, false);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
	}

	private Handler ScanResultHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DecodeManager.MESSAGE_DECODER_COMPLETE:
				mScanAccount++;
				DecodeResult decodeResult = (DecodeResult) msg.obj;
				editNumeroAlbaran.setText(decodeResult.barcodeData);
				SoundManager.playSound(1, 1);			 
				break;

			case DecodeManager.MESSAGE_DECODER_FAIL: {
				SoundManager.playSound(2, 1);
			}
			break;
			case DecodeManager.MESSAGE_DECODER_READY:
			{
				ArrayList<java.lang.Integer> arry =  mDecodeManager.getSymConfigActivityOpeartor().getAllSymbologyId();
				boolean b = arry.isEmpty();
			}
			break;
			default:
				super.handleMessage(msg);
				break;
			}
		}
	};
	
	private void cancelScan() throws Exception {
		mDecodeManager.cancelDecode();
	}
	



}
