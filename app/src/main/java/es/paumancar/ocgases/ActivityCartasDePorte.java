package es.paumancar.ocgases;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.paumancar.ocgases.datasync.Connectivity;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;

@SuppressLint("NewApi")
public class ActivityCartasDePorte extends Activity {

	MyWheelView wheelCartasDePorte;
	MySpinner spinnerCartasDePorte;
	ListView listaAlbaranesVenta;
	TextView textoDescripcion;

	ListaResultados listaResultados;

	private boolean scrolling = false;

	SharedPreferences sharedPref;
	
	ProgressDialog dialogProgreso;

	String[][] elemento_Etiquetas_Recupera = {
			{ "CartaDePorte", 
				"m_inout_porte_id", 
				"documentno", 
				"numero",
				"m_inout_id", 
				"c_bpartner_id" },

			{ "AlbaranVentaCabecera", 
					"m_inout_id", 
					"documentno",
					"tipoventa",
					"fecha",
					"n_pedido_cliente",
					"c_bpartner_id", 
					"nombrecomercial", 
					"cif",
					"telefono_e_comerciales", 
					"direccion", 
					"ciudad", 
					"ayuda_entrega",
					"latitud",
					"longitud",
					"c_doctype_id",
					"c_invoiceschedule_id",
					"totallines",
					"ismoroso",
					"usuario",
					"formapago",
					"vencimiento",
					"nombrefiscal",
					"m_freightcostrule_id", 
					"cdgcafentidad", 
					"codigoalternativo",
					"codigoairliquid"} };
	
	/*
//	 * 
 * 
<c_bpartner_location_id>1005466</c_bpartner_location_id>
<dropship_location_id>1005466</dropship_location_id>
<descripcion/>
<ayuda_entrega>vienen por aqui o se lleva a obra</ayuda_entrega>
<distribuido>Y</distribuido>
<ref_proveed/>
<totallines>76.32</totallines>
<grandtotal>92.35</grandtotal>
<numero_instalador/>
<nombrefiscal/>
<codigoalternativo>86</codigoalternativo>
<nombrelocalizfact/>
<detalleentrega/>
<persnacontacto2/>
<codigoairliquid/>
<direccion2/>
<region>Asturias</region>
<codigopostal>33011</codigopostal>
	 */
	
	String[] cabeceraColumnasAlbaran = new String[elemento_Etiquetas_Recupera[1].length - 1];

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cartas_layout);
		
		Button botonLlevame = (Button) findViewById(R.id.buttonUbicacionPorteLlevame);
		
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

		for (int i = 0; i < (elemento_Etiquetas_Recupera[1].length - 1); i++) {
			cabeceraColumnasAlbaran[i] = elemento_Etiquetas_Recupera[1][i + 1];
		}

		textoDescripcion = (TextView) findViewById(R.id.textViewUbicacionPorteDescripcion);
		
		wheelCartasDePorte = (MyWheelView) this
				.findViewById(R.id.wheelCartaDePorte);
		
		spinnerCartasDePorte = (MySpinner) this
				.findViewById(R.id.spinnerCartaDePorte);
		// wheelCartasDePorte.context = getApplicationContext();
		listaAlbaranesVenta = (ListView) findViewById(R.id.listviewAlbaranesVenta);
		RecuperaDatosCartasDePorte recuperaDatos = new RecuperaDatosCartasDePorte(
				getApplicationContext());
		recuperaDatos.execute("");
		
		spinnerCartasDePorte.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				updateAlbaranes(spinnerCartasDePorte.getIdCarta(position));
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		botonLlevame.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					Button b = (Button)v;
					String textoUbicacion = textoDescripcion.getText().toString();
					int indexAux = textoUbicacion.indexOf("Lat:");
					String latLong = textoUbicacion.substring(indexAux);
					String[] latLongArray = latLong.split(", ");
					indexAux = latLongArray[0].indexOf(":");
					String latitudString = latLongArray[0].substring(indexAux + 1).replace(",", ".");
					indexAux = latLongArray[1].indexOf(":");
					String longitudString = latLongArray[1].substring(indexAux + 1).replace(",", ".");
					String latLongString = latitudString + "," + longitudString;
					try {
						//double latitudDob = Double.valueOf(latitudString);
						//double longituDob = Double.valueOf(longitudString);
						Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latLongString);
						Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
						mapIntent.setPackage("com.google.android.apps.maps");
						startActivity(mapIntent);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
/*
		wheelCartasDePorte.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				if (!scrolling) {
					updateAlbaranes(wheelCartasDePorte.getIdCarta(newValue));
				}
			}

		});

		wheelCartasDePorte.addScrollingListener(new OnWheelScrollListener() {
			public void onScrollingStarted(WheelView wheel) {
				scrolling = true;
			}

			public void onScrollingFinished(WheelView wheel) {
				scrolling = false;
				updateAlbaranes(wheelCartasDePorte
						.getIdCarta(wheelCartasDePorte.getCurrentItem()));
			}
		});*/

	}

	class RecuperaDatosCartasDePorte extends
			AsyncTask<String, String, ListaResultados> {


		String direccionIP;
		Context context;
		// Intent intentNext;
		Class<?> NextClass;
		int filtroId;
		String valorFiltro;
		int nextFiltroId;

		public RecuperaDatosCartasDePorte(Context context) {
			super();
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			dialogProgreso = ProgressDialog.show(ActivityCartasDePorte.this, "", 
		            "Descargando datos ...", true);
			dialogProgreso.setCancelable(true);
			super.onPreExecute();
		}

		@Override
		protected ListaResultados doInBackground(String... StringDatos) {
			List<String[]> resultadoCartas = null;
			List<String[]> resultadoAlbaranes = null;
			List<String[]>[] resultadoCartasAlbaranes = null;
			// listaDatos = datosRecuperaArray[0].getLista();
			// elementoRecuperaCarta = "CartaDePorte";
			// elementoRecuperaAlbaran = "AlbaranVentaCabecera";
			// etiquetasRecuperaCartaDePorte =
			// datosRecuperaArray[0].getEtiquetasRecupera();
			//direccionIP = sharedPref.getString("direccion_ip", "localhost");
			// intentNext = datosRecuperaArray[0].getIntentNext();
			// NextClass = datosRecuperaArray[0].getNextClass();
			direccionIP = Connectivity.servidorActivo(getApplicationContext());
			filtroId = -1;
			valorFiltro = "";
			nextFiltroId = 0;

			String ficheroString = getApplicationContext().getFilesDir().getAbsolutePath() + "/" 
					+ elemento_Etiquetas_Recupera[0][0] + "_"
					+ elemento_Etiquetas_Recupera[1][0] + ".xml";


			try {

				File ficheroXml = new File(ficheroString);
	            InputStream in = new BufferedInputStream(new FileInputStream(ficheroXml));

//				in = new BufferedInputStream(con.getInputStream());

				XMLParserPedidoCompraCabecera parser = new XMLParserPedidoCompraCabecera(
							elemento_Etiquetas_Recupera, -1, "");

				resultadoCartasAlbaranes = parser.recuperaLista(in);
				resultadoCartas = resultadoCartasAlbaranes[0];
				resultadoAlbaranes = resultadoCartasAlbaranes[1];

			} catch (Exception e) {
				Log.e("OCGASES", e.getMessage());

			} 
			
			ListaResultados resultados = new ListaResultados(resultadoCartas,
					resultadoAlbaranes);

			return resultados;
		}

		@Override
		protected void onPostExecute(ListaResultados result) {
			super.onPostExecute(result);
			dialogProgreso.cancel();
			listaResultados = result;
			//wheelCartasDePorte.setMyAdapter(result.getArrayCartas());
			if (result.getArrayCartas() != null){
				spinnerCartasDePorte.setMyAdapter(result.getArrayCartas());
				try {
					String idCartaDePorte = result.getArrayCartas().get(0)[0];
					updateAlbaranes(idCartaDePorte);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	/********************************************
	 * CLASES
	 *
	 ******************************************/

	class ListaResultados {
		List<String[]> arrayCartas;
		List<String[]> arrayAlbaranes;

		public ListaResultados(List<String[]> arrayCartas,
				List<String[]> arrayAlbaranes) {
			super();
			this.arrayCartas = arrayCartas;
			this.arrayAlbaranes = arrayAlbaranes;
		}

		public List<String[]> getArrayCartas() {
			return arrayCartas;
		}

		public void setArrayCartas(List<String[]> arrayCartas) {
			this.arrayCartas = arrayCartas;
		}

		public List<String[]> getArrayAlbaranes() {
			return arrayAlbaranes;
		}

		public void setArrayAlbaranes(List<String[]> arrayAlbaranes) {
			this.arrayAlbaranes = arrayAlbaranes;
		}

	}

	/********************************************
	 * FUNCIONES
	 *
	 ******************************************/

	protected void updateAlbaranes(String idCartaDePorte) {
		List<String[]> listaAlbaranes = new ArrayList<String[]>();
		for (Iterator iteratorCarta = listaResultados.getArrayCartas()
				.iterator(); iteratorCarta.hasNext();) {
			String[] cartaDePorte = (String[]) iteratorCarta.next();
			if (cartaDePorte[0].equals(idCartaDePorte)) {
				for (Iterator iteratorAlbaranes = listaResultados
						.getArrayAlbaranes().iterator(); iteratorAlbaranes
						.hasNext();) {
					String[] albaran = (String[]) iteratorAlbaranes.next();
					if (cartaDePorte[3].equals(albaran[0])) {
						listaAlbaranes.add(albaran);
						break;
					}
				}
			}
		}
		listaAlbaranesVenta.setAdapter(new AdapterVentas(
				getApplicationContext(), listaAlbaranes,
				cabeceraColumnasAlbaran, ActivityAlbaranesVentaLineas.class,
				-1, 0, textoDescripcion));
		textoDescripcion.setText("");
	}

}
