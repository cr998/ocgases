package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentCodigosVacios extends Fragment {
	
	private List<String[]> codigosVacios;
	private MyListView listviewCodigosVacios;
	private String m_inoutline_id;
	private String compra_venta;
	private TextView textoTab;
	private boolean activarCantidad;
	private String cantidad;
	private String descripcion;
	private String etiquetaLeidos;
	private String documento;
	
	private TextView textoDescripcion;
	private TextView textoCantidad;
	private TextView textoLeidos;
	private TextView textoLeidosEtiqueta;
	
	private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
	
    private final static int CODIGOS_LLENOS = 0;
    private final static int CODIGOS_VACIOS = 1;
    
    public Button botonAddCodeBar;
    public EditText editTextAddCodeBar;
    
	public FragmentCodigosVacios(List<String[]> codigosVacios, 
			String m_inoutline_id, String documento, String compra_venta, boolean activarCantidad, 
			String cantidad, String descripcion, String etiquetaLeidos) {
		super();
		if (codigosVacios != null){
			this.codigosVacios = codigosVacios;
		} else {
			this.codigosVacios = new ArrayList<String[]>();
		}
		this.m_inoutline_id = m_inoutline_id;
		this.compra_venta = compra_venta;
		this.activarCantidad = activarCantidad;
		this.cantidad = cantidad;
		this.descripcion = descripcion;
		this.etiquetaLeidos = etiquetaLeidos;
		this.documento = documento;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {


	    View rootView = inflater.inflate(R.layout.tab_codigosvacios, container,
	            false);
	   
	    return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);   
        MySQLiteHelper myHelper = new MySQLiteHelper(getContext());
		textoLeidosEtiqueta = (TextView) view.findViewById(R.id.textoTabCodigosVaciosLeidosNombre);
        textoDescripcion = (TextView) view.findViewById(R.id.textoTabCodigosVaciosDescripcion);
        textoCantidad = (TextView) view.findViewById(R.id.textoTabCodigosVaciosCantidad);
        textoLeidos = (TextView) view.findViewById(R.id.textoTabCodigosVaciosLeidos);
        Button botonEscaneaVacios = (Button) view.findViewById(R.id.buttonTabCodVaciosScanCamara);
        botonAddCodeBar = (Button) view.findViewById(R.id.buttonTabCodVaciosCodeTextAdd);
        editTextAddCodeBar = (EditText) view.findViewById(R.id.editTextTabCodVaciosCodeText);
		SQLiteDatabase db = myHelper.getWritableDatabase();
        myHelper.actualizaCodigosVacios(db, m_inoutline_id, documento, descripcion,
        		codigosVacios, compra_venta);
        codigosVacios = myHelper.recuperaCodigosVacios(db, m_inoutline_id, compra_venta);
        db.close();
        myHelper.close();
		listviewCodigosVacios = (MyListView) view.findViewById(R.id.listviewCodigosCompraVacios);
        listviewCodigosVacios.setAdapter(new AdapterCodigos(getContext(), 
        		codigosVacios, m_inoutline_id, compra_venta, CODIGOS_VACIOS, activarCantidad, 
        		textoLeidos));
        textoCantidad.setText(cantidad);
        textoDescripcion.setText(descripcion);
        textoLeidosEtiqueta.setText(etiquetaLeidos);
        
        botonAddCodeBar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String textoCodigo = editTextAddCodeBar.getText().toString();
		      	insertaCodigoEnLista("1", textoCodigo);
		      	//editTextAddCodeBar.requestFocus();
		      	editTextAddCodeBar.setText("");
			}
		});

        editTextAddCodeBar.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View editText, int keyCode, KeyEvent event) {
				 if ((event.getAction() == KeyEvent.ACTION_UP) &&
				            (keyCode == KeyEvent.KEYCODE_ENTER))  { 
					 		String textoCodigo = ((EditText) editText).getText().toString();
					      	insertaCodigoEnLista("1", textoCodigo);
					      	editText.requestFocus();
					      	((EditText) editText).setText("");
					      	return true;
					   }
				return false;
			}

		});
        
        
        botonEscaneaVacios.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(), ActivityBarcodeCapture.class);
	            intent.putExtra(ActivityBarcodeCapture.AutoFocus, true);
	            intent.putExtra(ActivityBarcodeCapture.UseFlash, false);

	            startActivityForResult(intent, RC_BARCODE_CAPTURE);
				
			}
		});

	}
	
//	@Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == RC_BARCODE_CAPTURE) {
//            if (resultCode == CommonStatusCodes.SUCCESS) {
//                if (data != null) {
//                    Barcode barcode = data.getParcelableExtra(ActivityBarcodeCapture.BarcodeObject);
//                    //statusMessage.setText(R.string.barcode_success);
//                    //barcodeValue.setText(barcode.displayValue);
//                    String codigoDevuelto = barcode.displayValue;
//                    insertaCodigoEnLista("1", codigoDevuelto);
//                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
//                } else {
//                    //statusMessage.setText(R.string.barcode_failure);
//                    Log.d(TAG, "No barcode captured, intent data is null");
//                }
//            } else {
//                //statusMessage.setText(String.format(getString(R.string.barcode_error),
//                //        CommonStatusCodes.getStatusCodeString(resultCode)));
//            }
//        }
//        else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }
	
	public void setCodigos(List<String[]> codigosVacios){
		this.codigosVacios = codigosVacios;
		if (listviewCodigosVacios.getAdapter() != null){
			((AdapterCodigos)listviewCodigosVacios.getAdapter()).notifyDataSetChanged();
		} else {
			listviewCodigosVacios.setAdapter(new AdapterCodigos(getContext(), 
					this.codigosVacios, this.m_inoutline_id, compra_venta, CODIGOS_VACIOS, activarCantidad, 
					textoLeidos));
		}
		updateCantidad();
	}
	

	public void insertaCodigoEnLista(String cantidad, String textoCodigo) {
        if (!textoCodigo.equals("")){
            List<String[]> listaCodigos = new ArrayList<String[]>();
            String[] codAux = {cantidad, textoCodigo};
            listaCodigos.add(codAux);
            MySQLiteHelper myHelper = new MySQLiteHelper(getContext());
            SQLiteDatabase db = myHelper.getWritableDatabase();
            myHelper.actualizaCodigosVacios(db, m_inoutline_id, documento, descripcion,
            		listaCodigos, compra_venta);
            codigosVacios = myHelper.recuperaCodigosVacios(db, m_inoutline_id, compra_venta);
            db.close();
            myHelper.close();
            listviewCodigosVacios.setAdapter(new AdapterCodigos(getContext(), 
            		codigosVacios, m_inoutline_id, compra_venta, CODIGOS_VACIOS, activarCantidad, 
            		textoLeidos));
        }
		updateCantidad();
	}
	
	private void updateCantidad() {
		textoLeidos.setText(String.valueOf(listviewCodigosVacios.getAdapter().getCount()));
		}
	
	public void setDescripcion(String textoDesc){
		textoDescripcion.setText(textoDesc);
	}
	
	public void setCantidad(String textCantidad){
		textoCantidad.setText(textCantidad);
	}

}
