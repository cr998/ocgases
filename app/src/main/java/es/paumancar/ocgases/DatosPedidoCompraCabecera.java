package es.paumancar.ocgases;

public class DatosPedidoCompraCabecera {
	String c_order_id;
	String documentno;
	String created;
	String nombreproveedor;
	
	
	
	public DatosPedidoCompraCabecera(String c_order_id, String documentno,
			String created, String nombreproveedor) {
		super();
		this.c_order_id = c_order_id;
		this.documentno = documentno;
		this.created = created;
		this.nombreproveedor = nombreproveedor;
	}
	public String getC_order_id() {
		return c_order_id;
	}
	public void setC_order_id(String c_order_id) {
		this.c_order_id = c_order_id;
	}
	
	public String getDocumentno() {
		return documentno;
	}
	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}
	
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
	public String getNombreproveedor() {
		return nombreproveedor;
	}
	public void setNombreproveedor(String nombreproveedor) {
		this.nombreproveedor = nombreproveedor;
	}
	

}
