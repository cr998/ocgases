package es.paumancar.ocgases;

import com.honeywell.decodemanager.DecodeManager;

import es.paumancar.ocgases.datasync.Connectivity;
import es.paumancar.ocgases.datasync.MySQLiteHelperSync;
import es.paumancar.ocgases.datasync.RecuperaFichero;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

@SuppressLint("NewApi")
public class ActivityPreferenciasGenerales extends Activity {
	
	SharedPreferences sharedPref;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
			
			super.onCreate(savedInstanceState);
	        setContentView(R.layout.preferencias_generales);
	        
	        Button botonServidores = (Button) findViewById(R.id.buttonPreferenciasServidores);
	        Button botonRedLocal = (Button) findViewById(R.id.buttonPreferenciasRedLocal);
	        Button botonPassword = (Button) findViewById(R.id.buttonPreferenciasPassword);
	        Button botonPrinters = (Button) findViewById(R.id.buttonPreferenciasPrinters);
	        Button botonRecuperaCodigos = (Button) findViewById(R.id.buttonPreferenciasRecuperaCodigos);
	        Button botonDiasDatos = (Button) findViewById(R.id.buttonPreferenciasDiasGuardarDB);
	        Button botonBorrarCola = (Button) findViewById(R.id.buttonPreferenciasBorrarColaEnvios);
	        Button botonActualizaPortes = (Button) findViewById(R.id.buttonPreferenciasRecuperaReglasPortes);
	        
	        RadioButton radioButtonCamara = (RadioButton) findViewById(R.id.radioPreferenciasCamara);
	        RadioButton radioButtonLector = (RadioButton) findViewById(R.id.radioPreferenciasLector);
	        
	        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
	        
	        botonServidores.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intentPreferencias = new Intent(getApplicationContext(), ActivityPreferenciasServidores.class);
					startActivity(intentPreferencias);
					
				}
			});
	        
	        botonRecuperaCodigos.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intentRecuperaCodigos = new Intent(getApplicationContext(),
							 ActivityRecuperaCodigos.class);
					startActivity(intentRecuperaCodigos);
					
				}
			});
	        
	        botonRedLocal.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					final EditText editTextRedLocal = new EditText(ActivityPreferenciasGenerales.this);
					LinearLayout layoutTextos = new LinearLayout(ActivityPreferenciasGenerales.this);
					LinearLayout.LayoutParams layoutForInner = 
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
									LinearLayout.LayoutParams.WRAP_CONTENT);
					layoutTextos.setLayoutParams(layoutForInner);
					layoutTextos.addView(editTextRedLocal);
					LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
					editTextRedLocal.setLayoutParams(params);
					AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityPreferenciasGenerales.this);
					
					dialog.setTitle("RED LOCAL");
					dialog.setView(layoutTextos);
					dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int id) {
					    	String textoRedLocal = editTextRedLocal.getText().toString(); 
					    	Editor editor = sharedPref.edit();
					    	editor.putString(TagsPreferencias.RED_LOCAL, textoRedLocal);
					    	editor.commit();
							dialog.cancel();
					    }
					});
					dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int id) {
					        dialog.cancel();
					    }
					});
					dialog.create();
					dialog.show();
					editTextRedLocal.setText(sharedPref.getString(TagsPreferencias.RED_LOCAL, 
							Connectivity.RED_WIFI_PROPIA));
				}
			});
	        
	        botonPassword.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					final TextView TextPass1 = new TextView(ActivityPreferenciasGenerales.this);
					final TextView TextPass2 = new TextView(ActivityPreferenciasGenerales.this);
					final EditText editTextPass1 = new EditText(ActivityPreferenciasGenerales.this);
					final EditText editTextPass2 = new EditText(ActivityPreferenciasGenerales.this);
					TextPass1.setText("Nueva_Pass:  ");
					TextPass2.setText("Repita_Pass: ");
					editTextPass1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
					editTextPass2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
					
					LinearLayout layoutTextos = new LinearLayout(ActivityPreferenciasGenerales.this);
					LinearLayout.LayoutParams layoutForInner = 
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
									LinearLayout.LayoutParams.WRAP_CONTENT);
					layoutTextos.setLayoutParams(layoutForInner);
					layoutTextos.setOrientation(LinearLayout.VERTICAL);
					
					LayoutParams params5Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 5f);
					
					LinearLayout layoutPass1 = new LinearLayout(ActivityPreferenciasGenerales.this);
					layoutPass1.setLayoutParams(layoutForInner);
					layoutPass1.setOrientation(LinearLayout.HORIZONTAL);
					TextPass1.setLayoutParams(params5Weight);
					editTextPass1.setLayoutParams(params5Weight);
					layoutPass1.addView(TextPass1);
					layoutPass1.addView(editTextPass1);
					
					LinearLayout layoutPass2 = new LinearLayout(ActivityPreferenciasGenerales.this);
					layoutPass2.setLayoutParams(layoutForInner);
					layoutPass2.setOrientation(LinearLayout.HORIZONTAL);
					TextPass2.setLayoutParams(params5Weight);
					editTextPass2.setLayoutParams(params5Weight);
					layoutPass2.addView(TextPass2);
					layoutPass2.addView(editTextPass2);
					
					layoutTextos.addView(layoutPass1);
					layoutTextos.addView(layoutPass2);
					
					AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityPreferenciasGenerales.this);
					
					dialog.setTitle("CAMBIO DE CONTRASEÑA");
					dialog.setView(layoutTextos);
					dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int id) {
					    	String textoPass1 = editTextPass1.getText().toString(); 
					    	String textoPass2 = editTextPass2.getText().toString(); 
					    	if (textoPass1.equals(textoPass2)){
					    		MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
								SQLiteDatabase db = myHelper.getWritableDatabase();
								boolean exitoAlAct = myHelper.actualizaPassword(db, textoPass1);
								db.close();
								myHelper.close();
								if (exitoAlAct){
									Toast.makeText(getApplicationContext(), "Contraseña actualizada", Toast.LENGTH_LONG).show();
								} else {
									Toast.makeText(getApplicationContext(), "Error al actualizar", Toast.LENGTH_LONG).show();
								}
					    	} else {
					    		editTextPass1.setText("");
					    		editTextPass2.setText("");
					    		Toast.makeText(getApplicationContext(), "Contraseñas distintas", Toast.LENGTH_LONG).show();
					    	}
					    }
					});
					dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int id) {
					        dialog.cancel();
					    }
					});
					dialog.create();
					dialog.show();
				}
			});
	        
	        boolean existeLectorCCD = false;
	        try {
				DecodeManager mDecoderTest = new DecodeManager(this, new Handler());
				existeLectorCCD = true;
				mDecoderTest.release();
				mDecoderTest = null;
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        String escanerDefecto = sharedPref.getString(TagsPreferencias.ESCANER, TagsPreferencias.ESCANER_VALOR_CAMARA);
	        boolean escanearConCCD = escanerDefecto.equals(TagsPreferencias.ESCANER_VALOR_LECTOR)?true:false;
	        if (existeLectorCCD){
	        	radioButtonLector.setChecked(escanearConCCD);
	        	radioButtonCamara.setChecked(!escanearConCCD);
	        } else {
	        	escanearConCCD = false;
	        	radioButtonLector.setEnabled(false);
	        	radioButtonLector.setChecked(false);
	        	radioButtonCamara.setChecked(true);
	        	Editor editor = sharedPref.edit();
		    	editor.putString(TagsPreferencias.ESCANER, TagsPreferencias.ESCANER_VALOR_CAMARA);
		    	editor.commit();
	        }
	        
	        radioButtonCamara.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked){
						Editor editor = sharedPref.edit();
				    	editor.putString(TagsPreferencias.ESCANER, TagsPreferencias.ESCANER_VALOR_CAMARA);
				    	editor.commit();
					}
				}
			});
	        
	        
	        radioButtonLector.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked){
						Editor editor = sharedPref.edit();
				    	editor.putString(TagsPreferencias.ESCANER, TagsPreferencias.ESCANER_VALOR_LECTOR);
				    	editor.commit();
					}
				}
			});
	        
	        botonPrinters.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intentPreferenciasPrinters = new Intent(getApplicationContext(), ActivityPreferenciasPrinters.class);
					startActivity(intentPreferenciasPrinters);
					
				}
			});
	        
	        botonDiasDatos.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					final EditText editTextDiasDatos = new EditText(ActivityPreferenciasGenerales.this);
					LinearLayout layoutTextos = new LinearLayout(ActivityPreferenciasGenerales.this);
					LinearLayout.LayoutParams layoutForInner = 
							new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
									LinearLayout.LayoutParams.WRAP_CONTENT);
					layoutTextos.setLayoutParams(layoutForInner);
					layoutTextos.addView(editTextDiasDatos);
					LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
					editTextDiasDatos.setLayoutParams(params);
					AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityPreferenciasGenerales.this);
					
					dialog.setTitle("DIAS GUARDADO DATOS");
					dialog.setView(layoutTextos);
					dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int id) {
					    	String textoRedLocal = editTextDiasDatos.getText().toString(); 
					    	Editor editor = sharedPref.edit();
					    	editor.putString(TagsPreferencias.DIAS_GUARDADO_DATOS, textoRedLocal);
					    	editor.commit();
							dialog.cancel();
					    }
					});
					dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int id) {
					        dialog.cancel();
					    }
					});
					dialog.create();
					dialog.show();
					editTextDiasDatos.setText(sharedPref.getString(TagsPreferencias.DIAS_GUARDADO_DATOS, "3"));
				}
			});
	        
	        botonBorrarCola.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MySQLiteHelperSync mySyncHelper = new MySQLiteHelperSync(getApplicationContext());
					SQLiteDatabase db = mySyncHelper.getWritableDatabase();
					mySyncHelper.borraTodosDatosEnviados(db);
					db.close();
					mySyncHelper.close();
				}
			});
	        
			botonActualizaPortes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					RecuperaFichero recuperaTablaPortes = new RecuperaFichero(ActivityPreferenciasGenerales.this);
					String[] datosTablaPortes = {"TablaPortes"};
					recuperaTablaPortes.execute(datosTablaPortes);
					
					RecuperaFichero recuperaReglasPortes = new RecuperaFichero(ActivityPreferenciasGenerales.this);
					String[] datosReglasPortes = {"ReglasPortes"};
					recuperaReglasPortes.execute(datosReglasPortes);

				}
			});
	        
	    }

}
