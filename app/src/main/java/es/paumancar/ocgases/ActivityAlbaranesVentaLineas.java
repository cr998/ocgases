package es.paumancar.ocgases;




import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.paumancar.ocgases.datasync.Connectivity;
import es.paumancar.ocgases.datasync.DatosEnvio;
import es.paumancar.ocgases.datasync.DbUpdate;
import es.paumancar.ocgases.datasync.MySQLiteHelperSync;
import es.paumancar.ocgases.datasync.VentasHttpPost;
import es.paumancar.ocgases.reports.AlbaranPdf;
import es.paumancar.ocgases.reports.HtmlReport;
import es.paumancar.ocgases.reports.IntermecPrint;
import es.paumancar.ocgases.reports.ReportListLines;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class ActivityAlbaranesVentaLineas extends Activity {
	

	SharedPreferences sharedPref;
	ListView listviewAlbaranesVentaLineas;
	String[][] stringAdapterCompras = { {"HEADER1", "data1"},
	{"HEADER2", "data2"}, {"HEADER3", "data3"} };
	
	
    
    String etiquetaPrincipal = "AlbaranVentaLineas";
    String[] etiquetasDatos = {
		  "m_inout_id",
		  "m_inoutline_id",
		  "m_product_id",
		  "qtyordered",
		  "qtyreturn",
		  "priceentered",
		  "pricelist",
		  "discount",
		  "linenetamt",
		  "name",
		  "m_attributeset_id",
		  "pidecdgbarras",
		  "llenas",
		  "vacias",
		  "value",
		  "isdescription",
		  "description",
		  "unenvase",
		  "eurokg",
		   "coefconversion"
    };

    String[] stringColumnasAlbaran;
    String[] valoresCabeceraAlbaran;
    
    DatosLista datosListaVenta;
    
    TextView textoDetalleLineaVenta;
    
	private String base64SignaturePng = null;
 
	public static final int ALBARAN_SIGNATURE_ACTIVITY = 1;
	private static final int TIPO_ADAPTER_ALBARANESVENTALINEAS = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albaran_venta_lineas_layout);

		//requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        textoDetalleLineaVenta = (TextView) findViewById(R.id.textViewDetallesDeVentasDescripcion);
        
        listviewAlbaranesVentaLineas = (ListView) findViewById(R.id.listviewAlbaranVentaLineas);
        
        Intent intentReceived = getIntent();

        
        Button botonFirma = (Button) findViewById(R.id.buttonFirmaAlbaran);
        botonFirma.setOnClickListener(new OnClickListener() {
			public void onClick(View view)
			{
				Intent intent = new Intent(ActivityAlbaranesVentaLineas.this, ActivityAlbaranSignature.class);
//				AdapterVentaLineas adapterLineasAlbaran = (AdapterVentaLineas) listviewAlbaranesVentaLineas.getAdapter();
//				List<String[]> listaLineasDatos = adapterLineasAlbaran.getData();
//				ReportListLines listaLineasReport = new ReportListLines(getApplicationContext(), 
//						stringColumnasAlbaran,valoresCabeceraAlbaran,
//						etiquetasDatos, listaLineasDatos, "");
				//HtmlReport htmlReport = new HtmlReport(listaLineasReport.getLinesReport());
				//String htmlReportString = htmlReport.getHtmlString();
				String tipoAlbaran = 
						valoresCabeceraAlbaran[indiceArray(stringColumnasAlbaran, "c_doctype_id")];
				//intent.putExtra("htmlPage", htmlReportString);
				intent.putExtra("tipoAlbaran", tipoAlbaran);
				startActivityForResult(intent, ALBARAN_SIGNATURE_ACTIVITY);
			}
		});
        
        
        
        
        int filtroId = 0; 	// indica la posición del filtro en etiquetasDatos 
  					// Sin filtro -1
        int nextFiltroId = 0;
        stringColumnasAlbaran = intentReceived.getStringArrayExtra("valoresColumnas");
        valoresCabeceraAlbaran = intentReceived.getStringArrayExtra("valoresCabecera");
        String valorFiltro = intentReceived.getStringExtra("valorFiltro");
        String valorDocumento = intentReceived.getStringExtra("valorDocumento");
        Class<?> NextClass = ActivityCodigosBarras.class;
        //Intent intentNextActivity = new Intent(getApplicationContext(), CodigosBarrasCompras.class);
        //String direccionPuertoServidor = sharedPref.getString("direccion_ip", "localhost");
        String direccionPuertoServidor = Connectivity.servidorActivo(getApplicationContext());

		String numeroAlbaran = valoresCabeceraAlbaran[indiceArray(stringColumnasAlbaran, "documentno")];
		TextView textViewNumeroAlbaran = (TextView) findViewById(R.id.textViewNumeroAlbaran);
		textViewNumeroAlbaran.setText(numeroAlbaran);
        datosListaVenta = 
        		new DatosLista(getApplicationContext(), 
        				listviewAlbaranesVentaLineas, etiquetaPrincipal, etiquetasDatos, 
        				NextClass, filtroId, valorFiltro, nextFiltroId,
        				direccionPuertoServidor,
        				TIPO_ADAPTER_ALBARANESVENTALINEAS, textoDetalleLineaVenta, valorDocumento); 
        Context contextActivity = ActivityAlbaranesVentaLineas.this;
        Context contextApp = getApplicationContext();
        RecuperaDatos stringDatosVenta = new RecuperaDatos(contextActivity, contextApp);
        stringDatosVenta.execute(datosListaVenta);
 
        
    }

	@Override
	protected void onPostResume() {
		super.onPostResume();
		AdapterVentaLineas adapterLineas = (AdapterVentaLineas) listviewAlbaranesVentaLineas.getAdapter();
		if (adapterLineas != null){
			adapterLineas.notifyDataSetChanged();
//		    MySQLiteHelper myHelper = (MySQLiteHelper) new MySQLiteHelper(getApplicationContext());
//		    SQLiteDatabase db = myHelper.getReadableDatabase();
//			for (int i = 0; i < listviewAlbaranesVentaLineas.getCount(); i++) {
//			       View vista = listviewAlbaranesVentaLineas.getChildAt(i);
//			       TextView textDetailUdsOrdCont = (TextView) vista.findViewById(R.id.rowTextUdsOrdContVentas);
//			       TextView textDetailUdsRetCont = (TextView) vista.findViewById(R.id.rowTextUdsRetContVentas);
//			       String m_inoutline_id = adapterLineas.getItem(i)[adapterLineas.pos_m_inoutline_id];
//			       boolean esRetornable = 
//			    		   (adapterLineas.getItem(i)[adapterLineas.pos_m_attributeset_id].equals("1000000")?
//			    				    true:false);
//			       if (esRetornable){
//				       int cantCodLlenos = myHelper.cuentaCodigosLlenos(db, m_inoutline_id, "venta");
//				       int cantCodVacios = myHelper.cuentaCodigosVacios(db, m_inoutline_id, "venta");
//				       textDetailUdsOrdCont.setText("[" + String.valueOf(cantCodLlenos) + "]");
//				       textDetailUdsRetCont.setText("[" + String.valueOf(cantCodVacios) + "]");
//			       }
//			}
//		    db.close();
//		    myHelper.close();
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Bundle extras;

		switch (requestCode)
		{
		case ALBARAN_SIGNATURE_ACTIVITY:
			if (RESULT_OK == resultCode)
			{
				extras = data.getExtras();
				base64SignaturePng = extras.getString(ActivityAlbaranSignature.BASE64_SIGNATURE_KEY);
				String latLong = extras.getString(ActivityAlbaranSignature.LATITUD_LONGITUD);
				boolean imprimirAlbaran = extras.getBoolean(ActivityAlbaranSignature.IMPRIMIR_ALBARAN);
				boolean imprimirPdf = extras.getBoolean(ActivityAlbaranSignature.IMPRIMIR_PDF);
				double signRatio = extras.getDouble(ActivityAlbaranSignature.SIGNRATIO);
				String nombreFirma = extras.getString(ActivityAlbaranSignature.NOMBRE_FIRMA);
				//displaySignature(base64SignaturePng);
				AdapterVentaLineas adapterLineasAlbaran = (AdapterVentaLineas) listviewAlbaranesVentaLineas.getAdapter();
				List<String[]> listaLineasDatos = adapterLineasAlbaran.getData();
				List<String> listaLineas = new ArrayList<String>();
				for (String[] stringLinCod : listaLineasDatos) {
					listaLineas.add(stringLinCod[1]);
				}
				//String serverHost = sharedPref.getString("direccion_ip", "localhost");
				String serverHost = Connectivity.servidorActivo(getApplicationContext());
				String headerId = valoresCabeceraAlbaran[indiceArray(stringColumnasAlbaran, "m_inout_id")];
				String partnerId = valoresCabeceraAlbaran[indiceArray(stringColumnasAlbaran, "c_bpartner_id")];
				ReportListLines listaLineasReport = new ReportListLines(getApplicationContext(), 
						stringColumnasAlbaran,valoresCabeceraAlbaran,
						etiquetasDatos, listaLineasDatos, nombreFirma);
				HtmlReport htmlReport = new HtmlReport(listaLineasReport.getLinesReport());
				String numeroAlbaran = 
						valoresCabeceraAlbaran[indiceArray(stringColumnasAlbaran, "documentno")];

				VentasHttpPost PostGrabaLineasVenta = new VentasHttpPost(getApplicationContext(), 
					headerId, partnerId, htmlReport.getHtmStringBase64Encoded(),
					listaLineas, numeroAlbaran, latLong, base64SignaturePng, nombreFirma);
				List<String[]> datosPostGrabaLineasVenta = PostGrabaLineasVenta.getPostValues();
				DatosEnvio[] datosAEnviar = new DatosEnvio[1];
				datosAEnviar[0] = new DatosEnvio(getApplicationContext(), serverHost, "GrabaLineasVenta", 
						"POST", numeroAlbaran, listaLineas, datosPostGrabaLineasVenta, null);
				
				MySQLiteHelperSync myHelperSync = new MySQLiteHelperSync(getApplicationContext());
				SQLiteDatabase dbs =  myHelperSync.getWritableDatabase();
				myHelperSync.insertaDatosEnvio(dbs, datosAEnviar[0]);
				dbs.close();
				myHelperSync.close();
				DbUpdate.marcaCodigosEnviados(datosAEnviar[0], getApplicationContext());
				//TODO Notificar al SyncAdapter
				String anchoFirmaPrintString = "300";
				int altoFirmaPrint = (int) (Integer.valueOf(anchoFirmaPrintString) / signRatio);
				String altoFirmaPrintString = String.format("%03d", altoFirmaPrint);
				String anchoFirmaPdfString = "200";
				int altoFirmaPdf = (int) (Integer.valueOf(anchoFirmaPdfString) / signRatio);
				String altoFirmaPdfString = String.format("%03d", altoFirmaPdf);
				if (imprimirAlbaran) {
					MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
					SQLiteDatabase db = myHelper.getWritableDatabase();
					String[] impresoraActiva = myHelper.recuperaImpresoraActiva(db);
					db.close();
					myHelper.close();
					//String firmaSizeOWH = "020300150";
					String firmaSizeOWH = "020" + anchoFirmaPrintString + altoFirmaPrintString;
					Log.w("OCGASES", "Imprimiento .....");
					IntermecPrint printTask = new IntermecPrint(getApplicationContext(), 
							listaLineasReport.getLinesReport(), 
							base64SignaturePng, firmaSizeOWH);
					//printTask.execute("PB51", "00066608A5C3");
					printTask.execute(impresoraActiva[2], impresoraActiva[3]);
				}
				if (imprimirPdf){
					String firmaSizeOWH = "020" + anchoFirmaPdfString + altoFirmaPdfString;
					Log.w("OCGASES", "Creando PDF .....");
					AlbaranPdf albaranEnvioPdf = new AlbaranPdf(getApplicationContext(), 
							numeroAlbaran, 
							listaLineasReport.getLinesReport(), 
							base64SignaturePng, firmaSizeOWH);
					
					Intent emailIntent = new Intent(Intent.ACTION_SEND);
					emailIntent.setType("text/plain");
					//emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Albaran: " + numeroAlbaran);
					emailIntent.putExtra(Intent.EXTRA_TEXT, "Adjunto remitimos copia de nuestro "
							+ "albarán de referencia: " +  numeroAlbaran);
					File ruta_sd = Environment.getExternalStorageDirectory();
					File fileDir = new File(ruta_sd.getPath() ,  "ocgases/pdfs/");
					File pdfFile = new File(fileDir, albaranEnvioPdf.getPdfFileName());
					if (pdfFile.exists() & pdfFile.canRead()) {
						Uri uri = Uri.fromFile(pdfFile);
						emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
						startActivity(Intent.createChooser(emailIntent, "Seleccione método de envío"));
					}
				}
				finish();
			}
			break;
		}
	}
	
	


	/** FUNCIONES **/
	


	
	private int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}
	

	
	

}
