package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterLocalizaciones  extends BaseAdapter {

    Context context;
    //Intent intentNext;
    List<String[]>  dataLocalizacion;
    private static LayoutInflater inflater = null;

    public AdapterLocalizaciones(Context context, List<String[]> dataLocalizacion) {
        this.context = context;
        if (dataLocalizacion != null) {
        	this.dataLocalizacion = dataLocalizacion;
        } else {
        	this.dataLocalizacion = new ArrayList<String[]>();
        }
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataLocalizacion.size();
    }

    @Override
    public Object getItem(int position) {
        return dataLocalizacion.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_localizaciones, null);
        TextView textNombreLocalizacion = (TextView) vi.findViewById(R.id.rowTextNombreLocalizacion);
        TextView textDetalleLocalizacion = (TextView) vi.findViewById(R.id.rowTextDetailLocalizacion);

        final int positionAux = position;
    	
        vi.setOnClickListener(new OnClickListener() {
        	String[] datosLocalizacion = dataLocalizacion.get(positionAux);
			@Override
			public void onClick(View v) {
				Intent intentNext = new Intent(context.getApplicationContext(), ActivityLocalizacion.class);
				intentNext.putExtra("datosLocalizacion", datosLocalizacion);
				intentNext.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.getApplicationContext().startActivity(intentNext);
			}
		});
        
        textNombreLocalizacion.setText(dataLocalizacion.get(position)[2]);
        textDetalleLocalizacion.setText(dataLocalizacion.get(position)[3]);
        textNombreLocalizacion.setTextColor(Color.BLACK);
        textDetalleLocalizacion.setTextColor(Color.BLACK);
        return vi;
    }

}