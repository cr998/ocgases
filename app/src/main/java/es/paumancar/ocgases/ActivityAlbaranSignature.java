package es.paumancar.ocgases;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;

import es.paumancar.ocgases.datasync.Connectivity;
import es.paumancar.ocgases.reports.PrintFormats;
import es.paumancar.ocgases.reports.ReportListLines;
import android.location.Location;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Implements the UI for the user to sign and save the signature.
 */
@SuppressLint("NewApi")
public class ActivityAlbaranSignature extends FragmentActivity implements OnMapReadyCallback {
	public static final String BASE64_SIGNATURE_KEY = "es.paumancar.ocgases.albaran.Base64Signature";
	public static final String NOMBRE_FIRMA = "es.paumancar.ocgases.albaran.nombreFirma";
	public static final String LATITUD_LONGITUD = "es.paumancar.ocgases.albaran.latlong";
	public static final String IMPRIMIR_ALBARAN = "es.paumancar.ocgases.albaran.imprimiralbaran";
	public static final String IMPRIMIR_PDF = "es.paumancar.ocgases.albaran.imprimirpdf";
	public static final String SIGNRATIO = "es.paumancar.ocgases.albaran.signratio";
	private SignatureView signView;
	
	GoogleMap myMap;
	TextView textCoordsGps;
	double latitud;
	double longitud;
	String nombreFirma = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_albaran_signature);
		signView = (SignatureView) findViewById(R.id.viewAlbaranSignature);
		textCoordsGps = (TextView)  findViewById(R.id.textAlbSignCoordsGps);
		
		final LinearLayout layoutAceptLOPD = (LinearLayout) findViewById(R.id.linearLayoutAlbSignWebView);
        final LinearLayout layoutSignView = (LinearLayout) findViewById(R.id.linearLayoutAlbaranSignatureSignBox);
        final Button buttonAceptLOPD = (Button) findViewById(R.id.buttonAlbLOPDAccept);
        final Button buttonCancelLOPD = (Button) findViewById(R.id.buttonAlbLOPDCancel);
        
        final Button buttonSignPdf = (Button) findViewById(R.id.onAlbSignSavePdf);
        //final LinearLayout layoutButtonsSignClear = (LinearLayout) findViewById(R.id.linearLayoutCaptSignClear);
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion < android.os.Build.VERSION_CODES.KITKAT){
		   	buttonSignPdf.setVisibility(View.GONE);
		}
        
		final WebView webViewAlbaran = (WebView) findViewById(R.id.webViewAlbSign);
		
		layoutSignView.setVisibility(View.INVISIBLE);
		
		Intent intentReceived = getIntent();
		//String albaranHtml = intentReceived.getStringExtra("htmlPage");
		String tipoAlbaran = intentReceived.getStringExtra("tipoAlbaran");
		String leyOPDHtml = "";
		if (tipoAlbaran.equals(ReportListLines.TIPO_ALBARAN_CF)){
			leyOPDHtml = PrintFormats.TEXTO_LOPD_LSSI_2;
		} else {
			leyOPDHtml = PrintFormats.TEXTO_LOPD_LSSI_1;
		}
		WebSettings settings = webViewAlbaran.getSettings();
		//settings.setJavaScriptEnabled(true);
        //settings.setBuiltInZoomControls(true);
		settings.setUseWideViewPort(false);
		//settings.setLoadWithOverviewMode(true);
		leyOPDHtml = leyOPDHtml.replaceAll("á", "&aacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("é", "&eacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("í", "&iacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("ó", "&oacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("ú", "&uacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Á", "&Aacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("É", "&Eacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Í", "&Iacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Ó", "&Oacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("Ú", "&Uacute;");
		leyOPDHtml = leyOPDHtml.replaceAll("ñ", "&ntilde;");
		leyOPDHtml = leyOPDHtml.replaceAll("Ñ", "&Ntilde;");
		webViewAlbaran.loadData(leyOPDHtml, "text/html", "UTF-8");
		
		buttonAceptLOPD.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				layoutAceptLOPD.setVisibility(View.INVISIBLE);
				layoutSignView.setVisibility(View.VISIBLE);
				
				final EditText editTextNombreFirma = new EditText(ActivityAlbaranSignature.this);
				LinearLayout layoutTextos = new LinearLayout(ActivityAlbaranSignature.this);
				LinearLayout.LayoutParams layoutForInner = 
						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
								LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutTextos.setLayoutParams(layoutForInner);
				layoutTextos.addView(editTextNombreFirma);
				LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
				editTextNombreFirma.setLayoutParams(params);
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityAlbaranSignature.this);
				
				dialog.setTitle("NOMBRE FIRMANTE");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	nombreFirma = editTextNombreFirma.getText().toString(); 
				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
				
			}
		});
		
		buttonCancelLOPD.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				layoutAceptLOPD.setVisibility(View.INVISIBLE);
				layoutSignView.setVisibility(View.INVISIBLE);
				//signView.setVisibility(View.INVISIBLE);
				//layoutButtonsSignClear.setVisibility(View.INVISIBLE);
			}
		});
		
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()		    
				.findFragmentById(R.id.fragmentAlbSignMap);
		mapFragment.getMapAsync(this);
	}



	/**
	 * Called when the Save button is clicked. This method saves the current
	 * signature in PNG format encoded in Base64. It delivers the result
	 * containing the Base64 string via an Intent which is received by
	 * the PrintActivity.onActivityResult method.
	 * @param view The view where the click event occurs.
	 */
	public void onAlbSignSaveButtonClicked (View view)
	{
		String base64Jpg = signView.getBase64EncodedJPG();

		Intent intent = new Intent();
		if (base64Jpg != null)
		{
			intent.putExtra(BASE64_SIGNATURE_KEY, base64Jpg);
			intent.putExtra(NOMBRE_FIRMA, nombreFirma);
			intent.putExtra(LATITUD_LONGITUD, String.format("%f:%f",latitud, longitud));
			intent.putExtra(IMPRIMIR_ALBARAN, false);
			intent.putExtra(IMPRIMIR_PDF, false);
			intent.putExtra(SIGNRATIO, (double) 2);
		}

		setResult(RESULT_OK,intent);
		finish();
	}
	
	public void onAlbSignSaveSignButtonClicked (View view)
	{
		String base64Jpg = signView.getBase64EncodedJPG();
		double signViewRatio = ((double) signView.getWidth())/((double) signView.getHeight());
		Intent intent = new Intent();
		if (base64Jpg != null)
		{
			intent.putExtra(BASE64_SIGNATURE_KEY, base64Jpg);
			intent.putExtra(NOMBRE_FIRMA, nombreFirma);
			intent.putExtra(LATITUD_LONGITUD, String.format("%f:%f",latitud, longitud));
			intent.putExtra(IMPRIMIR_ALBARAN, true);
			intent.putExtra(IMPRIMIR_PDF, false);
			intent.putExtra(SIGNRATIO, signViewRatio);
		}

		setResult(RESULT_OK,intent);
		finish();
	}
	
	public void onAlbSignSavePdfButtonClicked (View view)
	{
		String base64Jpg = signView.getBase64EncodedJPG();
		double signViewRatio = ((double) signView.getWidth())/((double) signView.getHeight());
		Intent intent = new Intent();
		if (base64Jpg != null)
		{
			intent.putExtra(BASE64_SIGNATURE_KEY, base64Jpg);
			intent.putExtra(NOMBRE_FIRMA, nombreFirma);
			intent.putExtra(LATITUD_LONGITUD, String.format("%f:%f",latitud, longitud));
			intent.putExtra(IMPRIMIR_ALBARAN, false);
			intent.putExtra(IMPRIMIR_PDF, true);
			intent.putExtra(SIGNRATIO, signViewRatio);
		}

		setResult(RESULT_OK,intent);
		finish();
	}

	public void onAlbSignClearButtonClicked (View view)
	{
		signView.clear();
	}

	public void onAlbSignCancelButtonClicked (View view)
	{
		Intent intent = new Intent();
		setResult(RESULT_CANCELED,intent);
		finish();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onMapReady(GoogleMap map) {
		myMap = map;
		map.setMyLocationEnabled(true);
		map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			@Override
			public void onMyLocationChange(Location location) {
				latitud = location.getLatitude();
				longitud = location.getLongitude();
				LatLng myLocation = new LatLng(latitud, longitud);
				textCoordsGps.setText(String.format("lat: %f  long: %f",latitud, longitud));
		        myMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
		        myMap.animateCamera(CameraUpdateFactory.zoomTo(16), 5000, null);
				
			}
		});
	    
	     map.setOnMapClickListener(new OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng point) {
				String textoBoton = String.valueOf(point.latitude) + ", " + String.valueOf(point.longitude);
				
			}
		});
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			return rootView;
		}
	}

}
