package es.paumancar.ocgases;

public class TagsPreferencias {
	public static final String RED_LOCAL = "red_local";
	
	public static final String ESCANER = "escaner"; //puede tomar los valores camara o lector
	public static final String ESCANER_VALOR_CAMARA = "camara";
	public static final String ESCANER_VALOR_LECTOR = "lector";
	
	public static final String DIAS_GUARDADO_DATOS = "dias_guardado_datos";
	
}
