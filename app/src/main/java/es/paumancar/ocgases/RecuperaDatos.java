package es.paumancar.ocgases;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

@SuppressLint("NewApi")
public class RecuperaDatos extends AsyncTask<DatosLista, String, List<String[]>> {

	//String[] elementosRecuperables = { "PedidoCompraCabecera", "PedidoCompraLineas"};
	
	ListView listaDatos;
	String elementoRecupera;
	String[] etiquetasRecupera;
	String direccionIP;
	Context contextActivity;
	Context contextApp;
	//Intent intentNext;
	Class<?> NextClass;
	int filtroId;
	String valorFiltro;
	int nextFiltroId;
	int tipoAdapter;
	TextView textoDescripcion;
	String documento;
	
	ProgressDialog dialogProgreso;
	
	private static final int ADAPTER_COMPRAS = 0;
	private static final int ADAPTER_COMPRAS_LINEAS = 1;
	private static final int ADAPTER_VENTAS = 2;
	private static final int ADAPTER_VENTAS_LINEAS = 3;
	
	
	public RecuperaDatos(Context contextActivity, Context contextApp) {
		super();
		this.contextActivity = contextActivity;
		this.contextApp = contextApp;
	}


	@Override
	protected void onPreExecute() {
		dialogProgreso = ProgressDialog.show(contextActivity, "", 
	            "Descargando datos ...", true);
		dialogProgreso.setCancelable(true);
		super.onPreExecute();
	}


	@Override
	protected List<String[]> doInBackground(DatosLista... datosRecuperaArray) {
		List<String[]> resultado = new ArrayList<String[]>();
		listaDatos = datosRecuperaArray[0].getLista();
		//context = datosRecuperaArray[0].getContext();
		elementoRecupera = datosRecuperaArray[0].getDatoRecupera();
		etiquetasRecupera = datosRecuperaArray[0].getEtiquetasRecupera();
		direccionIP = datosRecuperaArray[0].getDireccionIP();
		//intentNext = datosRecuperaArray[0].getIntentNext();
		NextClass = datosRecuperaArray[0].getNextClass();
		filtroId = datosRecuperaArray[0].getFiltroId();
		valorFiltro = datosRecuperaArray[0].getValorFiltro();
		nextFiltroId = datosRecuperaArray[0].getNextFiltroId();
		tipoAdapter = datosRecuperaArray[0].getTipoAdapter();
		textoDescripcion = datosRecuperaArray[0].getTextoDescripcion();
		documento = datosRecuperaArray[0].getDocumento();
		
		switch (tipoAdapter) {
		case ADAPTER_COMPRAS: case ADAPTER_COMPRAS_LINEAS:
//			try {
				resultado = recuperaDatosServidor();
//			} catch (KeyManagementException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (CertificateException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (KeyStoreException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (NoSuchAlgorithmException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			break;
			
		case ADAPTER_VENTAS: case ADAPTER_VENTAS_LINEAS:
			resultado = recuperaDatosFichero();
			break;

		default:
			break;
		}

		return resultado;
	}
	

	@SuppressLint("NewApi")
	@Override
	protected void onPostExecute(List<String[]> result) {
		super.onPostExecute(result);
		dialogProgreso.cancel();
		switch (tipoAdapter) {
		case ADAPTER_COMPRAS:
			listaDatos.setAdapter(new AdapterCompras(contextApp, result, etiquetasRecupera, 
					NextClass, filtroId, nextFiltroId));
			break;
		case ADAPTER_COMPRAS_LINEAS:
			listaDatos.setAdapter(new AdapterCompraLineas(contextApp, result, etiquetasRecupera, 
					NextClass, filtroId, nextFiltroId, textoDescripcion));
			break;
		case ADAPTER_VENTAS:
			listaDatos.setAdapter(new AdapterVentas(contextApp, result, etiquetasRecupera, 
					NextClass, filtroId, nextFiltroId, textoDescripcion));
			break;
		case ADAPTER_VENTAS_LINEAS:
			String columnaFiltro = "value";
			String[] valoresAEliminar = {""};  //{"99"};
			List<String[]> resultadoFiltrado = filtraResultado(result, columnaFiltro, valoresAEliminar);
			List<String[]> resultadoSinDuplicados = eliminaDuplicados(resultadoFiltrado, "m_inoutline_id");
			listaDatos.setClickable(true);
			listaDatos.setAdapter(new AdapterVentaLineas(contextApp, resultadoSinDuplicados, etiquetasRecupera,
					NextClass, filtroId, nextFiltroId, textoDescripcion, documento));
			
			
			break;

		default:
			break;
		}
	}
	
	
	private List<String[]> recuperaDatosServidor() 
//			throws 
//		CertificateException, IOException, 
//		KeyStoreException, NoSuchAlgorithmException, KeyManagementException
		{
		List<String[]> resultado = new ArrayList<String[]>();
		
//		
//		// Load CAs from an InputStream
//		// (could be from a resource or ByteArrayInputStream or ...)
//		CertificateFactory cf = CertificateFactory.getInstance("X.509");
//		// From https://www.washington.edu/itconnect/security/ca/load-der.crt
//		File ruta_sd = Environment.getExternalStorageDirectory();
//		File fileDir = new File(ruta_sd.getPath() ,  "ocgases/");
//		File outputFile = new File(fileDir ,  "cacert.pem");
//		//InputStream caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
//		InputStream caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
//		Certificate ca;
//		try {
//		    ca = cf.generateCertificate(caInput);
//		    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//		} finally {
//		    caInput.close();
//		}
//
//		// Create a KeyStore containing our trusted CAs
//		String keyStoreType = KeyStore.getDefaultType();
//		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//		keyStore.load(null, null);
//		keyStore.setCertificateEntry("ca", ca);
//
//		// Create a TrustManager that trusts the CAs in our KeyStore
//		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//		TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//		tmf.init(keyStore);
//
//		// Create an SSLContext that uses our TrustManager
//		SSLContext context = SSLContext.getInstance("TLSv1");
//		context.init(null, tmf.getTrustManagers(), null);

		// Tell the URLConnection to use a SocketFactory from our SSLContext
//		URL url = new URL("https://certs.cac.washington.edu/CAtest/");
//		HttpsURLConnection urlConnection =
//		    (HttpsURLConnection)url.openConnection();
//		urlConnection.setSSLSocketFactory(context.getSocketFactory());
		
;
		
		HttpURLConnection con = null;
		String direccionGet = 
				direccionIP + "/rest/source/?object=" + elementoRecupera + "&action=download";
		URL url = null;
		try {
			url = new URL(direccionGet);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	       try {
	    	   if (url != null) {
		    	   con = (HttpURLConnection)url.openConnection();
		    	   //con.setSSLSocketFactory(context.getSocketFactory());
//	    		   con.setConnectTimeout(10000);
//	    		   con.setReadTimeout(10000);
	    	   }

	           int statusCode = con.getResponseCode();

	            if(statusCode == 200) {

	                InputStream in = new BufferedInputStream(con.getInputStream());
	                
	                String[][] elementosCabeceraEtiquetasRecupera = new String[1][etiquetasRecupera.length + 1];
	                elementosCabeceraEtiquetasRecupera[0][0] = elementoRecupera;
	                for (int i = 0; i < etiquetasRecupera.length; i++) {
						elementosCabeceraEtiquetasRecupera[0][i+1] = 
								etiquetasRecupera[i];
					}
	                XMLParserPedidoCompraCabecera parser = 
	                		new XMLParserPedidoCompraCabecera(elementosCabeceraEtiquetasRecupera, 
	                				filtroId, valorFiltro);

	                resultado = parser.recuperaLista(in)[0];

	            }

	        } catch (Exception e) {
	            Log.e("OCGASES", e.getMessage());

	        }finally {
	            con.disconnect();
	        }

		return resultado;

	}
	
	private List<String[]> recuperaDatosFichero(){
		List<String[]> resultado = new ArrayList<String[]>();

		String ficheroString = contextApp.getFilesDir().getAbsolutePath() + "/" 
				+ elementoRecupera + ".xml";

	       try {
                File ficheroXml = new File(ficheroString);
                InputStream in = new BufferedInputStream(new FileInputStream(ficheroXml));
                
                String[][] elementosCabeceraEtiquetasRecupera = new String[1][etiquetasRecupera.length + 1];
                elementosCabeceraEtiquetasRecupera[0][0] = elementoRecupera;
                for (int i = 0; i < etiquetasRecupera.length; i++) {
					elementosCabeceraEtiquetasRecupera[0][i+1] = 
							etiquetasRecupera[i];
				}
                XMLParserPedidoCompraCabecera parser = 
                		new XMLParserPedidoCompraCabecera(elementosCabeceraEtiquetasRecupera, 
                				filtroId, valorFiltro);

                resultado = parser.recuperaLista(in)[0];



	        } catch (Exception e) {
	            Log.e("OCGASES", e.getMessage());

	        }

		return resultado;
	}

	

	private List<String[]> filtraResultado(List<String[]> resultadoBruto,
			String columnaFiltro, String[] valoresAEliminar) {
		List<String[]> listaFiltrada = new ArrayList<String[]>();
		int indiceColumnaFiltro = indiceArray(etiquetasRecupera, columnaFiltro);
		for (String[] stringResultado : resultadoBruto) {
			boolean encontrado = false;
			for (String stringEliminar : valoresAEliminar) {
				if (stringResultado[indiceColumnaFiltro].equals(stringEliminar)){
					encontrado = true;
					break;
				}
			}
			if (!encontrado){
				listaFiltrada.add(stringResultado);
			}
		}
		return listaFiltrada;
	}

	private List<String[]> eliminaDuplicados(List<String[]> resultadoBruto,
			String columnaDuplicada){
		List<String[]> listaFiltrada = new ArrayList<String[]>();
		int indiceColumnaDuplicada = indiceArray(etiquetasRecupera, columnaDuplicada);
		for (String[] filaOriginal : resultadoBruto) {
			boolean yaExiste = false;
			for (String[] filaFiltrada : listaFiltrada) {
				if (filaFiltrada[indiceColumnaDuplicada].equals(filaOriginal[indiceColumnaDuplicada])){
					yaExiste = true;
				}
			}
			if (!yaExiste){
				listaFiltrada.add(filaOriginal);
			}
		}
		return listaFiltrada;
	}
	
	private int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}


}
