package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FragmentCodigosLlenos extends Fragment {
	
	private List<String[]> codigosLlenos;
	private MyListView listviewCodigosLlenos;
	private String m_inoutline_id;
	private String compra_venta;
	private boolean activarCantidad;
	private String cantidad;
	private String descripcion;
	private String etiquetaLeidos;
	private String documento;
	
	private TextView textoDescripcion;
	private TextView textoCantidad;
	private TextView textoLeidos;
	private TextView textoLeidosEtiqueta;
	
	private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    
    private final static int CODIGOS_LLENOS = 0;
    private final static int CODIGOS_VACIOS = 1;
    
    public Button botonAddCodeBar;
    public EditText editTextAddCodeBar;

	
	public FragmentCodigosLlenos(List<String[]> codigosLlenos, 
			String m_inoutline_id, String documento, String compra_venta, boolean activarCantidad, 
			String cantidad, String descripcion, String etiquetaLeidos ) {
		super();
		if (codigosLlenos != null){
			this.codigosLlenos = codigosLlenos;
		} else {
			this.codigosLlenos = new ArrayList<String[]>();
		}
		this.m_inoutline_id = m_inoutline_id;
		this.compra_venta = compra_venta;
		this.activarCantidad = activarCantidad;
		this.cantidad = cantidad;
		this.descripcion = descripcion;
		this.etiquetaLeidos = etiquetaLeidos;
		this.documento = documento;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {


	    View rootView = inflater.inflate(R.layout.tab_codigosllenos, container,
	            false);
	   
	    return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		textoLeidosEtiqueta = (TextView) view.findViewById(R.id.textoTabCodigosLlenosLeidosNombre);
        textoDescripcion = (TextView) view.findViewById(R.id.textoTabCodigosLlenosDescripcion);
        textoCantidad = (TextView) view.findViewById(R.id.textoTabCodigosLlenosCantidad);
        textoLeidos = (TextView) view.findViewById(R.id.textoTabCodigosLlenosLeidos);
        botonAddCodeBar = (Button) view.findViewById(R.id.buttonTabCodLlenosCodeTextAdd);
        editTextAddCodeBar = (EditText) view.findViewById(R.id.editTextTabCodLlenosCodeText);
        Button botonEscaneaLlenos = (Button) view.findViewById(R.id.buttonTabCodLlenosScanCamara);   
        MySQLiteHelper myHelper = new MySQLiteHelper(getContext());
		SQLiteDatabase db = myHelper.getWritableDatabase();
        myHelper.actualizaCodigosLlenos(db, m_inoutline_id, documento,
        		descripcion, codigosLlenos, compra_venta);
        codigosLlenos = myHelper.recuperaCodigosLlenos(db, m_inoutline_id, compra_venta);
        db.close();
        myHelper.close();
		listviewCodigosLlenos = (MyListView) view.findViewById(R.id.listviewCodigosCompraLlenos);
        listviewCodigosLlenos.setAdapter(new AdapterCodigos(getContext(), 
        		codigosLlenos, m_inoutline_id, compra_venta, CODIGOS_LLENOS, activarCantidad,  
        		textoLeidos));
        textoCantidad.setText(cantidad);
        textoDescripcion. setText(descripcion);
        textoLeidosEtiqueta.setText(etiquetaLeidos);
        
        botonAddCodeBar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String textoCodigo = editTextAddCodeBar.getText().toString();
		      	insertaCodigoEnLista("1", textoCodigo);
		      	//editTextAddCodeBar.requestFocus();
		      	editTextAddCodeBar.setText("");
			}
		});
        
        editTextAddCodeBar.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View editText, int keyCode, KeyEvent event) {
				 if ((event.getAction() == KeyEvent.ACTION_UP) &&
				            (keyCode == KeyEvent.KEYCODE_ENTER))  { 
					 		String textoCodigo = ((EditText) editText).getText().toString();
					      	insertaCodigoEnLista("1", textoCodigo);
					      	editText.requestFocus();
					      	((EditText) editText).setText("");
					      	return true;
					   }
				return false;
			}

		});
        
        botonEscaneaLlenos.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(), ActivityBarcodeCapture.class);
	            intent.putExtra(ActivityBarcodeCapture.AutoFocus, true);
	            intent.putExtra(ActivityBarcodeCapture.UseFlash, false);

	            startActivityForResult(intent, RC_BARCODE_CAPTURE);
				
			}
		});

	}
	
//	@Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == RC_BARCODE_CAPTURE) {
//            if (resultCode == CommonStatusCodes.SUCCESS) {
//                if (data != null) {
//                    Barcode barcode = data.getParcelableExtra(ActivityBarcodeCapture.BarcodeObject);
//                    //statusMessage.setText(R.string.barcode_success);
//                    //barcodeValue.setText(barcode.displayValue);
//                    String codigoDevuelto = barcode.displayValue;
//                    insertaCodigoEnLista("1", codigoDevuelto);
//                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
//                } else {
//                    //statusMessage.setText(R.string.barcode_failure);
//                    Log.d(TAG, "No barcode captured, intent data is null");
//                }
//            } else {
//                //statusMessage.setText(String.format(getString(R.string.barcode_error),
//                //        CommonStatusCodes.getStatusCodeString(resultCode)));
//            }
//        }
//        else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }
	
	
	public void setCodigos(List<String[]> codigosLlenos){
		this.codigosLlenos = codigosLlenos;
		if (listviewCodigosLlenos.getAdapter() != null){
			((AdapterCodigos)listviewCodigosLlenos.getAdapter()).notifyDataSetChanged();
		} else {
			listviewCodigosLlenos.setAdapter(new AdapterCodigos(getContext(), 
					this.codigosLlenos, this.m_inoutline_id, compra_venta, CODIGOS_LLENOS, activarCantidad, 
					textoLeidos));
		}
		updateCantidad();
	}
	


	public void insertaCodigoEnLista(String cantidad, String textoCodigo) {
        if (!textoCodigo.equals("")){
            List<String[]> listaCodigos = new ArrayList<String[]>();
            String[] codAux = {cantidad, textoCodigo};
            listaCodigos.add(codAux);
            MySQLiteHelper myHelper = new MySQLiteHelper(getContext());
            SQLiteDatabase db = myHelper.getWritableDatabase();
            myHelper.actualizaCodigosLlenos(db, m_inoutline_id, documento, descripcion, listaCodigos, compra_venta);
            codigosLlenos = myHelper.recuperaCodigosLlenos(db, m_inoutline_id, compra_venta);
            db.close();
            myHelper.close();
            listviewCodigosLlenos.setAdapter(new AdapterCodigos(getContext(), 
            		codigosLlenos, m_inoutline_id, compra_venta, CODIGOS_LLENOS, activarCantidad, 
            		textoLeidos));
            updateCantidad();
        }
	}
	
	private void updateCantidad() {
	textoLeidos.setText(String.valueOf(listviewCodigosLlenos.getAdapter().getCount()));
	}
	
	public void setDescripcion(String textoDesc){
		textoDescripcion.setText(textoDesc);
	}
	
	public void setCantidad(String textCantidad){
		textoCantidad.setText(textCantidad);
	}
	


}
