package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterCompraLineas  extends BaseAdapter {
	
	int posicionActiva = -1;
    Context context;
    Class<?> NextClass;
    int filtroId;
    int nextFiltroId;
    List<String[]> data;
    private static LayoutInflater inflater = null;
    int posLlenas = 7; // MODIFICAR SI SE CAMBIA ARRAY
    int posVacias = 8; // MODIFICAR SI SE CAMBIA ARRAY
    int pos_c_orderline_id = 1; // MODIFICAR SI SE CAMBIA ARRAY
    int pos_m_attributeset_id;
    String[] cabecerasColumnas;
    TextView textoDescripicion;

    public AdapterCompraLineas(Context context, List<String[]> data, String[] cabecerasColumnas, 
    		Class<?> NextClass, int filtroId, int nextFiltroId, TextView textoDescipcion) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.NextClass = NextClass;
        this.filtroId = filtroId;
        this.nextFiltroId = nextFiltroId;
        if (data != null) {
        	this.data = data;
        }
        this.pos_m_attributeset_id = indiceArray(cabecerasColumnas, "m_attributeset_id");
        this.posLlenas = indiceArray(cabecerasColumnas, "llenas");
        this.posVacias = indiceArray(cabecerasColumnas, "vacias");
        this.pos_c_orderline_id = indiceArray(cabecerasColumnas, "c_orderline_id");
        this.cabecerasColumnas = cabecerasColumnas;
        this.textoDescripicion = textoDescipcion;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public String[] getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_compralineas, null);
        //TextView textHeader = (TextView) vi.findViewById(R.id.rowTextHeaderCompraLineas);
        TextView textDetailUdsOrd = (TextView) vi.findViewById(R.id.rowTextUdsOrdCompras); //Llenas
        TextView textDetailUdsOrdCont = (TextView) vi.findViewById(R.id.rowTextUdsOrdContCompras); //Llenas
        TextView textDetailUdsRet = (TextView) vi.findViewById(R.id.rowTextUdsRetCompras); //Vacías
        TextView textDetailUdsRetCont = (TextView) vi.findViewById(R.id.rowTextUdsRetContCompras); //Vacías
        TextView textDetailName = (TextView) vi.findViewById(R.id.rowTextCodigoCompras);
        //TextView textDetailDescripcion = (TextView) vi.findViewById(R.id.rowTextDescripcionCompras);
        int codigosLlenosCont = 0;
        int codigosVaciosCont = 0;
        final int positionAux = position;
        
        if (positionAux == posicionActiva) {
        	vi.setBackgroundColor(Color.CYAN); 
        } else {
        	vi.setBackgroundColor(Color.TRANSPARENT);
        }
        
        boolean esRetornable =
        		(data.get(position)[indiceArray(cabecerasColumnas, "m_attributeset_id")].equals("1000000")? true:false);
 
    	
        if (esRetornable){
            MySQLiteHelper myHelper = new MySQLiteHelper(context);
            SQLiteDatabase db = myHelper.getWritableDatabase();
            List<String> listaCodigosLlenos = Arrays.asList(data.get(positionAux)[posLlenas].split(","));
            List<String> listaCodigosVacios = Arrays.asList(data.get(positionAux)[posVacias].split(","));
            List<String[]> listaCodigosLlenosArray = new ArrayList<String[]>();
            List<String[]> listaCodigosVaciosArray = new ArrayList<String[]>();
            for (String codigoLleno : listaCodigosLlenos) {
				String[] linAux = { "1", codigoLleno };
				listaCodigosLlenosArray.add(linAux);
			}
            for (String codigoVacio : listaCodigosVacios) {
				String[] linAux = { "1", codigoVacio };
				listaCodigosVaciosArray.add(linAux);
			}
            String c_orderline_id = data.get(positionAux)[pos_c_orderline_id];
            codigosLlenosCont = 
            		myHelper.actualizaCodigosLlenos(db, c_orderline_id, "", "", listaCodigosLlenosArray, "compra");
            codigosVaciosCont = 
            		myHelper.actualizaCodigosVacios(db, c_orderline_id, "", "", listaCodigosVaciosArray, "compra");
            db.close();
            myHelper.close();
            
            
            //TODO Revisar
            data.get(positionAux)[posLlenas] = "";
            data.get(positionAux)[posVacias] = "";
            
            
	        vi.setOnClickListener(new OnClickListener() {
	        	String[] cabeceraProv = data.get(positionAux);
	        	int sigFiltroId = nextFiltroId;
	        	String valorFiltro =(sigFiltroId < 0) ? "" : cabeceraProv[sigFiltroId];
	        	Class<?> sigClass = NextClass;
				@Override
				public void onClick(View v) {
					Log.w("OCGASES", "Pos actual " + String.valueOf(positionAux));
					Log.w("OCGASES", "Pos activa " + String.valueOf(posicionActiva));
					if (posicionActiva == positionAux) {
						Intent intentNext = new Intent(context.getApplicationContext(), sigClass);
						intentNext.putExtra("valoresCabecera", cabeceraProv);
						intentNext.putExtra("valorFiltro", valorFiltro);
						intentNext.putExtra("compra_venta", "compra");
						intentNext.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.getApplicationContext().startActivity(intentNext);
					} else {
						posicionActiva = positionAux;
						textoDescripicion.setText(data.get(positionAux)[indiceArray(cabecerasColumnas, "name")]);
						notifyDataSetChanged();
					}
				}
			});
	        textDetailUdsOrdCont.setText(String.format( "%.0f", (double) codigosLlenosCont));
	        String unidsRet = data.get(position)[indiceArray(cabecerasColumnas, "qtyreturn")];//.split(".");
	        if (esRetornable){
	        	textDetailUdsRet.setText(String.format( "%.0f", Double.valueOf(unidsRet)));
	        	textDetailUdsRetCont.setText(String.format( "%.0f", (double) codigosVaciosCont));
	        }
    	}  else {
    		vi.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.w("OCGASES", "Pos actual " + String.valueOf(positionAux));
					Log.w("OCGASES", "Pos activa " + String.valueOf(posicionActiva));
					posicionActiva = positionAux;
					textoDescripicion.setText(data.get(positionAux)[indiceArray(cabecerasColumnas, "name")]);
					notifyDataSetChanged();
				}
			});
    		textDetailUdsOrdCont.setText("");
	        textDetailUdsRetCont.setText(""); 
    	}

        
        //textHeader.setText(data.get(position)[indiceArray(cabecerasColumnas, "m_product_id")]);
        String unidsOrd = data.get(position)[indiceArray(cabecerasColumnas, "qtyordered")];//.split(".");
        textDetailUdsOrd.setText(String.format( "%.0f", Double.valueOf(unidsOrd)));
        textDetailName.setText(data.get(position)[indiceArray(cabecerasColumnas, "value")]);
        //textDetailDescripcion.setText(data.get(position)[indiceArray(cabecerasColumnas, "name")]);
        //textHeader.setTextColor(Color.BLACK);
        textDetailUdsOrd.setTextColor(Color.BLACK);
        textDetailUdsRet.setTextColor(Color.BLACK);
        textDetailUdsOrdCont.setTextColor(Color.BLACK);
        textDetailUdsRetCont.setTextColor(Color.BLACK);
        textDetailName.setTextColor(Color.BLACK);
        //textDetailDescripcion.setTextColor(Color.BLACK);
        return vi;
    }
    
	public List<String[]> getData() {
		return data;
	}
	
	
	protected int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}
}
