package es.paumancar.ocgases;

import java.util.List;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

public class ActivityRecuperaCodigos extends Activity {
	
	ListView listviewCodigos;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.codigos_perdidos_layout);
        
        listviewCodigos = (ListView) findViewById(R.id.listviewCodigosPerdidos);
        
        List<String[]> listaCodigosPerdidos = recuperaCodigosPerdidos();
        
        listviewCodigos.setAdapter(new AdapterCodigosPerdidos(getApplicationContext(), listaCodigosPerdidos));
 
        
    }

	private List<String[]> recuperaCodigosPerdidos() {
		MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
        SQLiteDatabase db = myHelper.getWritableDatabase();
        List<String[]> listaCodigos = myHelper.recuperaCodigosAntiguosVenta(db);
        db.close();
        myHelper.close();
		return listaCodigos;
	}
	

}
