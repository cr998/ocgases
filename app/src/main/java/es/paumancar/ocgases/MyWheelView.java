package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;

public class MyWheelView extends WheelView {
	
	public Context mcontext = getContext();
	private List<String[]> listaFiltrada;

	public MyWheelView(Context context) {
		super(context);
	}
	
	public MyWheelView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MyWheelView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public void setMyAdapter (List<String[]> listaCartas){
		this.listaFiltrada = eliminaDuplicadosLista(listaCartas, 0);
		this.setViewAdapter(new CartasWheelAdapter(mcontext, listaFiltrada, 2));
	}
	
	public String getIdCarta(int index){
		return listaFiltrada.get(index)[0];
	}
	
   class CartasWheelAdapter extends AbstractWheelTextAdapter {
        // Countries names
        private List <String[]> cartas;
        private int columna;
            //new String[] {"A", "B", "C", "D", "E", "F", "G"};
        // Countries flags
//	        private int flags[] =
//	            new int[] {R.drawable.letra_a, R.drawable.letra_b, R.drawable.letra_c, R.drawable.letra_d,
//	        		R.drawable.letra_e, R.drawable.letra_f, R.drawable.letra_g};
        
        /**
         * Constructor
         */
        protected  CartasWheelAdapter(Context context, List <String[]> cartas, int columna) {
            super(context, R.layout.wheel_layout, NO_RESOURCE);
            this.cartas = cartas;
            this.columna = columna;
            setItemTextResource(R.id.wheel_name);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            //ImageView img = (ImageView) view.findViewById(R.id.nivel);
            //img.setImageResource(flags[index]);
            return view;
        }
        
        @Override
        public int getItemsCount() {
            return cartas.size();
        }
        
        @Override
        protected CharSequence getItemText(int index) {
            return  cartas.get(index)[columna];
        }

    }
   
   
   /********************************************
    *            FUNCIONES
    *
    ******************************************/
	private List <String[]> eliminaDuplicadosLista ( List <String[]> listaCompleta, int indice) {
		List <String[]> listaFiltrada = new ArrayList<String[]>();
		if (listaCompleta == null) return listaFiltrada;
		for (Iterator<String[]> iteratorC = listaCompleta.iterator(); iteratorC.hasNext();) {
			String[] stringListaCompleta = (String[]) iteratorC.next();
			boolean repetido = false;
			for (Iterator<String[]> iteratorF = listaFiltrada.iterator(); iteratorF.hasNext();) {
				String[] stringListaFiltrada = (String[]) iteratorF.next();
				if (stringListaFiltrada[indice].equals(stringListaCompleta[indice])){
					repetido = true;
					break;
				}
			}
			if (!repetido) {
				listaFiltrada.add(stringListaCompleta);
			}
		}
		return listaFiltrada;
	}
	

}
