package es.paumancar.ocgases;

import android.content.Context;
import android.content.Intent;
import android.widget.ListView;
import android.widget.TextView;

public class DatosLista {
	
	private ListView lista;
	private String datoRecupera;
	private String[] etiquetasRecupera;
	private int filtroId;
	private String valorFiltro;
	private String direccionIP;
	private Context context;
	private Class<?> NextClass;
	private int nextFiltroId;
	private TextView textoDescripcion;
	int tipoAdapter; //0:Compras, 1:ComprasLineas  2:Ventas, 3:VentasLineas
	private String documento;
	
	public DatosLista(Context context, ListView lista, String datoRecupera, 
			String[] etiquetasRecupera, Class<?> NextClass, int filtroId, String valorFiltro, 
			int nextFiltroId, String direccionIP, int tipoAdapter, TextView textoDescripcion, 
			String documento) {
		this.context = context;
		this.lista = lista;
		this.datoRecupera = datoRecupera;
		this.etiquetasRecupera = etiquetasRecupera;
		this.direccionIP = direccionIP;
		this.NextClass = NextClass;
		this.filtroId = filtroId;
		this.valorFiltro = valorFiltro;
		this.nextFiltroId = nextFiltroId;
		this.tipoAdapter = tipoAdapter;
		this.textoDescripcion = textoDescripcion;
		this.documento = documento;
	}
	


	public int getTipoAdapter() {
		return this.tipoAdapter;
	}



	public void setTipoAdapter(String stringTipoAdapter) {
		this.tipoAdapter = 0;
		if (stringTipoAdapter.equals("Compra")) this.tipoAdapter = 0;
		if (stringTipoAdapter.equals("CompraLineas")) this.tipoAdapter = 1;
		if (stringTipoAdapter.equals("Venta")) this.tipoAdapter = 2;
		if (stringTipoAdapter.equals("VentaLineas")) this.tipoAdapter = 3;
	}



	public void setNextFiltroId(int nextFiltroId) {
		this.nextFiltroId = nextFiltroId;
	}



	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ListView getLista() {
		return lista;
	}
	
	public void setLista(ListView lista) {
		this.lista = lista;
	}
	
	public String getDatoRecupera() {
		return datoRecupera;
	}
	
	public void setDatoRecupera(String datoRecupera) {
		this.datoRecupera = datoRecupera;
	}
	
	public String[] getEtiquetasRecupera() {
		return etiquetasRecupera;
	}

	public void setEtiquetasRecupera(String[] etiquetasRecupera) {
		this.etiquetasRecupera = etiquetasRecupera;
	}

	public String getDireccionIP() {
		return direccionIP;
	}

	public void setDireccionIP(String direccionIP) {
		this.direccionIP = direccionIP;
	}
	
	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}


//
//	public Intent getIntentNext() {
//		return intentNext;
//	}
//
//
//
//	public void setIntentNext(Intent intentNext) {
//		this.intentNext = intentNext;
//	}



	public int getNextFiltroId() {
		return nextFiltroId;
	}



	public void setNextFiltro(int nextFiltroId) {
		this.nextFiltroId = nextFiltroId;
	}



	public int getFiltroId() {
		return filtroId;
	}



	public Class<?> getNextClass() {
		return NextClass;
	}



	public void setNextClass(Class<?> nextClass) {
		NextClass = nextClass;
	}



	public void setFiltroId(int filtroId) {
		this.filtroId = filtroId;
	}



	public String getValorFiltro() {
		return valorFiltro;
	}



	public void setValorFiltro(String valorFiltro) {
		this.valorFiltro = valorFiltro;
	}



	public TextView getTextoDescripcion() {
		return textoDescripcion;
	}



	public void setTextoDescripcion(TextView textoDescripcion) {
		this.textoDescripcion = textoDescripcion;
	}
	

}
