package es.paumancar.ocgases;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XMLParserPedidoCompraCabecera {



    // Namespace general. null si no existe
    private static final String ns = null;

    // Constantes del archivo Xml
    private static final String ETIQUETA_RESTSYNC = "restsync";

//    private static final String ETIQUETA_COMPRA_CABECERA = "PedidoCompraCabecera";
//
//    private static final String ETIQUETA_ID_PEDIDO_COMPRA = "c_order_id";
//    private static final String ETIQUETA_DOCUMENTNO = "documentno";
//    private static final String ETIQUETA_CREATED = "created";
//    private static final String ETIQUETA_NOMBRE_PROVEEDOR = "nombreproveedor";
    
    private String etiquetaCabecera;
    private String[] etiquetasDatos;
    private String[][] etiquetaCabeceraDatos;
    private int numeroDatos = 0;
    private int numeroListas = 0;
    private int filtroId; 
    private String valorFiltro;



    public XMLParserPedidoCompraCabecera(String[][] etiquetaCabeceraDatos,
			 int filtroId, String valorFiltro) {
		super();
		this.etiquetaCabeceraDatos = etiquetaCabeceraDatos;
		if (etiquetaCabeceraDatos != null) {
			this.numeroListas = etiquetaCabeceraDatos.length;
		}
		this.filtroId = filtroId;
		this.valorFiltro = valorFiltro;
	}


    
    public List<String[]>[] recuperaLista(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            //parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            parser.setInput(in, null);
            parser.nextTag();
            List<String[]>[] listaDatos = leerDatosPedidosCompraCabecera(parser);
            return filtraLista(filtroId, valorFiltro, listaDatos);
        } finally {
            in.close();
        }
    }
    
    

    private List<String[]>[] leerDatosPedidosCompraCabecera(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        ArrayList<String[]>[] listaPedidosCompraCabecera = 
        		(ArrayList<String[]>[]) new ArrayList[numeroListas];
        for (int i = 0; i < listaPedidosCompraCabecera.length; i++) {
        	listaPedidosCompraCabecera[i] = new ArrayList<String[]>();
		}
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_RESTSYNC);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nombreEtiqueta = parser.getName();
            // Buscar etiqueta <PedidoCompraCabecera>
            boolean algunaEtiquetaEncontrada = false;
            for (int i = 0; i < numeroListas; i++) {
                if (nombreEtiqueta.equals(etiquetaCabeceraDatos[i][0])) {
                	listaPedidosCompraCabecera[i].add(leerPedidoCompraCabecera(parser, i));
                	algunaEtiquetaEncontrada = true;
                }	
			}

            if (!algunaEtiquetaEncontrada) {
                saltarEtiqueta(parser);
            }
        }
        return listaPedidosCompraCabecera;
    }


    private String[] leerPedidoCompraCabecera(XmlPullParser parser, int index) 
    		throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, etiquetaCabeceraDatos[index][0]);
        int numeroDatos = etiquetaCabeceraDatos[index].length - 1;
        String[] stringReturn = new String[numeroDatos];

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            boolean etiquetaEncontrada = false;
            
            for (int i = 1; i < etiquetaCabeceraDatos[index].length; i++) {
				if (name.equals(etiquetaCabeceraDatos[index][i])){
					stringReturn[i-1] = leerTextoEtiqueta(parser, etiquetaCabeceraDatos[index][i]);
					etiquetaEncontrada = true;
					break;
				}
			}
            
            if (!etiquetaEncontrada)  {
            	saltarEtiqueta(parser);
            }
            
        }

        return stringReturn;
    }

    // Procesa la etiqueta que se indica de los pedidos de compra
    private String leerTextoEtiqueta(XmlPullParser parser, String etiqueta) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, etiqueta);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, etiqueta);
        return nombre;
    }


    // Obtiene el texto de los atributos
    private String obtenerTexto(XmlPullParser parser) throws IOException, XmlPullParserException {
        String resultado = "";
        if (parser.next() == XmlPullParser.TEXT) {
            resultado = parser.getText();
            parser.nextTag();
        }
        return resultado;
    }

    // Salta aquellos objetos que no interesen en la jerarquía XML.
    private void saltarEtiqueta(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
    


	private List<String[]>[] filtraLista(int filtroId, String valorFiltro,
			List<String[]>[] listaDatos) {
		 ArrayList<String[]>[] listaFiltrada = (ArrayList<String[]>[]) new ArrayList[numeroListas];
	        for (int i = 0; i < listaFiltrada.length; i++) {
	        	listaFiltrada[i] = new ArrayList<String[]>();
	    		for (Iterator iterator = listaDatos[i].iterator(); iterator.hasNext();) {
	    			String[] datosAux = (String[]) iterator.next();
	    			if (filtroId < 0){
	    				listaFiltrada[i].add(datosAux);
	    			} else if (datosAux[filtroId].equals(valorFiltro)){
	    				listaFiltrada[i].add(datosAux);
	    			}
	    		}
	        	
			}
		return listaFiltrada;
	}
    
    


}