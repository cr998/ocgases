package es.paumancar.ocgases.reports;

import java.io.UnsupportedEncodingException;
import java.util.List;

import es.paumancar.ocgases.MySQLiteHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Base64;

public class HtmlReport {
	

	List<String[]> listaLineasReport;
	
	private String htmlReport = "";


	public HtmlReport(List<String[]> listaLineasReport) {	
		this.listaLineasReport = listaLineasReport;
		htmlReport = CreaHtmlReport();
		}

	private String CreaHtmlReport() {
		StringBuilder htmlString = new StringBuilder();
		String cabeceraReport = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
				+ "\n\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" + 
				"</head>\n" + 
				"<body>\n" +
				"</br>\n" +
				"<pre>\n" +
				//"<table>\n" + 
				//"<tbody>\n"
				"";
		htmlString.append(cabeceraReport);
		//TODO insertar imagen
		
		for (String[] stringLinea : listaLineasReport) {
			htmlString.append(insertaFilaTabla(stringLinea));
		}
		
		
		htmlString.append(//"</tbody>\n" + 
				//"</table>\n" + 
				"</body>\n"
				+ "</pre>\n" 
				+ "</html>");
	
		return htmlString.toString();
	}
	
	
	private String encodeBase64(String htmlString){
		String htmlStringBase64 = "";
		try {
			byte[] data = htmlString.toString().getBytes("UTF-8");
			htmlStringBase64 = Base64.encodeToString(data, Base64.DEFAULT);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return htmlStringBase64;
	}
	
	
	private String insertaFilaTabla(String[] stringLinea) {
		String numeroFila = stringLinea[0];
		String formato = stringLinea[1];
		String valor = stringLinea[2].replaceAll("\n", "</br>");
		valor = valor.replaceAll("á", "&aacute;");
		valor = valor.replaceAll("é", "&eacute;");
		valor = valor.replaceAll("í", "&iacute;");
		valor = valor.replaceAll("ó", "&oacute;");
		valor = valor.replaceAll("ú", "&uacute;");
		valor = valor.replaceAll("Á", "&Aacute;");
		valor = valor.replaceAll("É", "&Eacute;");
		valor = valor.replaceAll("Í", "&Iacute;");
		valor = valor.replaceAll("Ó", "&Oacute;");
		valor = valor.replaceAll("Ú", "&Uacute;");
		valor = valor.replaceAll("ñ", "&ntilde;");
		valor = valor.replaceAll("Ñ", "&Ntilde;");
//		String textoInicio = "<tr>\n" + 
//				"<td class=\"line-number\" value=\"" + 
//				numeroFila + "\"></td>\n" + 
//				"<td class=\"line-content\">";
//		String textoFinal =  "</td>\n" + 
//				"</tr>\n";
		
		String textoInicio = "";
		String textoFinal =  "\n";
		if(formato.equals(PrintFormats.BOLD) 
				| formato.equals(PrintFormats.BOLD_DOUBLE_HIGH)
				 | formato.equals(PrintFormats.BOLD_DOUBLE_WIDE)
				 | formato.equals(PrintFormats.BOLD_COMPRESS)){
			valor = "<b>" + valor + "</b>";
		}
		if(formato.equals(PrintFormats.ITALIC)){
			valor = "<i>" + valor + "</i>";
		}
		if (formato.equals(PrintFormats.IMAGE_ENCODED_BASE64)){
			valor = "</pre><img src=\"" + valor + "\"/><br/><pre><br/>";
		}
		if (formato.equals(PrintFormats.IMAGE_FILE)){
			valor = "</pre><img src=\"" + valor.substring(9) + "\"/></p></p><pre><br/>";
		}
		if(formato.equals(PrintFormats.LINE)){
			valor = "--------------------------------------------------------------------------------<br/>";
		}
		
		return textoInicio + valor + textoFinal;
	}
	
	
	public String getHtmlString(){
		return htmlReport;
	}
	
	public String getHtmStringBase64Encoded(){
		return encodeBase64(htmlReport);
	}
}
