package es.paumancar.ocgases.reports;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.UUID;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.util.Log;

public class MyLinePrinter {
	
	  public static final int GraphicRotationDegrees_DEGREE_0 = 0;
	  BluetoothSocket btSocket = null;
	  OutputStream outStream = null;
	  BluetoothDevice localBluetoothDevice = null;
	  final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	  
	  String MyBDA = "00:06:66:08:A5:C3";
	  BluetoothAdapter mBluetoothAdapter = null;
	  int initial_bt_state;
	  
	  byte[] inicializa = {0x00,0x00,0x00,0x00,0x1b,0x40,0x00,0x00,0x00};
	  
	  boolean esLenguajeEsp = true;

	public MyLinePrinter( String macAddress) {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    if (mBluetoothAdapter == null)
	    {
//	      Toast.makeText(this, "Bluetooth is not available.", 1).show();
//	      finish();
	    	Log.w("OCGASES_BT", "Bluetooth is not available.");
	      return;
	    }
	    if (!mBluetoothAdapter.isEnabled())
	    {
	      BluetoothAdapter.getDefaultAdapter().enable();
	      this.initial_bt_state = 0;
	      return;
	    }
	    this.initial_bt_state = 1; 
	    MyBDA = macAddress;
	    localBluetoothDevice = mBluetoothAdapter.getRemoteDevice(MyBDA);
	    if (localBluetoothDevice == null) {
	    	Log.w("OCGASES_BT", "localBluetoothDevice es nulo");
	    }
	}

	public boolean connect(){
	   
	    try
	    {
	    	btSocket = localBluetoothDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID); 
	    	Method m = localBluetoothDevice.getClass().getMethod("createInsecureRfcommSocket", 
	    			new Class[] {int.class}); 
	    	btSocket = (BluetoothSocket) m.invoke(localBluetoothDevice, 1); 
	    	mBluetoothAdapter.cancelDiscovery(); 
	    	//btSocket.connect();
	    	//btSocket = localBluetoothDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID);
	    	//UUID uuid = localBluetoothDevice.getUuids()[0].getUuid();
	    	//btSocket = localBluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID);
	    	//mBluetoothAdapter.cancelDiscovery();
	    	if (btSocket == null){
	    		Log.w("OCGASES_BT", "btSocket es NULO");
	    	} else {
	    		Log.w("OCGASES_BT", "btSocket creado, no nulo");
	    	}
	    	Log.w("OCGASES_BT", "connect Creando conexión");
	      try
	      {
	        btSocket.connect();
	        Log.w("OCGASES_BT", "btSocket connect OK");
	        outStream = btSocket.getOutputStream();
	        Log.w("OCGASES_BT", "btSocket getOutputStream OK");
	    	outStream.write(inicializa);
	    	Log.w("OCGASES_BT", "connect Inicializa");
	        return true;
	      }
	      catch (IOException localIOException2)
	      {
	    	  Log.w("OCGASES_BT", "connect Inicializa error");
	    	  Log.w("OCGASES_BT", "connect " + localIOException2.getMessage());
	    	  try
	    	  {
	    		  this.btSocket.close();
	    		  mBluetoothAdapter.cancelDiscovery();
	    		  return false;
	    	  }
	    	  catch (IOException localIOException3)
	    	  {
	    		  return false;
	    	  }
	      }
	    }
	    catch (IOException localIOException1)
	    {
	    	Log.w("OCGASES_BT", "connect Creando conexión error");
	    	Log.w("OCGASES_BT", "connect " + localIOException1.getMessage());
	      return false;
	    } catch (NoSuchMethodException e) {
	    	Log.w("OCGASES_BT", "connect NoSuchMethodException " + e.getMessage());
		} catch (IllegalAccessException e) {
			Log.w("OCGASES_BT", "connect IllegalAccessException " + e.getMessage());
		} catch (IllegalArgumentException e) {
			Log.w("OCGASES_BT", "connect IllegalArgumentException " + e.getMessage());
		} catch (InvocationTargetException e) {
			Log.w("OCGASES_BT", "connect InvocationTargetException " + e.getMessage());
		}
		return false;
	}
	
	public boolean disconnect(){
	    if (this.outStream != null){
		    try
		    {
		      this.outStream.flush();
		      Log.w("OCGASES_BT", "disconnect flush");
		    }
		    catch (IOException localIOException2)
		    {
		    	Log.w("OCGASES_BT", "disconnect flush error");
		    	Log.w("OCGASES_BT", "disconnect " + localIOException2.getMessage());
		      return false;
		    }
	    }
	    try
	      {
	        //label18: 
	        this.btSocket.close();
	        mBluetoothAdapter.cancelDiscovery();
	        localBluetoothDevice = null;
	        mBluetoothAdapter = null;
	        Log.w("OCGASES_BT", "disconnect close");
	        return true;
	      }
	      catch (IOException localIOException1)
	      {
	    	  Log.w("OCGASES_BT", "disconnect close error");
	    	  Log.w("OCGASES_BT", "disconnect " + localIOException1.getMessage());
	    	  return false;
	      }
	    //return false;
	}

	public void write(String dato) {
		String valor = dato;
		if (esLenguajeEsp){
//			valor = valor.replaceAll("á", "a");
//			valor = valor.replaceAll("é", "e");
//			valor = valor.replaceAll("í", "i");
//			valor = valor.replaceAll("ó", "o");
//			valor = valor.replaceAll("ú", "u");
			valor = valor.replaceAll("Á", "A");
			valor = valor.replaceAll("É", "E");
			valor = valor.replaceAll("Í", "I");
			valor = valor.replaceAll("Ó", "O");
			valor = valor.replaceAll("Ú", "U");
//			valor = valor.replaceAll("ñ", "n");
//			valor = valor.replaceAll("Ñ", "N");
		}
		try {
			outStream.write(valor.getBytes("IBM850"));
		} catch (UnsupportedEncodingException e1) { 
			e1.printStackTrace();
//		      e1   e9   ed   f3   fa  f1   d1
//		      a0   82   a1   a2   a3  a4   a5
			try {
				byte[] valorBytes = valor.getBytes("ISO-8859-1");
				for (int i = 0; i < valorBytes.length; i++) {
					switch (valorBytes[i]) {
					case (byte) 0xe1:
						valorBytes[i] = (byte) 0xa0;
						break;
					case (byte) 0xe9:
						valorBytes[i] = (byte) 0x82;
						break;
					case (byte) 0xed:
						valorBytes[i] = (byte) 0xa1;
						break;
					case (byte) 0xf3:
						valorBytes[i] = (byte) 0xa2;
						break;
					case (byte) 0xfa:
						valorBytes[i] = (byte) 0xa3;
						break;
					case (byte) 0xf1:
						valorBytes[i] = (byte) 0xa4;
						break;
					case (byte) 0xd1:
						valorBytes[i] = (byte) 0xa5;
						break;
					default:
						break;
					}
				}
				outStream.write(valorBytes);
			} catch (UnsupportedEncodingException e11) {
				e11.printStackTrace();
				if (esLenguajeEsp){
					valor = valor.replaceAll("á", "a");
					valor = valor.replaceAll("é", "e");
					valor = valor.replaceAll("í", "i");
					valor = valor.replaceAll("ó", "o");
					valor = valor.replaceAll("ú", "u");
					valor = valor.replaceAll("ñ", "n");
					valor = valor.replaceAll("Ñ", "N");
				}
				try {
					outStream.write(valor.getBytes());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e12) {
				e12.printStackTrace();
			}
		} catch (IOException e2) { 
			e2.printStackTrace();
		}
	}

	public void newLine(int i) {
		try {
			for (int j = 0; j < i; j++) {
				outStream.write("\n".getBytes());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setBold(boolean b) {
		byte[] boldCharON = {0x1b,0x77,0x6d};
        byte[] boldCharOFF = {0x1b,0x77,0x21};
		if (b) {
			try {
				outStream.write(boldCharON);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				outStream.write(boldCharOFF);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setItalic(boolean b) {
		// TODO Auto-generated method stub
		
	}

	public void setDoubleHigh(boolean b) {
		byte[] doubleHighOff = {0x1b,0x21,0x10};
		byte[] doubleHighOn = {0x1b,0x21,0x10};
		if (b) {
			try {
				outStream.write(doubleHighOn);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				outStream.write(doubleHighOff);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setCompress(boolean b) {
        byte[] compressCharOFF = {0x1b,0x77,0x21};
        byte[] compressCharON = {0x1b,0x77,0x42};
        if (b) {
			try {
				outStream.write(compressCharON);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				outStream.write(compressCharOFF);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setStrikeout(boolean b) {
		// TODO Auto-generated method stub
		
	}

	public void setUnderline(boolean b) {
		// TODO Auto-generated method stub
		
	}

	public void writeLine(String string) {
		write(string);
		newLine(1);
	}
	public void writeGraphicBase64(String base64SignaturePng, int degree0,
			int offset, int imageWidth, int imageHeigth) {
		byte[] decodedString = Base64.decode(base64SignaturePng, Base64.DEFAULT);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		writeGraphicBitmap(decodedByte, degree0, offset, imageWidth, imageHeigth);
	}
	
	public void writeGraphic(String absolutePath, int degree0, int offset,
			int imageWidth, int imageHeigth) {
		Bitmap bitmap=BitmapFactory.decodeFile(absolutePath);
		writeGraphicBitmap(bitmap, degree0, offset, imageWidth, imageHeigth);
	}
	
	public void writeGraphicBitmap(Bitmap bitmap, int degree0, int offset,
			int imageWidth, int imageHeigth) {
    	int levelTr = -15000000;
		//Bitmap bitmap=BitmapFactory.decodeFile(absolutePath);
		int width = bitmap.getWidth();
	    int height = bitmap.getHeight();
	    int newWidth = imageWidth;
        int newHeight = imageHeigth;
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        //matrix.postRotate(degree0);
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                          width, height, matrix, true);
		width = resizedBitmap.getWidth();
	    height = resizedBitmap.getHeight();
	    int[] pixels = new int[width * height];
	    resizedBitmap.getPixels(pixels, 0, width, 0, 0, width, height);
  	  	int cuentaBytes = 0;
  	  	byte grupos256Lin = (byte) (height/256);
  	  	byte resto256 = (byte) (height%256);
  	  	int minPixels = min(pixels);
  	  	int maxPixels = max(pixels);
  	  	levelTr = (minPixels + maxPixels)/2;
  	  	Log.w("OCGASES", "Pixels m M T: " +  String.valueOf(minPixels) 
  	  			+ " " + String.valueOf(maxPixels)
  	  		+ " " + String.valueOf(levelTr));
  	  	if (minPixels == maxPixels) {
  	  		levelTr --;
  	  	}
  	  	byte[] bitmapGon = {0x1b,0x56,grupos256Lin, resto256};
  	  	try {
			outStream.write(bitmapGon);
	  	  	for (int i = 0; i < height; i++) {
	    		  byte[] linea = new byte[104];
	    		  for (int j = 0; j < 104; j++) {
	    			  String byteString = "";
	    			  for (int k = 0; k < 8; k++) {
	    				  if (((j*8 + k) < offset) | ((j*8 + k) >= (offset + width))) {
	    					byteString = byteString + "0";
	    				  } else {
	  					if (pixels[cuentaBytes] > levelTr) {
	  						byteString = byteString + "0";
	  					} else {
	  						byteString =  byteString + "1";
	  					}
	  					cuentaBytes ++;
	    				  }
	    			  }
	    			  
	    			  byte bAux = (byte) Integer.parseInt(byteString, 2);
	    			  linea[j] = bAux;
	  			}
	    		  outStream.write(linea);
	  		}
		} catch (IOException e) {
			try
	          {
	            outStream.flush();
	            return;
	          }
	          catch (IOException localIOException3)
	          {
	          }
			e.printStackTrace();
		}
	}
	
	   int max(int[] array) {
	      int max = array[0];
	      for (int i = 1; i < array.length; i++) {
	          if (array[i] > max) {
	              max = array[i];
	          }
	      }
	  
	      return max;
	  }
	   
	   int min(int[] array) {
		      int min = array[0];
		      for (int i = 1; i < array.length; i++) {
		          if (array[i] < min) {
		              min = array[i];
		          }
		      }
		  
		      return min;
		  }
	
}
