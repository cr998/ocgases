package es.paumancar.ocgases.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;

import com.intermec.print.PrinterException;
import com.intermec.print.lp.LinePrinter;
import com.intermec.print.lp.LinePrinterException;
import com.intermec.print.lp.PrintProgressEvent;
import com.intermec.print.lp.PrintProgressListener;

public class IntermecPrint extends AsyncTask<String, Integer, String> {
	private static final String PROGRESS_CANCEL_MSG = "Printing cancelled\n";
	private static final String PROGRESS_COMPLETE_MSG = "Printing completed\n";
	private static final String PROGRESS_ENDDOC_MSG = "End of document\n";
	private static final String PROGRESS_FINISHED_MSG = "Printer connection closed\n";
	private static final String PROGRESS_NONE_MSG = "Unknown progress message\n";
	private static final String PROGRESS_STARTDOC_MSG = "Start printing document\n";

	
	private Context context;
	private List<String[]> listAlbaranLines;
	private String base64SignaturePng;
	private String firmaSizeOWH;
	
	

	public IntermecPrint(Context context, List<String[]> listAlbaranLines, 
			String base64EncodedImage, String firmaSizeOWH ) {
		super();
		this.context = context;
		this.listAlbaranLines = listAlbaranLines;
		this.base64SignaturePng = base64EncodedImage;
		this.firmaSizeOWH = firmaSizeOWH;
	}

	/**
	 * Runs on the UI thread before doInBackground(Params...).
	 */
	@Override
	protected void onPreExecute()
	{
		// Clears the Progress and Status text box.
		//textMsg.setText("");
		//copyAssetFiles();


		// Shows a progress icon on the title bar to indicate
		// it is working on something.
		//setProgressBarIndeterminateVisibility(true);
	}

	/**
	 * This method runs on a background thread. The specified parameters
	 * are the parameters passed to the execute method by the caller of 
	 * this task. This method can call publishProgress to publish updates
	 * on the UI thread.
	 */
	@Override
	protected String doInBackground(String... args)
	{
		//LinePrinter lp = null;
		MyLinePrinter lp = null;
		String sResult = null;
		String sPrinterID = args[0];
		String sMacAddr = args[1];
		String sDocNumber = "1234567890";
		
		if (sMacAddr.contains(":") == false && sMacAddr.length() == 12)
		{
			// If the MAC address only contains hex digits without the
			// ":" delimiter, then add ":" to the MAC address string.
			char[] cAddr = new char[17];
			
			for (int i=0, j=0; i < 12; i += 2)
			{
				sMacAddr.getChars(i, i+2, cAddr, j);
				j += 2;
				if (j < 17)
				{
					cAddr[j++] = ':';
				}
			}
			
			sMacAddr = new String(cAddr);
		}
		
		String sPrinterURI = "bt://" + sMacAddr;
		

		LinePrinter.ExtraSettings exSettings = new LinePrinter.ExtraSettings();
		
		exSettings.setContext(context);

		try
		{
			File profiles = new File (context.getExternalFilesDir(null), "printer_profiles.JSON");
			
//			lp = new LinePrinter(
//					profiles.getAbsolutePath(),
//					sPrinterID,     
//					sPrinterURI,
//					exSettings);
			lp = new MyLinePrinter(sMacAddr);
			
			// Registers to listen for the print progress events.
//			lp.addPrintProgressListener(new PrintProgressListener() {
//				public void receivedStatus(PrintProgressEvent aEvent)
//				{
//					// Publishes updates on the UI thread.
//					publishProgress(aEvent.getMessageType());
//				}
//			});
			
			//A retry sequence in case the bluetooth socket is temporarily not ready
			int numtries = 0;
			int maxretry = 2;
			while(numtries < maxretry)
			{
				try{
					lp.connect();  // Connects to the printer
					break;
				}
				catch(/*MyLinePrinter*/Exception ex){
					numtries++;
					Thread.sleep(1000);
				}
			}
			if (numtries == maxretry) lp.connect();//Final retry 
//
//			lp.writeLine("\u001bEZ{PRINT:@1,1:ocgases_logo2,HMULT1,VMULT1|}\n{LP}\n");
//			lp.writeLine("-----------------------------------");
			//lp.sendCustomCommand("pt \"This is a test\" ");
		
			for (String[] lineaString : listAlbaranLines) {
				imprimeLinea(lp, lineaString);
			}
			
			//sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
		}
//		catch (LinePrinterException ex)
//		{
//			sResult = "LinePrinterException: " + ex.getMessage();
//		}
		catch (Exception ex)
		{
			if (ex.getMessage() != null)
				sResult = "Unexpected exception: " + ex.getMessage();
			else
				sResult = "Unexpected exception.";
		}
		finally
		{
			if (lp != null)
			{
				try
				{
					lp.disconnect();  // Disconnects from the printer
					//lp.close();  // Releases resources
				}
				catch (Exception ex) {}
			}
		}

		// The result string will be passed to the onPostExecute method
		// for display in the the Progress and Status text box.
		return sResult;
	}


	/**
	 * Runs on the UI thread after publishProgress is invoked. The
	 * specified values are the values passed to publishProgress.
	 */
	@Override
	protected void onProgressUpdate(Integer... values)
	{
		// Access the values array.
		int progress = values[0];
		
//		switch (progress)
//		{
//		case PrintProgressEvent.MessageTypes.CANCEL:
//			textMsg.append(PROGRESS_CANCEL_MSG);
//			break;
//		case PrintProgressEvent.MessageTypes.COMPLETE:
//			textMsg.append(PROGRESS_COMPLETE_MSG);
//			break;
//		case PrintProgressEvent.MessageTypes.ENDDOC:
//			textMsg.append(PROGRESS_ENDDOC_MSG);
//			break;
//		case PrintProgressEvent.MessageTypes.FINISHED:
//			textMsg.append(PROGRESS_FINISHED_MSG);
//			break;
//		case PrintProgressEvent.MessageTypes.STARTDOC:
//			textMsg.append(PROGRESS_STARTDOC_MSG);
//			break;
//		default:
//			textMsg.append(PROGRESS_NONE_MSG);
//			break;
//		}
	}

	/**
	 * Runs on the UI thread after doInBackground method. The specified
	 * result parameter is the value returned by doInBackground.
	 */
	@Override
	protected void onPostExecute(String result)
	{
		// Displays the result (number of bytes sent to the printer or
		// exception message) in the Progress and Status text box.
//		if (result != null)
//		{
//			textMsg.append(result);
//		}
//		
//		// Dismisses the progress icon on the title bar.
//		setProgressBarIndeterminateVisibility(false);

	}
	

	private void imprimeLinea(MyLinePrinter lp, String[] lineaString) throws PrinterException {
		String formato = lineaString[1];
		String dato = lineaString[2];
		
		if(formato.equals(PrintFormats.NORMAL) | formato.equals(PrintFormats.TEXTO_LIBRE)){
			lp.write(dato);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.BOLD)){
			lp.setBold(true);
			lp.write(dato);
			lp.setBold(false);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.ITALIC)){
			lp.setItalic(true);
			lp.write(dato);
			lp.setItalic(false);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.DOUBLE_HIGH)){
			lp.setDoubleHigh(true);
			lp.write(dato);
			lp.setDoubleHigh(false);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.DOUBLE_WIDE)){
			lp.setDoubleHigh(true);
			lp.write(dato);
			lp.setDoubleHigh(false);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.COMPRESS)){
			lp.setCompress(true);
			lp.write(dato);
			lp.setCompress(false);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.STRIKEOUT)){
			lp.setStrikeout(true);
			lp.write(dato);
			lp.setStrikeout(false);
			lp.newLine(1);
			return;
		}
		if(formato.equals(PrintFormats.UNDERLINE)){
			lp.setUnderline(true);
			lp.write(dato);
			lp.setUnderline(false);
			lp.newLine(1);
			return;
		}
		
		if(formato.equals(PrintFormats.BOLD_DOUBLE_HIGH)){
			lp.setBold(true);
			lp.setDoubleHigh(true);
			lp.write(dato);
			lp.setDoubleHigh(false);
			lp.setBold(false);
			lp.newLine(1);
			return;
		}
		
		if(formato.equals(PrintFormats.IMAGE_ENCODED_BASE64)){
			printImageBase64Encoded(lp);
			return;
		}
		if(formato.equals(PrintFormats.IMAGE_FILE)){
			printImageFromFile(lp, dato);;
			return;
		}
		
		if(formato.equals(PrintFormats.LINE)){
			lp.writeLine("--------------------------------------------------------------------------------");
			return;
		}
	}
	
	private void printImageFromFile(MyLinePrinter lp, String listaImageFileNameOWH) throws LinePrinterException{
		String[] listaImageFiles = listaImageFileNameOWH.split(",");
		for (String imageFileNameOWH : listaImageFiles) {
			String imageFileName = imageFileNameOWH.substring(9);
			int offset = Integer.valueOf(imageFileNameOWH.substring(0,3));
			int imageWidth = Integer.valueOf(imageFileNameOWH.substring(3,6));
			int imageHeigth = Integer.valueOf(imageFileNameOWH.substring(6,9));
			File graphicFile = new File (context.getExternalFilesDir(null), imageFileName);
			if (graphicFile.exists())
			{	
				lp.writeGraphic(graphicFile.getAbsolutePath(),
						MyLinePrinter.GraphicRotationDegrees_DEGREE_0,
						offset,  // Offset in printhead dots from the left of the page
						imageWidth, // Desired graphic width on paper in printhead dots
						imageHeigth); // Desired graphic height on paper in printhead dots

			}
		}
		lp.newLine(1);
	}
	
	private void printCodeBar(LinePrinter lp, String codebarString)  throws LinePrinterException{
		// Print a Code 39 barcode containing the document number.
		lp.writeBarcode(LinePrinter.BarcodeSymbologies.SYMBOLOGY_CODE39, 
				codebarString,  // Document# to encode in barcode
				90,           	// Desired height of the barcode in printhead dots
				40); 			// Offset in printhead dots from the left of the page
		lp.newLine(1);        
	}
	
	private void printImageBase64Encoded(MyLinePrinter lp) throws LinePrinterException{
		// Prints the captured signature if it exists.
		int offset = Integer.valueOf(firmaSizeOWH.substring(0,3));
		int imageWidth = Integer.valueOf(firmaSizeOWH.substring(3,6));
		int imageHeigth = Integer.valueOf(firmaSizeOWH.substring(6,9));
		if (base64SignaturePng != null){
			lp.writeGraphicBase64(base64SignaturePng,
					MyLinePrinter.GraphicRotationDegrees_DEGREE_0,
					offset,   // Offset in printhead dots from the left of the page
					imageWidth,  // Desired graphic width on paper in printhead dots
					imageHeigth); // Desired graphic height on paper in printhead dots
		}
		lp.newLine(1);
	}
	


} //endofclass IntermecPrint
