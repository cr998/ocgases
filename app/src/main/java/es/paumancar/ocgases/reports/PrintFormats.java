package es.paumancar.ocgases.reports;

import android.util.Log;

public class PrintFormats {

	static final String NORMAL= "10";
	static final String BOLD = "11";
	static final String ITALIC = "12";
	static final String DOUBLE_HIGH = "13";
	static final String DOUBLE_WIDE = "14";
	static final String COMPRESS = "15";
	static final String STRIKEOUT = "16";
	static final String UNDERLINE = "17";
	static final String TEXTO_LIBRE = "18";
	
	static final String BOLD_ITALIC = "22";
	static final String BOLD_DOUBLE_HIGH = "23";
	static final String BOLD_DOUBLE_WIDE = "24";
	static final String BOLD_COMPRESS = "25";
	static final String BOLD_STRIKEOUT = "26";
	static final String BOLD_UNDERLINE = "27";
	
	static final String ITALIC_DOUBLE_HIGH = "33";
	static final String ITALIC_DOUBLE_WIDE = "34";
	static final String ITALIC_COMPRESS = "35";
	static final String ITALIC_STRIKEOUT = "36";
	static final String ITALIC_UNDERLINE = "37";
	
	
	static final String STRIKEOUT_DOUBLE_HIGH = "43";
	static final String STRIKEOUT_DOUBLE_WIDE = "44";
	static final String STRIKEOUT_COMPRESS = "45";
	static final String STRIKEOUT_UNDERLINE = "47";
	
	
	static final String IMAGE_FILE = "100";
	static final String IMAGE_ENCODED_BASE64 = "101";
	
	public static final String TEXTO_LOPD_LSSI_1 = 
			"En cumplimiento de lo dispuesto en la Ley Orgánica 15/1999 de "
    		+ "Protección de Datos de Carácter Personal, OXIGENO CASTAÑO le informa que "
    		+ "los Datos Personales recogidos incluida la firma en este documento digitalizado "
    		+ "(o albaranes relacionados), forman parte de ficheros normalizados cuyo titular "
    		+ "es Oxigeno Castaño y cuya finalidad es la gestión administrativa de la relación "
    		+ "negocial y/o comercial, y el  envío de información comercial relacionada con esta; "
    		+ "tratamiento que autoriza. El Cliente autoriza expresamente a Oxígeno Castaño a ceder "
    		+ "los Datos Personales de este documento a Air Liquide España con la finalidad de que "
    		+ "pueda gestionar directa o indirectamente el suministro de gases industriales y "
    		+ "documentación del servicio. Puede ejercer los derechos de acceso e información, "
    		+ "rectificación, cancelación y oposición formulando una comunicación escrita y "
    		+ "firmada indicando su dirección mail, fotocopia del DNI, así como el tipo de derecho "
    		+ "que desea ejercitar, en la dirección de: Oxígeno Castaño, Polígono Industrias Asipo, "
    		+ "calle A parcela 1 nave 4, 33428 LLanera - Asturias. O en la dirección electrónica:  "
    		+ "lopd@oxigenocastano.es.";
	
	public static final String TEXTO_LOPD_LSSI_2 = 
			"En cumplimiento de lo dispuesto en la Ley Orgánica 15/1999 de "
    		+ "Protección de Datos de Carácter Personal, OXIGENO CASTAÑO le informa que "
    		+ "los Datos Personales recogidos incluida la firma en este documento digitalizado "
    		+ "(o albaranes relacionados), forman parte de ficheros normalizados cuyo titular "
    		+ "es Oxigeno Castaño y cuya finalidad es la gestión administrativa de la relación "
    		+ "negocial y/o comercial, y el  envío de información comercial relacionada con esta; "
    		+ "tratamiento que autoriza. El Cliente autoriza expresamente a Oxígeno Castaño a ceder "
    		+ "los Datos Personales de este documento a Gas Servei con la finalidad de que "
    		+ "pueda gestionar directa o indirectamente el suministro de gases industriales y "
    		+ "documentación del servicio. Puede ejercer los derechos de acceso e información, "
    		+ "rectificación, cancelación y oposición formulando una comunicación escrita y "
    		+ "firmada indicando su dirección mail, fotocopia del DNI, así como el tipo de derecho "
    		+ "que desea ejercitar, en la dirección de: Oxígeno Castaño, Polígono Industrias Asipo, "
    		+ "calle A parcela 1 nave 4, 33428 LLanera - Asturias. O en la dirección electrónica:  "
    		+ "lopd@oxigenocastano.es.";
	
	
	
	static final String LINE = "105";
	
	public static String formateaAnchoString( String texto, int ancho){
		if (texto.length() < (ancho + 1)){
			return texto;
		}
		int cuentaLineas = 1;
		int finPrimeraLinea = finPrimeraLinea(texto, ancho);
		String primeraLinea = texto.substring(0, finPrimeraLinea + 1);
		Log.w("OCGASES_PDF", String.valueOf(cuentaLineas) + ": " + primeraLinea);
		String totalLinea = primeraLinea;
		String restoLinea = texto.substring(finPrimeraLinea + 1);
		if((restoLinea.length() > 0) & restoLinea.substring(0, 1).equals(" ")){
			restoLinea = restoLinea.substring(1);
		}
		while (restoLinea.length() > ancho){
			finPrimeraLinea = finPrimeraLinea(restoLinea, ancho);
			primeraLinea = restoLinea.substring(0, finPrimeraLinea + 1);
			cuentaLineas ++;
			//Log.w("OCGASES_PDF", String.valueOf(cuentaLineas) + ": " + primeraLinea);
			String restoLineaAux = restoLinea.substring(finPrimeraLinea + 1);
			restoLinea = restoLineaAux;
			if((restoLinea.length() > 0) & restoLinea.substring(0, 1).equals(" ")){
				restoLinea = restoLinea.substring(1);
			}
			totalLinea = totalLinea + "\n" + primeraLinea;
		}
		return totalLinea + "\n" + restoLinea;
	}

	private static int finPrimeraLinea(String texto, int ancho) {
		if (texto.length() < (ancho + 1)){
			return texto.length()-1;
		}
		String primeraLinea = texto.substring(0, ancho+1);
		if (primeraLinea.substring(ancho).equals(" ") | primeraLinea.substring(ancho).equals("\n") ){
			return (ancho-1);
		}
		int posicion = ancho-1;
		for (int i = 0; i < primeraLinea.length(); i++) {
			String caracter = primeraLinea.substring(ancho-1-i, ancho-i);
			if (caracter.equals(" ")){
				posicion = ancho - 2 - i;
				break;
			}
		}
		return posicion;
	}
	
	

}
