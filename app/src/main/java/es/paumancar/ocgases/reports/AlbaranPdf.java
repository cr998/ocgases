package es.paumancar.ocgases.reports;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.intermec.print.lp.LinePrinterException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfDocument.Page;
import android.graphics.pdf.PdfDocument.PageInfo;
import android.os.Environment;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class AlbaranPdf {
	Context context;
	private String base64SignaturePng;
	private String firmaSizeOWH;
	
	private static int TEXT_SIZE = 10;
	private static int NUMERO_COLUMNAS = 80;
	private static int HEADER_IMAGE_WIDTH = 500;
	private static int PAGE_WIDTH = 575;
	private static int PAGE_HEIGHT = 820;
	private static	float POSICION_Y_INICIAL = 20;
	
	PdfDocument document;
	int pageNumber;
	Canvas myPdfCanvas;
	Page paginaActual = null;
	float posicionY;
	String pdfName;

	public AlbaranPdf(Context context, String numeroAlbaran, List<String[]> listAlbaranLines, 
			String base64EncodedImage, String firmaSizeOWH ) {
		this.context = context;
		this.base64SignaturePng = base64EncodedImage;
		this.firmaSizeOWH = firmaSizeOWH;
		File ruta_sd = Environment.getExternalStorageDirectory();
		File fileDir = new File(ruta_sd.getPath() ,  "ocgases/pdfs/");
		if (!fileDir.exists()){
			fileDir.mkdirs();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
		pdfName = numeroAlbaran + "_" 
		      + sdf.format(Calendar.getInstance().getTime()) + ".pdf";
		
		document = new PdfDocument();
		pageNumber = 0;
		posicionY = POSICION_Y_INICIAL;
		
		creaNuevaPagina();


		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setStyle(Style.FILL);
		paint.setTypeface(Typeface.MONOSPACE);
		paint.setTextSize(TEXT_SIZE);
		int altoLinea = (int) (TEXT_SIZE * 1.15);
		
		
		for (String[] stringLinea : listAlbaranLines) {
			imprimeFilaPdf(stringLinea, altoLinea, paint);
			//viewTotal.addView(viewLinea);
		}
		//content.draw(myPdfCanvas);
		//viewTotal.draw(myPdfCanvas);
		
		document.finishPage(paginaActual);
		

		File outputFile = new File(fileDir ,  pdfName);
		 
		try {
		   outputFile.createNewFile();
		   OutputStream out = new FileOutputStream(outputFile);
		   document.writeTo(out);
		   document.close();
		   out.close();
		} catch (IOException e) {
		   e.printStackTrace();
		}
	}

	
	private void creaNuevaPagina() {
		if (paginaActual != null){
			document.finishPage(paginaActual);
		}
		posicionY = POSICION_Y_INICIAL;
		pageNumber ++;
		PageInfo pageInfo = new PageInfo.Builder(PAGE_WIDTH, PAGE_HEIGHT, pageNumber).create();
		Page page = document.startPage(pageInfo);
		paginaActual = page;
		myPdfCanvas = page.getCanvas();
		
	}


	private void imprimeFilaPdf(String[] stringLinea, int altoLinea, Paint paint) {
		String numeroFila = stringLinea[0];
		String formato = stringLinea[1];
		String valor = stringLinea[2];//.replaceAll("\n", "</br>");
//		valor = valor.replaceAll("á", "&aacute;");
//		valor = valor.replaceAll("é", "&eacute;");
//		valor = valor.replaceAll("í", "&iacute;");
//		valor = valor.replaceAll("ó", "&oacute;");
//		valor = valor.replaceAll("ú", "&uacute;");
//		valor = valor.replaceAll("Á", "&Aacute;");
//		valor = valor.replaceAll("É", "&Eacute;");
//		valor = valor.replaceAll("Í", "&Iacute;");
//		valor = valor.replaceAll("Ó", "&Oacute;");
//		valor = valor.replaceAll("Ú", "&Uacute;");
//		valor = valor.replaceAll("ñ", "&ntilde;");
//		valor = valor.replaceAll("Ñ", "&Ntilde;");
//		String textoInicio = "<tr>\n" + 
//				"<td class=\"line-number\" value=\"" + 
//				numeroFila + "\"></td>\n" + 
//				"<td class=\"line-content\">";
//		String textoFinal =  "</td>\n" + 
//				"</tr>\n";
		String[] valorSplit;
		if (formato.equals(PrintFormats.TEXTO_LIBRE)){
			String valorLineaAncho = valor;
			if (valor.length() > NUMERO_COLUMNAS){
				valorLineaAncho = PrintFormats.formateaAnchoString(valor, NUMERO_COLUMNAS);
			}
			valorSplit = valorLineaAncho.split("\n");
		} else {
			valorSplit = valor.split("\n");
		}
		
		
		//float posicionYFinal = posicionY;
		int posicionDato = 0;
		for (String valorLinea : valorSplit) {
			posicionDato ++;
			if ((valorLinea.equals("") | valorLinea.equals(" ")) & (posicionDato > 1)) {
				continue;
			}
			if(formato.equals(PrintFormats.BOLD) 
					 | formato.equals(PrintFormats.BOLD_DOUBLE_WIDE)
					 | formato.equals(PrintFormats.BOLD_COMPRESS)){
				if ((posicionY + (float) altoLinea) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				paint.setStyle(Style.FILL_AND_STROKE);
				myPdfCanvas.drawText(valorLinea, 15, posicionY, paint);
				posicionY = posicionY + (float) altoLinea;
				continue;
			}
			if(formato.equals(PrintFormats.BOLD_DOUBLE_HIGH) ){
				if ((posicionY + (float) altoLinea) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				paint.setStyle(Style.FILL_AND_STROKE);
				float altoTexto = paint.getTextSize();
				paint.setTextSize((float) (altoTexto*1.5));
				myPdfCanvas.drawText(valorLinea, 15, posicionY, paint);
				posicionY = (float) (posicionY + (float) altoLinea*1.4);
				paint.setTextSize(altoTexto);
				continue;
			}
			if(formato.equals(PrintFormats.ITALIC)){
				if ((posicionY + (float) altoLinea) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				myPdfCanvas.drawText(valorLinea, 15, posicionY, paint);
				posicionY = posicionY + (float) altoLinea;
				continue;
			}
			if (formato.equals(PrintFormats.IMAGE_ENCODED_BASE64)){
				if ((posicionY + (float) getimageBase64EncodedHeight()) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				int altoImagen = printImagePdfBase64Encoded(myPdfCanvas, 
						15, posicionY, paint);
				posicionY = posicionY + (float) altoImagen;
				continue;
			}
			if (formato.equals(PrintFormats.IMAGE_FILE)){
				if ((posicionY + (float) getImageFromFileHeight(valorLinea)) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				int altoImagen = printImagePdfFromFile(valorLinea, myPdfCanvas, 
						15, posicionY, paint);
				posicionY = posicionY + (float) altoImagen;
				continue;
			}
			if(formato.equals(PrintFormats.LINE)){
				if ((posicionY + (float) altoLinea) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				String valorLinea1 = "--------------------------------------------------------------------------------";
				myPdfCanvas.drawText(valorLinea1, 15, posicionY, paint);
				posicionY = posicionY + (float) altoLinea;
				continue;
			}
			if(formato.equals(PrintFormats.NORMAL) | formato.equals(PrintFormats.TEXTO_LIBRE)){
				if ((posicionY + (float) altoLinea) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
				myPdfCanvas.drawText(valorLinea, 15, posicionY, paint);
				posicionY = posicionY + (float) altoLinea;
				continue;
			}			
			if(formato.equals(PrintFormats.UNDERLINE)){
				if ((posicionY + (float) altoLinea) > PAGE_HEIGHT){
					creaNuevaPagina();
				}
					myPdfCanvas.drawText(valorLinea, 15, posicionY, paint);
				posicionY = posicionY+ (float) altoLinea;
				continue;
			}		

		}
		paint.setStyle(Style.FILL);
		//return posicionYFinal;
	}
	
	private int printImagePdfFromFile(String listaImageFileNameOWH, Canvas myCanvas,
			 float posX, float posY, Paint paint) {
		int altoImagenes = 0;
		String[] listaImageFiles = listaImageFileNameOWH.split(",");
		for (String imageFileNameOWH : listaImageFiles) {
			String imageFileName = imageFileNameOWH.substring(9);
			int offset = Integer.valueOf(imageFileNameOWH.substring(0,3));
			int imageWidth = Integer.valueOf(imageFileNameOWH.substring(3,6));
			int imageHeigth = Integer.valueOf(imageFileNameOWH.substring(6,9));
			File graphicFile = new File (context.getExternalFilesDir(null), imageFileName);
			if (graphicFile.exists())
			{	
				int imageHeigth2 = 
						(int) (((float) HEADER_IMAGE_WIDTH)*((float) imageHeigth)/((float) imageWidth));
				Bitmap bMap = BitmapFactory.decodeFile(graphicFile.getAbsolutePath());
				Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, 
						HEADER_IMAGE_WIDTH, imageHeigth2, false);
				myCanvas.drawBitmap(bMapScaled, posX, posY, paint);
				altoImagenes = altoImagenes + imageHeigth2 + 25;
			}
		}
		return altoImagenes;
	}
	
	private int getImageFromFileHeight(String listaImageFileNameOWH){
		int altoImagenes = 0;
		String[] listaImageFiles = listaImageFileNameOWH.split(",");
		for (String imageFileNameOWH : listaImageFiles) {
			String imageFileName = imageFileNameOWH.substring(9);
			int imageWidth = Integer.valueOf(imageFileNameOWH.substring(3,6));
			int imageHeigth = Integer.valueOf(imageFileNameOWH.substring(6,9));
			File graphicFile = new File (context.getExternalFilesDir(null), imageFileName);
			if (graphicFile.exists())
			{	
				int imageHeigth2 = 
						(int) (((float) HEADER_IMAGE_WIDTH)*((float) imageHeigth)/((float) imageWidth));;
				altoImagenes = altoImagenes + imageHeigth2 + 25;
			}
		}
		return altoImagenes;
	}
	
	private int printImagePdfBase64Encoded(Canvas myCanvas, float posX, float posY, Paint paint) {
		// Prints the captured signature if it exists.
		int offset = Integer.valueOf(firmaSizeOWH.substring(0,3));
		int imageWidth = Integer.valueOf(firmaSizeOWH.substring(3,6));
		int imageHeigth = Integer.valueOf(firmaSizeOWH.substring(6,9));
		if (base64SignaturePng != null){
			byte[] decodedString = Base64.decode(base64SignaturePng, Base64.DEFAULT);
			Bitmap decodedByte = 
					BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			Bitmap bMapScaled = Bitmap.createScaledBitmap(decodedByte, 
					imageWidth, imageHeigth, false);
			myCanvas.drawBitmap(bMapScaled, posX, posY, paint);
		}
		return imageHeigth + 25;
	}
	
	private int getimageBase64EncodedHeight(){
		int imageHeigth = Integer.valueOf(firmaSizeOWH.substring(6,9));
		return imageHeigth + 25;
	}
	
	public String getPdfFileName(){
		return this.pdfName;
	}
}
