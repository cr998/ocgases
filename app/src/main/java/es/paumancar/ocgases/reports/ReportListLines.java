package es.paumancar.ocgases.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import es.paumancar.ocgases.MySQLiteHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Chronometer;

public class ReportListLines {
	
	public static final String TIPO_ALBARAN_AC = "1000046";
	public static final String TIPO_ALBARAN_CR = "1000011";
	public static final String TIPO_ALBARAN_CR2 = "1000089";
	public static final String TIPO_ALBARAN_CF = "1000091"; 
	
	private static final Double VALOR_IVA = 0.21;
	
	Context context;
	String[] datosCabeceraAlbaran;
	String[] valoresCabeceraAlbaran;
	String[] cabecerasLinea;
	List<String[]> listaLineasAlbaran;
	
	private List<String[]> linesReport;
	//private String htmlReport = "";
	//private boolean ventaDelProveedor = false;
	private boolean albaranValorado = false;
	//private boolean esMoroso = false;
	private boolean pagoContado = false;
	private String tipoAlbaran = "";
	private String imagenesEncabezado = "";
	private String nombreFirma = "";
	
	int[] formatoLinea = { 10, 37, 4, 4, 9, 7, 9};
	String[] alineacionLinea = {"I", "I", "D", "D", "D", "D", "D"}; //Izquierda o Derecha

	public ReportListLines(Context context, String[] datosCabeceraAlbaran, String[] valoresCabeceraAlbaran, 
			String[] cabecerasLinea, List<String[]> listaLineasAlbaran, String nombreFirma) {
		this.context = context;
		this.datosCabeceraAlbaran = datosCabeceraAlbaran;
		this.valoresCabeceraAlbaran = valoresCabeceraAlbaran;
		this.cabecerasLinea = cabecerasLinea;
		this.listaLineasAlbaran = listaLineasAlbaran;
		this.nombreFirma = nombreFirma;
		//this.ventaDelProveedor = (valorAlbaran("tipoventa").equals("D") ? true : false);
		//No se utiliza. Utilizamos tipoAlbaran para conseguir albaranValorado
		//this.esMoroso = (valorAlbaran("ismoroso").equals("Y") ? true : false);	
		this.pagoContado = (valorAlbaran("c_invoiceschedule_id").equals("1000000") ? true : false);
		if (valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_AC)){
			this.tipoAlbaran = "AC";
			imagenesEncabezado = "020" + "800" + "237" + "LOGOH.jpg";
			
					
					//"\u001bEZ{PRINT:@1,1:ocgases_logo2,HMULT1,VMULT1|}\n{LP}\n";
		}
		if (valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CR)){
			this.tipoAlbaran = "CR";
			imagenesEncabezado = "020" + "800" + "237" + "LOGO2.jpg";
		}
		if (valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CR2)){
			this.tipoAlbaran = "CR2";
			imagenesEncabezado = "020" + "800" + "237" + "LOGO2.jpg";
		}
		if (valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CF)){
			this.tipoAlbaran = "CF";
			imagenesEncabezado = "020" + "800" + "237" + "gasServei.jpg";
		}
		this.albaranValorado = (tipoAlbaran.equals("AC") || (tipoAlbaran.equals("CR2") & pagoContado));
		this.linesReport = CreaReportLines();
	} 
	
	private List<String[]> CreaReportLines() {
		List<String[]> listString = new ArrayList<String[]>();
		int posicion = 1;
		listString.add(insertaFilaTabla(posicion, PrintFormats.IMAGE_FILE, imagenesEncabezado));
		posicion++;

		listString.add(insertaFilaTabla(posicion, PrintFormats.BOLD_DOUBLE_HIGH, 
				valorAlbaran("nombrecomercial")));
		posicion++;
		
		String datoAdicionalCabecera = valorAlbaran("nombrefiscal");
		if (!datoAdicionalCabecera.equals("")){
			listString.add(insertaFilaTabla(posicion, PrintFormats.BOLD_DOUBLE_HIGH, 
					datoAdicionalCabecera));
			posicion++;
		}
		String codigoEntidad = valorAlbaran("codigoalternativo");
		String codigoAL = valorAlbaran("codigoairliquid");
		String codigosCliente = "";
		if (valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CR) | 
				valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CR2)){
			codigosCliente = "Cod. Cliente: " + codigoAL +  " - " + codigoEntidad;
		} else {
			codigosCliente = "Cod. Cliente: " + codigoEntidad +  " - " + codigoAL;
		}
		listString.add(insertaFilaTabla(posicion,PrintFormats.NORMAL, valorAlbaran("cif")
				 + "  " + codigosCliente));
		posicion++;
		listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL,
				valorAlbaran("direccion") + " " + valorAlbaran("ciudad")));
		posicion++;
		listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, " "));
		posicion++;
		listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE, 
				"Albaran: " + valorAlbaran("documentno")
				 + " - Fecha: " + valorAlbaran("fecha").substring(8, 10) + "-" 
				                + valorAlbaran("fecha").substring(5, 7) + "-" 
				                + valorAlbaran("fecha").substring(0, 4) 
				 + " - Pedido: " +	valorAlbaran("n_pedido_cliente")));
		posicion++;


	    String[] datosEncabezado = {"Codigo", "Descipción", "Uds.", "Rcs.", 
	    		"Precio", "Dto.", "Importe"};
		String lineaCabecera = creaLinea(datosEncabezado, formatoLinea, alineacionLinea);
		listString.add(insertaFilaTabla(posicion, PrintFormats.UNDERLINE, lineaCabecera));
		posicion++;
		
	    listString.add(insertaFilaTabla(posicion, PrintFormats.LINE, ""));
	    posicion++;
	    
		MySQLiteHelper myHelper = (MySQLiteHelper) new MySQLiteHelper(context);
	    SQLiteDatabase db = myHelper.getReadableDatabase();

		String[] lineaServicioEntrega = null;
		double sumaAlbaran = 0;
		int sumaBotellas = 0;
		
//		Tablas CAF
//		m_product_adr eurokg
//		c_uom_conversion coefconversion

		boolean esClienteCaf = false;
		boolean addLineaJustificaCaf = false;
		if (!valorAlbaran("cdgcafentidad").equals("")){
			esClienteCaf = true;
		}
		String valueStringCaf = "CAF";
		String nameStringCaf = "Impuesto gases refrigerantes";
		String valueStringCafnoSuj = "CAFnoSujeto";
		String nameStringCafnoSuj =  "Art. 11 R.D. 1042/2013. EXENCIÓN para quines destinen "
				+ "los gases fluorados de efecto invernadero a su reventa en el ámbito territorial del "
				+ "impuesto y que hace referencia a la letra a) del nº1 del apartado siete del "
				+ "artículo 5 de la Ley 16/2013";
		for (String[] lineaAlbaran : listaLineasAlbaran) {
			if (valorLineaAlbaran("m_product_id", lineaAlbaran).equals("1003716")){
				valueStringCafnoSuj = valorLineaAlbaran("value", lineaAlbaran);
				String nameStringCafnoSujOrig = valorLineaAlbaran("name", lineaAlbaran);
				if (!nameStringCafnoSujOrig.replace(" ", "").equals("")){
					nameStringCafnoSuj = nameStringCafnoSujOrig;
				}
				break;
			}
		}
//		for (String[] lineaAlbaran : listaLineasAlbaran) {
//			if (valorLineaAlbaran("m_product_id", lineaAlbaran).equals("1003423")){
//				valueStringCaf = valorLineaAlbaran("value", lineaAlbaran);
//				nameStringCaf = valorLineaAlbaran("name", lineaAlbaran);
//			}
//			if (valorLineaAlbaran("m_product_id", lineaAlbaran).equals("1003716")){
//				esClienteCaf = true;
//			}
//		}
		//TODO CAF
		
		//m_product_id CAF 1003423
		//m_product_id 99 1000035
		
		for (String[] lineaAlbaran : listaLineasAlbaran) {
			double totalLinea = 0;
			int cantidadBotellas = 0;
			if (valorLineaAlbaran("m_product_id", lineaAlbaran).equals("1003423") | 
					valorLineaAlbaran("m_product_id", lineaAlbaran).equals("1003716")){
				continue;
			}
			if (!valorLineaAlbaran("m_product_id", lineaAlbaran).equals("1000035")){
				//totalLinea = Double.valueOf(valorLineaAlbaran("linenetamt", lineaAlbaran));
				DatosLinea datosLinea = imprimeLinea(lineaAlbaran, myHelper, db);
				String lineaString = datosLinea.getLineaImprimir();
				totalLinea = datosLinea.getTotalLinea();
				cantidadBotellas = datosLinea.getCantidadBotellas();
				if(!albaranValorado)	totalLinea = 0;
				sumaAlbaran = sumaAlbaran + totalLinea;
				if (cantidadBotellas > 0) {
					sumaBotellas = sumaBotellas + cantidadBotellas;
				}  else {
					sumaBotellas = sumaBotellas - cantidadBotellas;
				}
				listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, lineaString));
				posicion++;
				listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, " "));
				posicion++;
				if (esClienteCaf & !valorLineaAlbaran("eurokg", lineaAlbaran).equals("")){
					addLineaJustificaCaf = true;
				}
				if (!esClienteCaf & !valorLineaAlbaran("eurokg", lineaAlbaran).equals("")){
					double impuestoKg = Double.valueOf(valorLineaAlbaran("eurokg", lineaAlbaran));
					double coefConversion = Double.valueOf(valorLineaAlbaran("coefconversion", lineaAlbaran));
					String entregadosString = String.format("%d", cantidadBotellas * ((int) coefConversion));
					String eurosKgString = String.format( "%.2f", impuestoKg );
					totalLinea = impuestoKg * cantidadBotellas * coefConversion;
					if(!albaranValorado)	totalLinea = 0;
					sumaAlbaran = sumaAlbaran + totalLinea;
					String totalLineaString = String.format( "%.2f", totalLinea );
					String[] datosLinea1 = {valueStringCaf, nameStringCaf, entregadosString, "0",
							eurosKgString, "0", totalLineaString};
					
					lineaString = creaLinea(datosLinea1, formatoLinea, alineacionLinea);
					listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, lineaString));
					posicion++;
					listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, " "));
					posicion++;
				}
			} else {
				lineaServicioEntrega = lineaAlbaran;
			}
		}
		
		if (addLineaJustificaCaf){
			String[] datosLinea1 = {valueStringCafnoSuj, nameStringCafnoSuj, "", "", "", "", ""};
			
			String lineaString = creaLinea(datosLinea1, formatoLinea, alineacionLinea);
			listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, lineaString));
			posicion++;
			listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, " "));
			posicion++;
		}
		
		/**TODO CALCULO SERVICIO DE ENTREGA NITROGENO Y PORTES ACORDADOS*/
		/**TODO CALCULO CAF*/
		
		if (lineaServicioEntrega != null){
			double totalLinea = Double.valueOf(valorLineaAlbaran("linenetamt", lineaServicioEntrega));
			int indiceCantidad = indiceArray(cabecerasLinea, "qtyordered");
//			int indicePrecio= indiceArray(cabecerasLinea, "priceentered");
			int indicePrecio= indiceArray(cabecerasLinea, "pricelist");
			int indiceTotal= indiceArray(cabecerasLinea, "linenetamt");
			String reglaPorte = valorAlbaran("m_freightcostrule_id");
			double[] calculoPortes = myHelper.calculaPortes(db, reglaPorte, sumaBotellas);
			int sumaBotellasCorregida = (int) calculoPortes[0];
			double precioPorte = calculoPortes[1];
			Log.w("OCGASES", "Sumabotellas " + sumaBotellasCorregida);
			Log.w("OCGASES", "Precioporte " + precioPorte);
			totalLinea =  sumaBotellasCorregida * precioPorte;
			Log.w("OCGASES", "Totallinea " + totalLinea);
//			double totalLinea = ((double) sumaBotellas) * 
//				Double.valueOf(valorLineaAlbaran("priceentered", lineaServicioEntrega));
			if(!albaranValorado)	totalLinea = 0;
			sumaAlbaran = sumaAlbaran + totalLinea;
			lineaServicioEntrega[indiceCantidad] = String.valueOf(sumaBotellasCorregida);
			lineaServicioEntrega[indicePrecio] = String.valueOf(precioPorte);
			lineaServicioEntrega[indiceTotal] = String.valueOf(totalLinea);
			String lineaString = imprimeLinea(lineaServicioEntrega, myHelper, db).getLineaImprimir();
			listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL, lineaString));
			posicion++;
		}
		
		db.close();
	    myHelper.close();

	    
	    listString.add(insertaFilaTabla(posicion, PrintFormats.LINE, ""));
	    posicion++;
	    double sumaAlbaranConIva = 0;
	    if (valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CR2) | 
	    		valorAlbaran("c_doctype_id").equals(TIPO_ALBARAN_CR)){
			sumaAlbaranConIva = sumaAlbaran;
		} else {
			sumaAlbaranConIva = sumaAlbaran * (1 + VALOR_IVA);
		}
	    
	    if (albaranValorado) {
	    	listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL,
		    		"               TOTAL IMPORTE (CON IVA)................." + 
		    				String.format( "%.2f", sumaAlbaranConIva ) +  " EUROS"));
			posicion++;
	    }
	    if (tipoAlbaran.equals("AC")){
	    	listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE,
		    		"Forma Pago: " + valorAlbaran("formapago") + " " + valorAlbaran("vencimiento")));
	    }
	    if (tipoAlbaran.equals("CR")){
	    	listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE,
		    		"Condiciones Economicas en Fact. fin de mes de AIR LIQUIDE ESPAÑA, S.A."));
	    }
	    if (tipoAlbaran.equals("CR2")){
	    	listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE,
		    		"Condiciones Economicas en Fact. fin de mes de AIR LIQUIDE ESPAÑA, S.A. con "
		    		+ "'Cesion del credito' a OXIGENO CASTAÑO S.L."));
	    	listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE,
		    		"Forma Pago: " + valorAlbaran("formapago") + " " + valorAlbaran("vencimiento")));
	    }
	    if (tipoAlbaran.equals("CF")){
	    	listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE,
		    		"Condiciones Economicas en Fact. fin de mes de GAS SERVEI, S.A."));
	    }
	      
		posicion++;
		
	    listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL,
	    		"Revisado por: " + valorAlbaran("usuario")));
		posicion++;
		
	    listString.add(insertaFilaTabla(posicion, PrintFormats.IMAGE_ENCODED_BASE64, 
	    		valorAlbaran("documentno") + ".jpg"));
	    posicion++;
	    
//	    listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL,
//	    		"   " + nombreFirma));
//		posicion++;
		
	    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	    String currentDate = df.format(Calendar.getInstance().getTime());
	    listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL,
	    		currentDate + "    Recepcionado por:" + nombreFirma.toUpperCase(Locale.getDefault())));
		posicion++;
		String leyOPDText = "";
		 if (tipoAlbaran.equals("CF")){
		    	leyOPDText = PrintFormats.TEXTO_LOPD_LSSI_2;
		    } else {
		    	leyOPDText = PrintFormats.TEXTO_LOPD_LSSI_1;
		    }
	    listString.add(insertaFilaTabla(posicion, PrintFormats.TEXTO_LIBRE, leyOPDText));
	    
	    posicion++;
	    
	    listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL," "));
		posicion++;
		
		listString.add(insertaFilaTabla(posicion, PrintFormats.NORMAL," "));
		posicion++;
	    

	  
	
		return listString;
	}
	
	private DatosLinea imprimeLinea(String[] lineaAlbaran, MySQLiteHelper myHelper, SQLiteDatabase db ) {
		String m_inoutline_id = valorLineaAlbaran("m_inoutline_id", lineaAlbaran);
		String valueString = valorLineaAlbaran("value", lineaAlbaran);
		String nameString = valorLineaAlbaran("name", lineaAlbaran);
		String entregadosString = "";
		String devueltosString = "";
		String precioTarifaString = "";
		String descuentoString = valorLineaAlbaran("discount", lineaAlbaran);
		if (descuentoString.equals("")) descuentoString = "0";
		String totalLineaString = "";
		
		if (valorLineaAlbaran("isdescription", lineaAlbaran).equals("Y")){
			nameString = valorLineaAlbaran("descriptiounEnvasen", lineaAlbaran);
		}
		
		String attId = valorLineaAlbaran("m_attributeset_id", lineaAlbaran);
		String unEnvase = valorLineaAlbaran("unenvase", lineaAlbaran);
		String pideCodBarr = valorLineaAlbaran("pidecdgbarras", lineaAlbaran);
		boolean esRetornable = attId.equals("1000000");
		boolean esUnEnvase = unEnvase.equals("Y");
		boolean esTrazable = ((attId.equals("1000000") & pideCodBarr.equals("Y")) 
				| attId.equals("1000001") | attId.equals("1000002"));
		
		int cantidadEntregada = Double.valueOf(valorLineaAlbaran("qtyordered", lineaAlbaran)).intValue();
		int cantidadDevuelta = Double.valueOf(valorLineaAlbaran("qtyreturn", lineaAlbaran)).intValue();
		double precioTarifa = Double.valueOf(valorLineaAlbaran("pricelist", lineaAlbaran));
		double precioNeto = Double.valueOf(valorLineaAlbaran("priceentered", lineaAlbaran));
		double descuento = Double.valueOf(descuentoString);
		double totalLinea = Double.valueOf(valorLineaAlbaran("linenetamt", lineaAlbaran));
		int signoCantidadEntregada = (int) Math.signum((double) cantidadEntregada);
		if (signoCantidadEntregada == 0) {
			signoCantidadEntregada = 1;
		}
		int cantidadBotellas = 0;
		
		if (esRetornable) {
			cantidadEntregada =  myHelper.cuentaCodigosLlenos(db, m_inoutline_id, "venta");
			cantidadEntregada = cantidadEntregada * signoCantidadEntregada;
			cantidadDevuelta =  myHelper.cuentaCodigosVacios(db, m_inoutline_id, "venta");
			totalLinea = cantidadEntregada * precioNeto;
			cantidadBotellas = cantidadEntregada;
		}
		
		if (esUnEnvase){
			cantidadBotellas = 1;
		}
		
		if(!albaranValorado){
			precioTarifa = 0;
			totalLinea = 0;
		}

		entregadosString = String.format("%d", cantidadEntregada);
		devueltosString = String.format("%d", cantidadDevuelta);
		precioTarifaString = String.format( "%.2f", precioTarifa );
		totalLineaString = String.format( "%.2f", totalLinea );
		descuentoString = String.format( "%.2f", descuento );
		
		String[] datosLinea = {valueString, nameString, entregadosString, devueltosString,
				precioTarifaString, descuentoString, totalLineaString};
		
		String lineaString = creaLinea(datosLinea, formatoLinea, alineacionLinea);
		String codigosLlenosString = myHelper.getStringCodigosLlenosPost(db, m_inoutline_id, "venta");
		String codigosVaciosString = myHelper.getStringCodigosVaciosPost(db, m_inoutline_id, "venta");
		if (esTrazable) {
			if (!codigosLlenosString.equals("")){
				lineaString = lineaString + "\n          Entregados: " + codigosLlenosString;
			}
			if (!codigosVaciosString.equals("")){
				lineaString = lineaString + "\n          Recibidos: " + codigosVaciosString;
			}
		}
		return new DatosLinea(lineaString, totalLinea, cantidadBotellas);
	}

	private String creaLinea(String[] datosLinea, int[] formatoLinea, String[] alineacionLinea) {
		StringBuilder lineaFormateada = new StringBuilder();
		String descripcionSinRets = datosLinea[1].replace("\n", " ");
		String[] descripcionEnLineas = 
				PrintFormats.formateaAnchoString(descripcionSinRets, formatoLinea[1]).split("\n");
		for (int i = 0; i < datosLinea.length; i++) {
			String dato = datosLinea[i];
			int espacio = formatoLinea[i];
			if (dato.length() > espacio){
				dato = dato.substring(0,espacio);
			}
			String datoFormateado = "";
			if (alineacionLinea[i].equals("D")){
				datoFormateado = String.format("%1$" + espacio + "s", dato);
			} else {
				datoFormateado = String.format("%1$-" + espacio + "s", dato);
			}
			lineaFormateada.append(datoFormateado);
		}
		if (descripcionEnLineas.length > 0){
			for (int i = 1; i < descripcionEnLineas.length; i++) {
				int espacios = formatoLinea[0];
				String estaLinea = 
						"\n" + String.format("%1$-" + espacios + "s", "") + descripcionEnLineas[i];
				lineaFormateada.append(estaLinea);
			}
		}
		
		//lineaFormateada.append("\n");
		return lineaFormateada.toString();
	}

	private String valorAlbaran(String nombreCabecera) {
		return valoresCabeceraAlbaran[indiceArray(datosCabeceraAlbaran, nombreCabecera)];
	}
	
	private String valorLineaAlbaran(String nombreCabecera, String[] linea) {
		return linea[indiceArray(cabecerasLinea, nombreCabecera)];
	}


	private String[] insertaFilaTabla(int numeroFila, String formato, String valor) {
		String numFilaString = String.valueOf(numeroFila);
		String[] stringFinal = {numFilaString, formato, valor};
		return stringFinal;
	}
	
	
	private int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}
	
	public List<String[]> getLinesReport(){
		return linesReport;
	}
	
	private class DatosLinea {
		private String lineaImprimir;
		private Double totalLinea;
		private int cantidadBotellas;
		public DatosLinea(String lineaImprimir, Double totalLinea, int cantidadBotellas) {
			super();
			this.lineaImprimir = lineaImprimir;
			this.totalLinea = totalLinea;
			this.cantidadBotellas = cantidadBotellas;
		}
		public String getLineaImprimir() {
			return lineaImprimir;
		}
		public void setLineaImprimir(String lineaImprimir) {
			this.lineaImprimir = lineaImprimir;
		}
		public Double getTotalLinea() {
			return totalLinea;
		}
		public void setTotalLinea(Double totalLinea) {
			this.totalLinea = totalLinea;
		}
		public int getCantidadBotellas() {
			return cantidadBotellas;
		}
		public void setCantidadBotellas(int cantidadBotellas) {
			this.cantidadBotellas = cantidadBotellas;
		}
		
	}
	
	private String[] recortaTexto( String texto, int ancho){
		int numLineas = (texto.length()/ancho);
		int charsUltimaLinea = (texto.length()%ancho);
		int lineasTotales = numLineas;
		if (texto.length() <= ancho){
			String[] textoRecortado = {texto};
			return textoRecortado;
		} else{
			if (charsUltimaLinea > 0){
				lineasTotales++;
			}
			String[] textoRecortado = new String[lineasTotales];
			for (int i = 0; i < numLineas; i++) {
				String linea = texto.substring(i*ancho, (i+1)*ancho);
				textoRecortado[i] = linea;
			}
			if (charsUltimaLinea > 0){
				textoRecortado[lineasTotales-1] = texto.substring(numLineas*ancho);
			}
			return textoRecortado;
		}
	}

}
