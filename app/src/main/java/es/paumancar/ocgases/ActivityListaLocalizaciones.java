package es.paumancar.ocgases;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import es.paumancar.ocgases.R;
import es.paumancar.ocgases.datasync.Connectivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class ActivityListaLocalizaciones extends Activity {

	Spinner spinnerZonas;
	ListView listviewLocalizaciones;

	ProgressDialog dialogProgreso;
	List<String> listaLocalizaciones;

	String[] zonas = {
			"F10",	"L3",	"L2",
	"a09",	"a08",	"a07",	"a06",
	"a05",	"a04",	"a03",	"a02",
	"a13",	"a12",	"a11",	"a10",
	"a01",	"a00",	"L1",	"J",
	"I",	"H",	"G05",	"G04",
	"G03",	"G02",	"G01",	"G00",
	"F09",	"F08",	"F07",	"F06",
	"F05",	"F04",	"F03",	"F02",
	"F01",	"F00",	"E09",	"E08",
	"E07",	"E06",	"E05",	"E04",
	"E03",	"E02",	"E13",	"E12",
	"E11",	"E10",	"E01",	"E00",
	"D07",	"D06",	"D05",	"D04",
	"D03",	"D02",	"D01",	"D00",
	"C03",	"C02",	"C01",	"C00",
	"B09",	"B08",	"B07",	"B06",
	"B05",	"B04",	"B03",	"B02",
	"B01",	"B00",	"A08",	"A07",
	"A06",	"A05",	"A04",	"A03",
	"A02", "A01",	"A00"
	};
	
    	@Override
    	public void onCreate(Bundle savedInstanceState) {
    		requestWindowFeature(Window.FEATURE_NO_TITLE);
    		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
    		super.onCreate(savedInstanceState);
    		setContentView(R.layout.activity_lista_localizaciones);
        
    		spinnerZonas = (Spinner) findViewById(R.id.spinnerZonasLocalizaciones);
    		listviewLocalizaciones = (ListView) findViewById(R.id.listviewLocalizaciones);
    		
    	    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zonas);
    	    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	    spinnerZonas.setAdapter(dataAdapter);
    		
    		spinnerZonas.setOnItemSelectedListener(new OnItemSelectedListener() {

    			@Override
    			public void onItemSelected(AdapterView<?> parent, View view,
    					int position, long id) {
    				updateLocalizaciones(parent.getItemAtPosition(position).toString());
    				
    			}

				@Override
    			public void onNothingSelected(AdapterView<?> parent) {
    				// TODO Auto-generated method stub
    				
    			}
    		});
        
  
    }
    	

	private void updateLocalizaciones(String zonaString) {
			RecuperaLocalizaciones recuperaLocalizaciones = 
					new RecuperaLocalizaciones(getApplicationContext(), zonaString);
			recuperaLocalizaciones.execute("");
			
	}
	
	class RecuperaLocalizaciones extends
	AsyncTask<String, String, List<String[]>> {

	
		String direccionIP;
		Context context;
		String zonaString;
		
		public RecuperaLocalizaciones(Context context, String zonaString) {
			super();
			this.context = context;
			this.zonaString = zonaString;
		}
		
		@Override
		protected void onPreExecute() {
			dialogProgreso = ProgressDialog.show(ActivityListaLocalizaciones.this, "", 
		            "Descargando datos ...", true);
			dialogProgreso.setCancelable(true);
			super.onPreExecute();
		}
		
		@Override
		protected List<String[]> doInBackground(String... StringDatos) {
			List<String[]> resultadoCartas = null;
			List<String[]> resultadoAlbaranes = null;
			List<String[]>[] resultadoCartasAlbaranes = null;
			direccionIP = Connectivity.servidorActivo(getApplicationContext());

			String consultaSql = "select  "
					+ "c_bpartner.c_bpartner_id, "
					+ "c_bpartner.name as namepartner,  "
					+ "c_bpartner.c_locationzone_id, 	"
					+ "c_bpartner_location.name as namelocation,  "
					+ "c_bpartner.telefono_e_comerciales as telefonopartner, "
					+ "c_bpartner.email_e_comerciales as mailpartner, "
					+ "c_locationzone.value as valuezone, "
					+ "c_locationzone.name as namezone, "
					+ "c_bpartner_location.longitud, "
					+ "c_bpartner_location.latitud, "
					+ "c_bpartner_location.c_bpartner_location_id "
					+ "from c_bpartner  "
					+ "inner join c_locationzone "
					+ "on c_bpartner.c_locationzone_id = c_locationzone.c_locationzone_id "
					+ "inner join c_bpartner_location "
					+ "on c_bpartner.c_bpartner_id = c_bpartner_location.c_bpartner_id "
					+ "where c_locationzone.value = "
					+ "'" + zonaString + "'"
					+ " and iscustomer = 'Y' "
					+ "order by namepartner";
			
			
			InputStream in = new BufferedInputStream(null);
		
			HttpURLConnection conn = null;
			String direccionPost = direccionIP + "/rest/source/?action=select";
			 URL url = null;
			 try {
			 url = new URL(direccionPost);
			 } catch (MalformedURLException e1) {
			 // TODO Auto-generated catch block
			 e1.printStackTrace();
			 }

		    try {
		    	if (url != null) {
		    	   conn = (HttpURLConnection)url.openConnection();
		    	   conn.setReadTimeout(10000);
		    	   conn.setConnectTimeout(15000);
		    	   conn.setRequestMethod("POST");
		    	   conn.setDoInput(true);
		    	   conn.setDoOutput(true);

		    	   OutputStream os = conn.getOutputStream();
		    	   BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		           StringBuilder sb = new StringBuilder();
		           sb.append(URLEncoder.encode("sql", "UTF-8"));
		           sb.append("=");
		           sb.append(URLEncoder.encode(consultaSql, "UTF-8"));
		           writer.write(sb.toString());
		    	   writer.flush();
		    	   writer.close();
		    	   os.close();
		    	}

		        int statusCode = conn.getResponseCode();
		        if(statusCode == 200) {
		        	in = new BufferedInputStream(conn.getInputStream());
		        } else {
		        	InputStream is = conn.getErrorStream();
		        	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		        	StringBuilder sb = new StringBuilder();
	        	    String line = null;
	        	    try {
	        	        while ((line = reader.readLine()) != null) {
	        	            sb.append(line).append('\n');
	        	        }
	        	    } catch (IOException e) {
	        	        e.printStackTrace();
	        	    } finally {
	        	        try {
	        	            is.close();
	        	        } catch (IOException e) {
	        	            e.printStackTrace();
	        	        }
	        	    }
		        }
		    } catch (Exception e) {
				Log.e("OCGASES", e.getMessage());
			} 
			
			List<String[]> resultados = new ArrayList<String[]>();
			try {
				resultados = readJsonStream(in);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return resultados;
		}
		
		@Override
		protected void onPostExecute(List<String[]> result) {
			super.onPostExecute(result);
			dialogProgreso.cancel();
			List<String[]> listaResultados = result;
			//wheelCartasDePorte.setMyAdapter(result.getArrayCartas());
			if (result != null){
				AdapterLocalizaciones adapterLoc = 
						new AdapterLocalizaciones(getApplicationContext(), result);
				listviewLocalizaciones.setAdapter(adapterLoc);
			}
		}
		
	}
	
	List<String[]> readJsonStream(InputStream in) throws IOException {
		     JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		     try {
		       return readStringsArray(reader);
		     } finally {
		       reader.close();
		     }
		   }

	List<String[]> readStringsArray(JsonReader reader) throws IOException {
		     List<String[]> localizacionDatos = new ArrayList<String[]>();

		     reader.beginArray();
		     while (reader.hasNext()) {
		       localizacionDatos.add(readLocalizacionDatos(reader));
		     }
		     reader.endArray();
		     return localizacionDatos;
		   }

	String[] readLocalizacionDatos(JsonReader reader) throws IOException {
		     String partnerId = "";
		     String localId = "";
		     String nombrepartner = "";
		     String nombrelocalizacion = "";
		     String telefono = "";
		     String correo = "";
		     String valorzona = "";
		     String nombrezona = "";
		     String latitud = "";
		     String longitud = "";
		     String c_bpartner_location_id = "";

		     reader.beginObject();
		     while (reader.hasNext()) {
		       try {
				String name = reader.nextName();
				   if (name.equals("c_bpartner_id")) {
					   partnerId = reader.nextString();
				   } else if (name.equals("c_locationzone_id")) {	
					   localId = reader.nextString();
				   } else if (name.equals("namepartner")) {
					   nombrepartner = reader.nextString();
				   } else if (name.equals("namelocation")) {
					   nombrelocalizacion = reader.nextString();
				   } else if (name.equals("valuezone")) {
					   valorzona = reader.nextString();
				   } else if (name.equals("namezone")) {
					   nombrezona = reader.nextString();
				   } else if (name.equals("telefonopartner")) {
					   telefono = reader.nextString();
				   } else if (name.equals("mailpartner")) {
					   correo = reader.nextString();
				   } else if (name.equals("latitud")) {
					   latitud = reader.nextString();
				   } else if (name.equals("longitud")) {
					   longitud = reader.nextString();
				   } else if (name.equals("c_bpartner_location_id")) {
					   c_bpartner_location_id = reader.nextString();
				   }   else {
				     reader.skipValue();
				   }
		       } catch (Exception e) {
		    	   reader.skipValue();
		    	   e.printStackTrace();
		       }
		     }
		     reader.endObject();
		     String[] valoresArray = { partnerId, localId, nombrepartner, nombrelocalizacion, telefono, 
		    		 correo, valorzona, nombrezona, latitud, longitud, c_bpartner_location_id};
		     return valoresArray;
		   }
}
