package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class AdapterServidores extends BaseAdapter {

    Context context;
    List<String[]> listaServidores;
    String locales_remotos;
    private static LayoutInflater inflater = null;
    

    public AdapterServidores(Context context, List<String[]> listaServidores, String locales_remotos) {
        // TODO Auto-generated constructor stub
        this.context = context;
        if (listaServidores != null) {
        	this.listaServidores = listaServidores;
        } else {
        	this.listaServidores = new ArrayList<String[]>();
        }
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.locales_remotos = locales_remotos;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listaServidores.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listaServidores.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_servidores_row, null);
        RadioButton botonActivo = (RadioButton) vi.findViewById(R.id.radioButtonServidorActivo);
        TextView textDireccion = (TextView) vi.findViewById(R.id.rowTextServidorDireccion);
        TextView textPuerto = (TextView) vi.findViewById(R.id.rowTextServidorPuerto);
        Button botonDelete = (Button) vi.findViewById(R.id.rowButtonServidorDelete);
        Button botonEdit = (Button) vi.findViewById(R.id.rowButtonServidorEdit);
        TextView servidorSsl = (TextView) vi.findViewById(R.id.rowTextServidorSsl);
        
        servidorSsl.setText(listaServidores.get(position)[4].equals("0")?"---":"SSL");
        botonActivo.setChecked(listaServidores.get(position)[3].equals("0")?false:true);
        textDireccion.setText(listaServidores.get(position)[1]);
        textPuerto.setText(listaServidores.get(position)[2]);
		final int id_servidor = Integer.valueOf(listaServidores.get(position)[0]);
		final int activo = Integer.valueOf(listaServidores.get(position)[3]);
		botonActivo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean esActivo = ((RadioButton) v).isChecked();
				if (esActivo) {
					MySQLiteHelper myHelper = new MySQLiteHelper(context);
					SQLiteDatabase db = myHelper.getWritableDatabase();
					myHelper.desactivaServidores(db, locales_remotos);
					myHelper.activaServidor(db, id_servidor, locales_remotos);
					listaServidores = myHelper.recuperaServidores(db, locales_remotos);
					db.close();
					myHelper.close();
					actualizaDatos();
				}
			}
		});

		
		botonEdit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MySQLiteHelper myHelper = new MySQLiteHelper(context);
				SQLiteDatabase db = myHelper.getWritableDatabase();
				String[] lineaServidor = myHelper.recuperaServidorPorId(db, locales_remotos, id_servidor);
				db.close();
				myHelper.close();
//				
//				final EditText editTextDireccion = new EditText(context);
//				editTextDireccion.setText(lineaServidor[1]);
//				final EditText editTextPuerto = new EditText(context);
//				editTextPuerto.setText(lineaServidor[2]);
//				
//				LinearLayout layoutTextos = new LinearLayout(context);
//				layoutTextos.addView(editTextDireccion);
//				layoutTextos.addView(editTextPuerto);
				

				final TextView TextPuerto = new TextView(context);
				final TextView TextDireccion = new TextView(context);
				final TextView TextSsl = new  TextView(context);
				final EditText editTextPuerto = new EditText(context);
				final EditText editTextDireccion = new EditText(context);
				final CheckBox checkSsl = new CheckBox(context);
				editTextDireccion.setText(lineaServidor[1]);
				editTextPuerto.setText(lineaServidor[2]);
				checkSsl.setChecked(lineaServidor[4].equals("0")?false:true);
				TextPuerto.setText("Puerto:   ");
				TextDireccion.setText("Dirección: ");
				TextSsl.setText("SSL: ");
				editTextPuerto.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				editTextDireccion.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				
				LinearLayout layoutTextos = new LinearLayout(context);
				LinearLayout.LayoutParams layoutForInner = 
						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
								LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutTextos.setLayoutParams(layoutForInner);
				layoutTextos.setOrientation(LinearLayout.VERTICAL);
				
				LayoutParams params3Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3f);
				LayoutParams params7Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 7f);
				LayoutParams params5Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 5f);
				
				
				LinearLayout layoutPuerto = new LinearLayout(context);
				layoutPuerto.setLayoutParams(layoutForInner);
				layoutPuerto.setOrientation(LinearLayout.HORIZONTAL);
				TextPuerto.setLayoutParams(params3Weight);
				editTextPuerto.setLayoutParams(params7Weight);
				layoutPuerto.addView(TextPuerto);
				layoutPuerto.addView(editTextPuerto);
				
				LinearLayout layoutDireccion = new LinearLayout(context);
				layoutDireccion.setLayoutParams(layoutForInner);
				layoutDireccion.setOrientation(LinearLayout.HORIZONTAL);
				TextDireccion.setLayoutParams(params3Weight);
				editTextDireccion.setLayoutParams(params7Weight);
				layoutDireccion.addView(TextDireccion);
				layoutDireccion.addView(editTextDireccion);
				
				LinearLayout layoutSsl = new LinearLayout(context);
				layoutSsl.setLayoutParams(layoutForInner);
				layoutSsl.setOrientation(LinearLayout.HORIZONTAL);
				TextSsl.setLayoutParams(params3Weight);
				checkSsl.setLayoutParams(params7Weight);
				layoutSsl.addView(TextSsl);
				layoutSsl.addView(checkSsl);
				
				layoutTextos.addView(layoutDireccion);
				layoutTextos.addView(layoutPuerto);
				layoutTextos.addView(layoutSsl);
	
				AlertDialog.Builder dialog = new AlertDialog.Builder(context);
				
				dialog.setTitle("EDITA SERVIDOR");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	String textoDireccion = editTextDireccion.getText().toString(); 
				    	String textoPuerto = editTextPuerto.getText().toString(); 
				    	int esSsl = checkSsl.isChecked()?1:0;
				    	MySQLiteHelper myHelper = new MySQLiteHelper(context);
						SQLiteDatabase db = myHelper.getWritableDatabase();
						myHelper.actualizaServidor(db, id_servidor, locales_remotos, 
								textoDireccion, textoPuerto, activo, esSsl);
						listaServidores = myHelper.recuperaServidores(db, locales_remotos);
						db.close();
						myHelper.close();
						dialog.cancel();
						actualizaDatos();
				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
				
			}
		});
		
		
        botonDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MySQLiteHelper myHelper = new MySQLiteHelper(context);
				SQLiteDatabase db = myHelper.getWritableDatabase();
				myHelper.borraServidor(db, id_servidor, locales_remotos);
				listaServidores = myHelper.recuperaServidores(db, locales_remotos);
			    db.close();
			    myHelper.close();
				actualizaDatos();
			}
		});
        return vi;
    }
    
    private void actualizaDatos(){
    	this.notifyDataSetChanged();
    }
    

}
