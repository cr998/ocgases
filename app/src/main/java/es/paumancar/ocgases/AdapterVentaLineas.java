package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterVentaLineas  extends BaseAdapter {
	
	

	int posicionActiva = -1;
    Context context;
    Class<?> NextClass;
    int filtroId;
    int nextFiltroId;
    List<String[]> data;
    List<String[]> dataCompleta;
    private static LayoutInflater inflater = null;
    int posPideCDGBarras = 6; // MODIFICAR SI SE CAMBIA ARRAY
    int posLlenas = 7; // MODIFICAR SI SE CAMBIA ARRAY
    int posVacias = 8; // MODIFICAR SI SE CAMBIA ARRAY
    int pos_m_inoutline_id = 1; // MODIFICAR SI SE CAMBIA ARRAY
    int pos_m_attributeset_id;
    String[] cabecerasColumnas;
    TextView textoDescripicion;
    String documento;
    
//    String[] etiquetasDatos = {
//		  "m_inout_id",
//		  "m_inoutline_id",
//		  "m_product_id",
//		  "qtyordered",
//		  "qtyreturn",
//		  "priceentered",
//		  "pricelist",
//		  "discount",
//		  "linenetamt",
//		  "name",
//		  "m_attributeset_id",
//		  "pidecdgbarras",
//		  "llenas",
//		  "vacias",
//		  "value"
//    };
    
    public AdapterVentaLineas(Context context, List<String[]> data, String[] cabecerasColumnas,
    		Class<?> NextClass, int filtroId, int nextFiltroId, TextView textoDescipcion, 
    		String documento) {
        this.context = context;
        this.NextClass = NextClass;
        this.filtroId = filtroId;
        this.nextFiltroId = nextFiltroId;
        this.pos_m_attributeset_id = indiceArray(cabecerasColumnas, "m_attributeset_id");
        this.posPideCDGBarras = indiceArray(cabecerasColumnas, "pidecdgbarras");
        this.posLlenas = indiceArray(cabecerasColumnas, "llenas");
        this.posVacias = indiceArray(cabecerasColumnas, "vacias");
        this.pos_m_inoutline_id = indiceArray(cabecerasColumnas, "m_inoutline_id");
        this.cabecerasColumnas = cabecerasColumnas;
        this.textoDescripicion = textoDescipcion;
        this.documento = documento;
        if (data != null) {
        	this.dataCompleta = data;
        } else {
        	this.dataCompleta = new ArrayList<String[]>();
        }
        String columnaFiltro = "m_product_id"; //"value";
		String[] valoresAEliminar = {"1000035", "1003423", "1003716"};  //{"99"};
		this.data = filtraResultado(dataCompleta, columnaFiltro, valoresAEliminar);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public String[] getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_ventalineas, null);
        //TextView textHeader = (TextView) vi.findViewById(R.id.rowTextHeaderVentaLineas);
        TextView textDetailUdsOrd = (TextView) vi.findViewById(R.id.rowTextUdsOrdVentas); //Llenas
        TextView textDetailUdsOrdCont = (TextView) vi.findViewById(R.id.rowTextUdsOrdContVentas); //Llenas
        TextView textDetailUdsRet = (TextView) vi.findViewById(R.id.rowTextUdsRetVentas); //Vacías
        TextView textDetailUdsRetCont = (TextView) vi.findViewById(R.id.rowTextUdsRetContVentas); //Vacías
        TextView textDetailName = (TextView) vi.findViewById(R.id.rowTextCodigoVentas);
        //TextView textDetailDescripcion = (TextView) vi.findViewById(R.id.rowTextDescripcionVentas);
        int codigosLlenosCont = 0;
        int codigosVaciosCont = 0;
        final int positionAux = position;
        
        if (positionAux == posicionActiva) {
        	vi.setBackgroundColor(Color.CYAN); 
        } else {
        	vi.setBackgroundColor(Color.TRANSPARENT);
        }
        
        boolean esRetornable =
        		(data.get(position)[indiceArray(cabecerasColumnas, "m_attributeset_id")].equals("1000000")? true:false);
 
        
        //if (esRetornable){
        if (true){
        	String m_inoutline_id = data.get(positionAux)[pos_m_inoutline_id];
            MySQLiteHelper myHelper = new MySQLiteHelper(context);
            SQLiteDatabase db = myHelper.getWritableDatabase();
            List<String> listaCodigosLlenos = new ArrayList<String>();
            List<String> listaCodigosVacios = new ArrayList<String>();
            boolean existeLineaActiva = myHelper.existeLineaAlbaranActiva(db, m_inoutline_id);
            if (!existeLineaActiva){
            	listaCodigosLlenos = Arrays.asList(data.get(positionAux)[posLlenas].split(","));
                listaCodigosVacios = Arrays.asList(data.get(positionAux)[posVacias].split(","));
                myHelper.insertaLineaAlbaranActiva(db, m_inoutline_id);
            }
            List<String[]> listaCodigosLlenosArray = new ArrayList<String[]>();
            List<String[]> listaCodigosVaciosArray = new ArrayList<String[]>();
            for (String codigoLleno : listaCodigosLlenos) {
				String[] linAux = { "1", codigoLleno };
				listaCodigosLlenosArray.add(linAux);
			}
            for (String codigoVacio : listaCodigosVacios) {
				String[] linAux = { "1", codigoVacio };
				listaCodigosVaciosArray.add(linAux);
			}
            
           
            codigosLlenosCont = 
            		myHelper.actualizaCodigosLlenos(db, m_inoutline_id, documento, 
            				data.get(positionAux)[indiceArray(cabecerasColumnas, "name")],
            				listaCodigosLlenosArray, "venta");
            codigosVaciosCont = 
            		myHelper.actualizaCodigosVacios(db, m_inoutline_id, documento, 
            				data.get(positionAux)[indiceArray(cabecerasColumnas, "name")],
            				listaCodigosVaciosArray, "venta");
            db.close();
            myHelper.close();
            
            
            //TODO Revisar
            data.get(positionAux)[posLlenas] = "";
            data.get(positionAux)[posVacias] = "";
            
	        vi.setOnClickListener(new OnClickListener() {
	        	String[] cabeceraProv = data.get(positionAux);
	        	int sigFiltroId = nextFiltroId;
	        	String valorFiltro =(sigFiltroId < 0) ? "" : cabeceraProv[sigFiltroId];
	        	Class<?> sigClass = NextClass;
				@Override
				public void onClick(View v) {
					Log.w("OCGASES", "Pos actual " + String.valueOf(positionAux));
					Log.w("OCGASES", "Pos activa " + String.valueOf(posicionActiva));
					v.setBackgroundColor(Color.CYAN);
					if (positionAux == posicionActiva){
						Intent intentNext = new Intent(context.getApplicationContext(), sigClass);
						intentNext.putExtra("valoresCabecera", cabeceraProv);
						intentNext.putExtra("valorFiltro", valorFiltro);
						intentNext.putExtra("compra_venta", "venta");
						intentNext.putExtra("valorDocumento", documento);
						intentNext.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.getApplicationContext().startActivity(intentNext);
					}  else {
						posicionActiva = positionAux;
						String name = data.get(positionAux)[indiceArray(cabecerasColumnas, "name")];
						if (data.get(positionAux)[indiceArray(cabecerasColumnas, "isdescription")].equals("Y")){
							name = data.get(positionAux)[indiceArray(cabecerasColumnas, "description")];
						}
						textoDescripicion.setText(name);
						notifyDataSetChanged();
					}
				}
			});
	        textDetailUdsOrdCont.setText(String.format( "%.0f", (double) codigosLlenosCont));
	        String unidsRet = data.get(position)[indiceArray(cabecerasColumnas, "qtyreturn")];//.split(".");
	        if (esRetornable){
	        	textDetailUdsRet.setText(String.format( "%.0f", Double.valueOf(unidsRet)));
	        	textDetailUdsRetCont.setText(String.format( "%.0f", (double) codigosVaciosCont));  
	        }
    	} else {
    		vi.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.w("OCGASES", "Pos actual " + String.valueOf(positionAux));
					Log.w("OCGASES", "Pos activa " + String.valueOf(posicionActiva));
					posicionActiva = positionAux;
					textoDescripicion.setText(data.get(positionAux)[indiceArray(cabecerasColumnas, "name")]);
					notifyDataSetChanged();
				}
			});
    	}
        
        //textHeader.setText(data.get(position)[indiceArray(cabecerasColumnas, "m_product_id")]);
        String unidsOrd = data.get(position)[indiceArray(cabecerasColumnas, "qtyordered")];//.split(".");
        textDetailUdsOrd.setText(String.format( "%.0f", Double.valueOf(unidsOrd)));
        textDetailName.setText(data.get(position)[indiceArray(cabecerasColumnas, "value")]);
        //textDetailDescripcion.setText(data.get(position)[indiceArray(cabecerasColumnas, "name")]);
        //textHeader.setTextColor(Color.BLACK);
        textDetailUdsOrd.setTextColor(Color.BLACK);
        textDetailUdsOrdCont.setTextColor(Color.BLACK);
        textDetailUdsRet.setTextColor(Color.BLACK);
        textDetailUdsRetCont.setTextColor(Color.BLACK);
        textDetailName.setTextColor(Color.BLACK);
        //textDetailDescripcion.setTextColor(Color.BLACK);
        return vi;
    }

	public List<String[]> getData() {
		return dataCompleta;
	}
	
	
	public int getPosicionActiva() {
		return posicionActiva;
	}

	public void setPosicionActiva(int posicionActiva) {
		this.posicionActiva = posicionActiva;
	}

	protected int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}
	
	private List<String[]> filtraResultado(List<String[]> resultadoBruto,
			String columnaFiltro, String[] valoresAEliminar) {
		List<String[]> listaFiltrada = new ArrayList<String[]>();
		int indiceColumnaFiltro = indiceArray(cabecerasColumnas, columnaFiltro);
		for (String[] stringResultado : resultadoBruto) {
			boolean encontrado = false;
			for (String stringEliminar : valoresAEliminar) {
				if (stringResultado[indiceColumnaFiltro].equals(stringEliminar)){
					encontrado = true;
					break;
				}
			}
			if (!encontrado){
				listaFiltrada.add(stringResultado);
			}
		}
		return listaFiltrada;
	}

    
}