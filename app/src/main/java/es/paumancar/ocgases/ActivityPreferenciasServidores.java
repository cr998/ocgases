package es.paumancar.ocgases;

import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;




@SuppressLint("NewApi")
public class ActivityPreferenciasServidores extends FragmentActivity {
	
	
	SectionsPagerAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;
	
	List<String> codigosLlenos;
	List<String> codigosVacios;
	String m_inoutline_id;
	String compra_venta;

	   @Override
	    public void onCreate(Bundle savedInstanceState) {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.preferencias_servidores);
	     
	        

			// Create the adapter that will return a fragment for each of the three
			// primary sections of the activity.
			mSectionsPagerAdapter = new SectionsPagerAdapter(
					getSupportFragmentManager());

			// Set up the ViewPager with the sections adapter.
			mViewPager = (ViewPager) findViewById(R.id.pagerServidores);
			mViewPager.setAdapter(mSectionsPagerAdapter);
			

	    }
	
	
	
	
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			  switch (position){
			    case 0:
			    FragmentServidores FragmentLocales = 
			    	new FragmentServidores("locales");
			    return FragmentLocales;
			    
			    case 1:
			    	FragmentServidores FragmentRemotos = 
			    	new FragmentServidores("remotos");
			    return FragmentRemotos;

		    default:
		    //this page does not exists
		    return null;
		    }
			//return PlaceholderFragment.newInstance(position + 1);
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			}
			return null;
		}
	}


}
