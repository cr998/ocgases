package es.paumancar.ocgases;

import java.util.List;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentServidores extends Fragment {

		
	
	private List<String[]> listServidores;
	private MyListView listviewServidores;
	private String locales_remotos;
	
	private TextView textoTab;

	
	public FragmentServidores(String locales_remotos) {
		super();
		this.locales_remotos = locales_remotos;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {


	    View rootView = inflater.inflate(R.layout.tab_servidores, container,
	            false);
	   
	    return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);   
        MySQLiteHelper myHelper = new MySQLiteHelper(getContext());
		SQLiteDatabase db = myHelper.getWritableDatabase();
        listServidores = myHelper.recuperaServidores(db, locales_remotos);
        db.close();
        myHelper.close();

		listviewServidores = (MyListView) view.findViewById(R.id.listviewServidores);
        listviewServidores.setAdapter(new AdapterServidores(getContext(), listServidores, locales_remotos));
        textoTab = (TextView) view.findViewById(R.id.textoTituloTabPreferenciasServidores);
        Button botonAddServidor = (Button) view.findViewById(R.id.buttonTabServidoresNuevo);
        
        setTitulo(locales_remotos);
        
        botonAddServidor.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				final EditText editTextDireccion = new EditText(getContext());
//				final EditText editTextPuerto = new EditText(getContext());
//				LinearLayout layoutTextos = new LinearLayout(getContext());
//				layoutTextos.addView(editTextDireccion);
//				layoutTextos.addView(editTextPuerto);
//				LinearLayout.LayoutParams layoutForInner = 
//						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
//								LinearLayout.LayoutParams.WRAP_CONTENT);
//				layoutTextos.setLayoutParams(layoutForInner);
//				LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
//				editTextPuerto.setLayoutParams(params);
				
				final TextView TextPuerto = new TextView(getContext());
				final TextView TextDireccion = new TextView(getContext());
				final TextView TextSsl = new  TextView(getContext());
				final EditText editTextPuerto = new EditText(getContext());
				final EditText editTextDireccion = new EditText(getContext());
				final CheckBox checkSsl = new CheckBox(getContext());
				TextPuerto.setText("Puerto:   ");
				TextDireccion.setText("Dirección: ");
				TextSsl.setText("SSL: ");
				checkSsl.setChecked(false);
				editTextPuerto.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				editTextDireccion.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				
				LinearLayout layoutTextos = new LinearLayout(getContext());
				LinearLayout.LayoutParams layoutForInner = 
						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
								LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutTextos.setLayoutParams(layoutForInner);
				layoutTextos.setOrientation(LinearLayout.VERTICAL);
				
				LayoutParams params3Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3f);
				LayoutParams params7Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 7f);
				
				
				LinearLayout layoutPuerto = new LinearLayout(getContext());
				layoutPuerto.setLayoutParams(layoutForInner);
				layoutPuerto.setOrientation(LinearLayout.HORIZONTAL);
				TextPuerto.setLayoutParams(params3Weight);
				editTextPuerto.setLayoutParams(params7Weight);
				layoutPuerto.addView(TextPuerto);
				layoutPuerto.addView(editTextPuerto);
				
				LinearLayout layoutDireccion = new LinearLayout(getContext());
				layoutDireccion.setLayoutParams(layoutForInner);
				layoutDireccion.setOrientation(LinearLayout.HORIZONTAL);
				TextDireccion.setLayoutParams(params3Weight);
				editTextDireccion.setLayoutParams(params7Weight);
				layoutDireccion.addView(TextDireccion);
				layoutDireccion.addView(editTextDireccion);
				
				LinearLayout layoutSsl = new LinearLayout(getContext());
				layoutSsl.setLayoutParams(layoutForInner);
				layoutSsl.setOrientation(LinearLayout.HORIZONTAL);
				TextSsl.setLayoutParams(params3Weight);
				checkSsl.setLayoutParams(params7Weight);
				layoutSsl.addView(TextSsl);
				layoutSsl.addView(checkSsl);
				
				layoutTextos.addView(layoutDireccion);
				layoutTextos.addView(layoutPuerto);
				layoutTextos.addView(layoutSsl);

				AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
				
				dialog.setTitle("NUEVO SERVIDOR");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	String textoDireccion = editTextDireccion.getText().toString(); 
				    	String textoPuerto = editTextPuerto.getText().toString();
				    	int esSsl = checkSsl.isChecked()?1:0;
				    	MySQLiteHelper myHelper = new MySQLiteHelper(getContext());
						SQLiteDatabase db = myHelper.getWritableDatabase();
						myHelper.insertaServidor(db, locales_remotos, textoDireccion, textoPuerto, esSsl);
						listServidores = myHelper.recuperaServidores(db, locales_remotos);
						db.close();
						myHelper.close();
				        listviewServidores.setAdapter(new AdapterServidores(getContext(), listServidores, locales_remotos));
						//listviewServidores.getAdapter().notifyAll();
						dialog.cancel();

				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
			}
		});
        
	}
	

	
	public void setTitulo(String tituloTab){
		textoTab.setText(tituloTab.toUpperCase());
	}
	

	
	
}
