package es.paumancar.ocgases;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import es.paumancar.ocgases.datasync.MySQLiteHelperSync;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;



public class MySQLiteHelper extends SQLiteOpenHelper {
	
	private Context context;
	
	private static final String DATABASE_NAME = "ocgases.db";
	private static final int DATABASE_VERSION = 3;
	
//	private static final boolean[] TABLAS_A_ACTUALIZAR = {
//		false, false, false, false, false, false, false, true, true, false, false
//	};
	//Esta es para actualizar de la version 1 a la 2
	//Solo se actualizan las tablas de servidores

	private static final boolean[] TABLAS_A_ACTUALIZAR = {
		false, false, false, false, false, false, false, false, false, false, false, true, true
	};
	//Esta es para actualizar de la version 2 a la 3
	//Solo se crean las dos últimas de portes
	
    private final static int CODIGOS_LLENOS = 0;
    private final static int CODIGOS_VACIOS = 1;
	
	private static final String[] INCIDENCIAS = {
			"01.-No sale gas",
			"02.-Vacía",
			"03.-Fuga",
			"04.-Acetona",
			"05.-Placa defectuosa",
			"06.-Racor salida defectuoso",
			"07.-Regulación caudal",
			"08.-Manómetro defectuoso",
			"09.-Mezcla incorrecta",
			"10.-Rotura sifón",
			"11.-Presión salida insuficiente",
			"15.-Hace ruido al salir el",
			"20.-Observaciones"
		};
	private static final String[] LETRAS = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
		
	private static final String CREATE_TABLE_CODIGOS_VACIOS_VENTA = "create table " +
			"table_codigos_vacios_venta (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null, " +
			"documento text, " +
			"descripcion text, " +
			"codigo_vacio text not null, " +
			"incidencia integer default 0, " +
			"cantidad integer default 1, "
			+ "enviado string default '0', "
			+ " unique(m_inoutline_id, codigo_vacio, enviado)"
			+ ");";
	
	private static final String CREATE_TABLE_CODIGOS_LLENOS_VENTA = "create table " +
			"table_codigos_llenos_venta (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null, " +
			"documento text, " +
			"descripcion text, " +
			"codigo_lleno text not null, " +
			"incidencia integer default 0, " +
			"cantidad integer default 1, "
			+ "enviado string default '0', "
			+ " unique(m_inoutline_id, codigo_lleno, enviado)"
			+ ");";
	
	private static final String CREATE_TABLE_CODIGOS_VACIOS_COMPRA = "create table " +
			"table_codigos_vacios_compra (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null, " +
			"documento text, " +
			"descripcion text, " +
			"codigo_vacio text not null, " +
			"incidencia integer default 0, " +
			"cantidad integer default 1, "
			+ "enviado string default '0', "
			+ " unique(m_inoutline_id, codigo_vacio, enviado)"
			+ ");";
	
	private static final String CREATE_TABLE_CODIGOS_LLENOS_COMPRA = "create table " +
			"table_codigos_llenos_compra (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null, " +
			"documento text, " +
			"descripcion text, " +
			"codigo_lleno text not null, " +
			"incidencia integer default 0, " +
			"cantidad integer default 1, "
			+ "enviado string default '0', "
			+ " unique(m_inoutline_id, codigo_lleno, enviado)"
			+ ");";
	
	private static final String CREATE_TABLE_CODIGOS_VACIOS_PROVISIONAL = "create table " +
			"table_codigos_vacios_provisional (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null, " +
			"documento text, " +
			"descripcion text, " +
			"codigo_vacio text not null, " +
			"incidencia integer default 0, " +
			"cantidad integer default 1, "
			+ "enviado string default '0', "
			+ " unique(m_inoutline_id, codigo_vacio, enviado)"
			+ ");";
	
	private static final String CREATE_TABLE_CODIGOS_LLENOS_PROVISIONAL = "create table " +
			"table_codigos_llenos_provisional (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null, " +
			"documento text, " +
			"descripcion text, " +
			"codigo_lleno text not null, " +
			"incidencia integer default 0, " +
			"cantidad integer default 1, "
			+ "enviado string default '0', "
			+ " unique(m_inoutline_id, codigo_lleno, enviado)"
			+ ");";
	
	private static final String CREATE_TABLE_LINEAS_ALBARAN_ACTIVA = "create table " +
			"table_lineas_albaran_activas (  _id integer primary key autoincrement, " +
			"m_inoutline_id text not null);";
	
	private static final String CREATE_TABLE_SERVIDORES_LOCALES = "create table " +
			"table_servidores_locales (  _id integer primary key autoincrement, " +
			"direccion_servidor text not null, " +
			"puerto_servidor text, " +
			"activo integer default 0," +
			"ssl integer default 0 "
			+ ");";
	
	private static final String CREATE_TABLE_SERVIDORES_REMOTOS = "create table " +
			"table_servidores_remotos (  _id integer primary key autoincrement, " +
			"direccion_servidor text not null, " +
			"puerto_servidor text, " +
			"activo integer default 0, " +
			"ssl integer default 0 "
			+ ");";
	
	private static final String CREATE_TABLE_PASSWORD = "create table " +
			"table_passwords (  _id integer primary key autoincrement, " +
			"password text not null "
			+ ");";
	
	
	private static final String CREATE_TABLE_IMPRESORAS = "create table " +
			"table_impresoras (  _id integer primary key autoincrement, " +
			"etiqueta_impresora text not null, " +
			"nombre_impresora text not null, " +
			"direccion_impresora text not null, " +
			"activa integer default 0 "
			+ ");";
	
	private static final String CREATE_TABLE_PORTES = "create table " +
			"table_portes (  _id integer primary key autoincrement, " +
			"m_freightcostrule_id text not null, " +
			"ad_client_id text, " +
			"ad_org_id text, " +
			"isactive text, " +
			"created text, " +
			"createdby text, " +
			"updated text, " +
			"updatedby text, " +
			"description text, " +
			"value text, " +
			"name text " +
			");";
	
	private static final String CREATE_TABLE_REGLAS_PORTES = "create table " +
			"table_reglas_portes (  _id integer primary key autoincrement, " +
			"m_freightcostruleline_id text not null, " +
			"ad_client_id text, " +
			"ad_org_id text, " +
			"isactive text, " +
			"created text, " +
			"createdby text, " +
			"updated text, " +
			"updatedby text, " +
			"description text, " +
			"fromnumber text, " +
			"tonumber text, " +
			"isbottlecontrolled text, " +
			"isoneinstance text, " +
			"freightamt text, " +
			"m_freightcostrule_id text not null " +
			");";
	


	
	private static final String[] CONSULTA_CREAR_TABLAS = {
		CREATE_TABLE_CODIGOS_VACIOS_VENTA, 
		CREATE_TABLE_CODIGOS_LLENOS_VENTA, 
		CREATE_TABLE_CODIGOS_VACIOS_COMPRA, 
		CREATE_TABLE_CODIGOS_LLENOS_COMPRA, 
		CREATE_TABLE_CODIGOS_VACIOS_PROVISIONAL, 
		CREATE_TABLE_CODIGOS_LLENOS_PROVISIONAL, 
		CREATE_TABLE_LINEAS_ALBARAN_ACTIVA, 
		CREATE_TABLE_SERVIDORES_LOCALES, 
		CREATE_TABLE_SERVIDORES_REMOTOS, 
		CREATE_TABLE_PASSWORD, 
		CREATE_TABLE_IMPRESORAS,
		CREATE_TABLE_PORTES,
		CREATE_TABLE_REGLAS_PORTES
	};
		
	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_TABLE_CODIGOS_VACIOS_VENTA);
		database.execSQL(CREATE_TABLE_CODIGOS_LLENOS_VENTA);
		database.execSQL(CREATE_TABLE_CODIGOS_VACIOS_COMPRA);
		database.execSQL(CREATE_TABLE_CODIGOS_LLENOS_COMPRA);
		database.execSQL(CREATE_TABLE_CODIGOS_VACIOS_PROVISIONAL);
		database.execSQL(CREATE_TABLE_CODIGOS_LLENOS_PROVISIONAL);
		database.execSQL(CREATE_TABLE_LINEAS_ALBARAN_ACTIVA);
		database.execSQL(CREATE_TABLE_SERVIDORES_LOCALES);
		database.execSQL(CREATE_TABLE_SERVIDORES_REMOTOS);
		database.execSQL(CREATE_TABLE_PASSWORD);
		database.execSQL(CREATE_TABLE_IMPRESORAS);
		database.execSQL(CREATE_TABLE_PORTES);
		database.execSQL(CREATE_TABLE_REGLAS_PORTES);
		
		long servidorActLocal = insertaServidor(database, "locales", "192.168.0.2", "443", 1);
		servidorActLocal = insertaServidor(database, "locales", "192.168.0.2", "80", 0);
//		servidorActLocal = insertaServidor(database, "locales", "192.168.200.2", "80");
		if (servidorActLocal > -1){
			activaServidor(database, servidorActLocal, "locales");
		}

		long servidorActRemoto = insertaServidor(database, "remotos", "85.152.57.14", "7447", 1);
		servidorActRemoto = insertaServidor(database, "remotos", "85.152.57.14", "80", 0);
//		servidorActRemoto = insertaServidor(database, "remotos", "tg2k.no-ip.org", "7007");
		if (servidorActRemoto > -1){
			activaServidor(database, servidorActRemoto, "remotos");
		}
		
		ContentValues valuesPass = new ContentValues();
		valuesPass.put("password", "0000");
    	database.insert("table_passwords", null, valuesPass);
    	
    	ContentValues valuesPrinters = new ContentValues();
		valuesPrinters.put("etiqueta_impresora", "I1");
		valuesPrinters.put("nombre_impresora", "PB51");
		valuesPrinters.put("direccion_impresora", "00066608A5C3");
		valuesPrinters.put("activa", 1);
    	database.insert("table_impresoras", null, valuesPrinters);
    	valuesPrinters.put("etiqueta_impresora", "I2");
		valuesPrinters.put("nombre_impresora", "PB51");
		valuesPrinters.put("direccion_impresora", "00066608A57D");
		valuesPrinters.put("activa", 0);
    	database.insert("table_impresoras", null, valuesPrinters);
	   
	}
	
	/* CODIGOS VACIOS */
	
	
	public int actualizaCodigosVacios(SQLiteDatabase db, String m_inoutline_id, String documento,
			String descripcionLinea, List<String[]> listaCodigosVacios, String stringCompraOVenta){
		
		for (String[] stringCodigoVacio : listaCodigosVacios) {
		    if (!stringCodigoVacio[1].equals("") & !stringCodigoVacio[1].equals("---")){
		    	ContentValues values = new ContentValues();
		    	values.put("m_inoutline_id", m_inoutline_id);
		    	values.put("cantidad", stringCodigoVacio[0]);
		    	values.put("codigo_vacio", stringCodigoVacio[1]);
		    	values.put("enviado", "0");
		    	values.put("documento", documento);
		    	values.put("descripcion", descripcionLinea);
				db.insertWithOnConflict("table_codigos_vacios_" + stringCompraOVenta,
					null, values, SQLiteDatabase.CONFLICT_IGNORE);
		    }
		}
		return cuentaCodigosVacios(db, m_inoutline_id, stringCompraOVenta);
	}
	
	public void actualizaCantidadCodigosVacios(SQLiteDatabase db, String m_inoutline_id, 
			String codigo, String cantidad, String stringCompraOVenta){
		String[] inoutLine = new String[1];
		inoutLine[0] = m_inoutline_id;
		ContentValues values = new ContentValues();
    	values.put("cantidad", Integer.valueOf(cantidad));
    	values.put("codigo_vacio", codigo);
    	String consultaString = "update table_codigos_vacios_" + stringCompraOVenta 
    			+ " set cantidad=" + cantidad + " "
    			+ "where m_inoutline_id='" + m_inoutline_id + "' and "
    					+ "codigo_vacio='" + codigo + "' and enviado='0'";
    	db.execSQL(consultaString);
		//db.update("table_codigos_vacios_" + stringCompraOVenta, 
		//		values, "m_inoutline_id = ? and enviado = '0'", inoutLine);
	}
	
	public int cuentaCodigosVacios(SQLiteDatabase db, String m_inoutline_id, String stringCompraOVenta){
		int cantidadCodigos = 0;
		String consultaCuenta = 
				"select count(m_inoutline_id) as cantidad_codigos from "
				+ "table_codigos_vacios_" + stringCompraOVenta
				+ " where m_inoutline_id = '" + m_inoutline_id + "' and enviado = '0'";
		Cursor curs = db.rawQuery(consultaCuenta, null);
		curs.moveToPosition(0);
		cantidadCodigos = curs.getInt(curs.getColumnIndex("cantidad_codigos"));
		curs.close();
		return cantidadCodigos;
	}
	
	public void borraCodigoVacio(SQLiteDatabase db, String m_inoutline_id, String stringCodigoVacio,
			String stringCompraOVenta){
		String[] codigoVacio = new String[2];
		codigoVacio[0] = m_inoutline_id;
		codigoVacio[1] = stringCodigoVacio;
		db.delete("table_codigos_vacios_" + stringCompraOVenta, 
				"m_inoutline_id = ? and codigo_vacio=? and enviado = '0'", codigoVacio);
	}
	

	
	public List<String[]> recuperaCodigosVacios(SQLiteDatabase db, String m_inoutline_id, 
			String stringCompraOVenta){
		List<String[]> listaCodigosVacios = new ArrayList<String[]>();
		String consultaCodigosVacios = "select m_inoutline_id, codigo_vacio, cantidad from "
				+ "table_codigos_vacios_" + stringCompraOVenta 
				+ " where ((m_inoutline_id = '" + m_inoutline_id + "') and "
						+ "(enviado = '0')) order by _id desc";
		Cursor curs = db.rawQuery(consultaCodigosVacios, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			String[] arrayCodigo = new String[2];
			arrayCodigo[0] = curs.getString(curs.getColumnIndex("cantidad"));
			arrayCodigo[1] = curs.getString(curs.getColumnIndex("codigo_vacio"));
			listaCodigosVacios.add(arrayCodigo);
		}
		curs.close();
		return listaCodigosVacios;
	}
	
	public void marcaCodigosVaciosEnviados(SQLiteDatabase db, String m_inoutline_id, String documento, 
			String stringCompraOVenta){
		String[] codigoVacio = new String[1];
		codigoVacio[0] = m_inoutline_id;
		ContentValues values = new ContentValues();
        Calendar calendar = Calendar.getInstance();
        List<String[]> listaCodigosEnviados = recuperaCodigosVacios(db, m_inoutline_id, stringCompraOVenta);
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss"); 
		//String dateString = formatter.format(new Date(calendar.getTimeInMillis()));
        String dateString = String.valueOf(calendar.getTimeInMillis());
    	values.put("enviado", dateString);
		db.update("table_codigos_vacios_" + stringCompraOVenta, 
				values, "m_inoutline_id = ? and enviado = '0'", codigoVacio);
		for (String[] codigoEnviado : listaCodigosEnviados) {
			ContentValues valuesAux = new ContentValues();
			valuesAux.put("enviado", "0");
	    	valuesAux.put("m_inoutline_id", m_inoutline_id);
	    	valuesAux.put("cantidad", codigoEnviado[0]);
	    	valuesAux.put("codigo_vacio", codigoEnviado[1]);
	    	valuesAux.put("documento", documento);
	    	db.insert("table_codigos_vacios_" + stringCompraOVenta, null, valuesAux);
		}
	}
	
	public String getStringCodigosVaciosPost(SQLiteDatabase db, String lineaId, String stringCompraOVenta){
		List<String[]> listaCodigosArray = recuperaCodigosVacios(db, lineaId, stringCompraOVenta);
		List<String> listaCodigos = new  ArrayList<String>();
		for (String[] lineaCodigo : listaCodigosArray) {
			for (int i = 0; i < Integer.valueOf(lineaCodigo[0]); i++) {
				listaCodigos.add(lineaCodigo[1]);
			}
		}
		return listToStringCharSV(listaCodigos, ",");
	}
	

		
	
	
	/* CODIGOS LLENOS */
	
	public int actualizaCodigosLlenos(SQLiteDatabase db, String m_inoutline_id, String documento,
			String descripcionLinea, List<String[]> listaCodigosLlenos, String stringCompraOVenta){
		for (String[] stringCodigoLleno : listaCodigosLlenos) {
		    if (!stringCodigoLleno[1].equals("") & !stringCodigoLleno[1].equals("---")){
		    	ContentValues values = new ContentValues();
		    	values.put("m_inoutline_id", m_inoutline_id);
		    	values.put("codigo_lleno", stringCodigoLleno[1]);
		    	values.put("cantidad", stringCodigoLleno[0]);
		    	values.put("enviado", "0");
		    	values.put("documento", documento);
		    	values.put("descripcion", descripcionLinea);
		    	db.insertWithOnConflict("table_codigos_llenos_" + stringCompraOVenta,
					null, values, SQLiteDatabase.CONFLICT_IGNORE);
		    }
		}
		return cuentaCodigosLlenos(db, m_inoutline_id, stringCompraOVenta);
	}
	
	public void actualizaCantidadCodigosLlenos(SQLiteDatabase db, String m_inoutline_id, 
			String codigo, String cantidad, String stringCompraOVenta){
		String[] inoutLine = new String[1];
		inoutLine[0] = m_inoutline_id;
		ContentValues values = new ContentValues();
    	values.put("cantidad", Integer.valueOf(cantidad));
    	values.put("codigo_lleno", codigo);
    	String consultaString = "update table_codigos_llenos_" + stringCompraOVenta 
    			+ " set cantidad=" + cantidad + " "
    			+ "where m_inoutline_id='" + m_inoutline_id + "' and "
    					+ "codigo_lleno='" + codigo + "' and enviado ='0'";
    	db.execSQL(consultaString);
		//db.update("table_codigos_llenos_" + stringCompraOVenta, 
		//		values, "m_inoutline_id = ? and enviado = '0'", inoutLine);
	}
	
	public int cuentaCodigosLlenos(SQLiteDatabase db, String m_inoutline_id, String stringCompraOVenta){
		int cantidadCodigos = 0;
		String consultaCuenta = 
				"select count(m_inoutline_id) as cantidad_codigos "
				+ "from table_codigos_llenos_" + stringCompraOVenta 
				+ " where m_inoutline_id = '" + m_inoutline_id + "' and enviado = '0'";
		Cursor curs = db.rawQuery(consultaCuenta, null);
		curs.moveToPosition(0);
		cantidadCodigos = curs.getInt(curs.getColumnIndex("cantidad_codigos"));
		curs.close();
		return cantidadCodigos;
	}
	
	public void borraCodigoLleno(SQLiteDatabase db, String m_inoutline_id, String stringCodigoLleno,
			 String stringCompraOVenta){
		String[] codigoLleno = new String[2];
		codigoLleno[0] = m_inoutline_id;
		codigoLleno[1] = stringCodigoLleno;
		db.delete("table_codigos_llenos_" + stringCompraOVenta,
				"m_inoutline_id = ? and codigo_lleno=? and enviado = '0'", codigoLleno);
	}		
	
	
	public List<String[]> recuperaCodigosLlenos(SQLiteDatabase db, String m_inoutline_id, 
			String stringCompraOVenta){
		List<String[]> listaCodigosLlenos = new ArrayList<String[]>();
		String consultaCodigosLlenos = "select m_inoutline_id, codigo_lleno, cantidad from "
				+ "table_codigos_llenos_" + stringCompraOVenta  
				+ " where ((m_inoutline_id = '" + m_inoutline_id + "') and "
				+ "(enviado = '0'))  order by _id desc";
		Cursor curs = db.rawQuery(consultaCodigosLlenos, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			String[] arrayCodigo = new String[2];
			arrayCodigo[0] = curs.getString(curs.getColumnIndex("cantidad"));
			arrayCodigo[1] = curs.getString(curs.getColumnIndex("codigo_lleno"));
			listaCodigosLlenos.add(arrayCodigo);
		}
		curs.close();
		return listaCodigosLlenos;
	}
	
	public void marcaCodigosLlenosEnviados(SQLiteDatabase db, String m_inoutline_id, String documento,
			String stringCompraOVenta){
		String[] codigoLleno = new String[1];
		codigoLleno[0] = m_inoutline_id;
        List<String[]> listaCodigosEnviados = recuperaCodigosLlenos(db, m_inoutline_id, stringCompraOVenta);
		ContentValues values = new ContentValues();
		Calendar calendar = Calendar.getInstance();
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss"); 
		//String dateString = formatter.format(new Date(calendar.getTimeInMillis()));
        String dateString = String.valueOf(calendar.getTimeInMillis());
		values.put("enviado", dateString);
		db.update("table_codigos_llenos_" + stringCompraOVenta, 
				values, "m_inoutline_id = ? and enviado = '0'", codigoLleno);
		for (String[] codigoEnviado : listaCodigosEnviados) {
			ContentValues valuesAux = new ContentValues();
			valuesAux.put("enviado", "0");
	    	valuesAux.put("m_inoutline_id", m_inoutline_id);
	    	valuesAux.put("cantidad", codigoEnviado[0]);
	    	valuesAux.put("codigo_lleno", codigoEnviado[1]);
	    	valuesAux.put("documento", documento);
	    	db.insert("table_codigos_llenos_" + stringCompraOVenta, null, valuesAux);
		}
	}
	
	public String getStringCodigosLlenosPost(SQLiteDatabase db, String lineaId, String stringCompraOVenta){
		List<String[]> listaCodigosArray = recuperaCodigosLlenos(db, lineaId, stringCompraOVenta);		
		List<String> listaCodigos = new  ArrayList<String>();
		for (String[] lineaCodigo : listaCodigosArray) {
			for (int i = 0; i < Integer.valueOf(lineaCodigo[0]); i++) {
				listaCodigos.add(lineaCodigo[1]);
			}
		}
		return listToStringCharSV(listaCodigos, ",");
	}
	
	
	//**  LLENOS Y VACIOS **//
	
	
	public List<String[]> recuperaCodigosAntiguosVenta(SQLiteDatabase db){
		List<String[]> listaCodigosVacios = new ArrayList<String[]>();
		List<String[]> listaCodigosLlenos = new ArrayList<String[]>();
		List<String[]> listaAlbaranes = new ArrayList<String[]>();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String consultaCodigosVacios = "select m_inoutline_id, documento, codigo_vacio, enviado from "
				+ "table_codigos_vacios_venta where (enviado != '0')";
		Cursor cursVacios = db.rawQuery(consultaCodigosVacios, null);
		int numFilas = cursVacios.getCount();
		for (int i = 0; i < numFilas; i++) {
			cursVacios.moveToPosition(i);
			String[] arrayCodigo = new String[3];
			arrayCodigo[0] = cursVacios.getString(cursVacios.getColumnIndex("m_inoutline_id"));
			arrayCodigo[1] = cursVacios.getString(cursVacios.getColumnIndex("codigo_vacio"));
		    calendar.setTimeInMillis(Long.valueOf(cursVacios.getString(cursVacios.getColumnIndex("enviado"))));
			arrayCodigo[2] = formatter.format(calendar.getTime());
			listaCodigosVacios.add(arrayCodigo);
		}
		cursVacios.close();
		String consultaCodigosLlenos = "select m_inoutline_id, codigo_lleno, enviado from "
				+ "table_codigos_llenos_venta where (enviado != '0')";
		Cursor cursLlenos = db.rawQuery(consultaCodigosLlenos, null);
		numFilas = cursLlenos.getCount();
		for (int i = 0; i < numFilas; i++) {
			cursLlenos.moveToPosition(i);
			String[] arrayCodigo = new String[3];
			arrayCodigo[0] = cursLlenos.getString(cursLlenos.getColumnIndex("m_inoutline_id"));
			arrayCodigo[1] = cursLlenos.getString(cursLlenos.getColumnIndex("codigo_lleno"));
		    calendar.setTimeInMillis(Long.valueOf(cursLlenos.getString(cursLlenos.getColumnIndex("enviado"))));
			arrayCodigo[2] = formatter.format(calendar.getTime());
			listaCodigosLlenos.add(arrayCodigo);
		}
		cursLlenos.close();
		String consultaAlbaranes = "select distinct m_inoutline_id, descripcion, documento, enviado from ("
				+ " select  m_inoutline_id, descripcion, documento, enviado "
				+ " from table_codigos_vacios_venta "
				+ " where table_codigos_vacios_venta.enviado != '0' "
				+ " union"
				+ " select  m_inoutline_id, descripcion, documento, enviado "
				+ " from table_codigos_llenos_venta "
				+ " where table_codigos_llenos_venta.enviado != '0'"
				+ ") "
				+ "order by documento, m_inoutline_id";
		Cursor cursAlbaranes = db.rawQuery(consultaAlbaranes, null);
		numFilas = cursAlbaranes.getCount();
		for (int i = 0; i < numFilas; i++) {
			cursAlbaranes.moveToPosition(i);
			String[] arrayCodigo = {"", "", "", "", "", ""};
			arrayCodigo[0] = cursAlbaranes.getString(cursAlbaranes.getColumnIndex("m_inoutline_id"));
			arrayCodigo[1] = cursAlbaranes.getString(cursAlbaranes.getColumnIndex("descripcion"));
			arrayCodigo[2] = cursAlbaranes.getString(cursAlbaranes.getColumnIndex("documento"));
		    calendar.setTimeInMillis(Long.valueOf(cursAlbaranes.getString(cursAlbaranes.getColumnIndex("enviado"))));
			arrayCodigo[3] = formatter.format(calendar.getTime());
			listaAlbaranes.add(arrayCodigo);
		}
		cursAlbaranes.close();
		for (String[] lineaAlbaran : listaAlbaranes) {
			String codigosVacios = "";
			boolean primerCodVac = true;
			for (String[] lineaCodigo : listaCodigosVacios) {
				if (lineaCodigo[0].equals(lineaAlbaran[0]) & lineaCodigo[2].equals(lineaAlbaran[3])){
					if (primerCodVac){
						codigosVacios = lineaCodigo[1];
						primerCodVac = false;
					} else {
						codigosVacios = codigosVacios + "\n" + lineaCodigo[1];
					}
				}
			}
			String codigosLlenos = "";
			boolean primerCodLle = true;
			for (String[] lineaCodigo : listaCodigosLlenos) {
				if (lineaCodigo[0].equals(lineaAlbaran[0]) & lineaCodigo[2].equals(lineaAlbaran[3])){
					if (primerCodLle){
						codigosLlenos = lineaCodigo[1];
						primerCodLle = false;
					} else {
						codigosLlenos = codigosLlenos + "\n" + lineaCodigo[1];
					}
				}
			}
			lineaAlbaran[4] = codigosLlenos;
			lineaAlbaran[5] = codigosVacios;
		}
		
		return listaAlbaranes;
	}
	
	//Devuelve una lista con las posiciones activas del array INCIDENCIAS 
	
	public List<Integer> recuperaIncidenciasCodigo(SQLiteDatabase db, String m_inoutline_id, 
			String codigo, String stringCompraOVenta, int llenos_o_vacios){
		List<Integer> listaIncidencias = new ArrayList<Integer>();
		String consultaIncidenciasCodigos = "";
		switch (llenos_o_vacios) {
		case CODIGOS_VACIOS:
			consultaIncidenciasCodigos = "select m_inoutline_id, codigo_vacio, incidencia from "
					+ "table_codigos_vacios_" + stringCompraOVenta 
					+ " where ((m_inoutline_id = '" + m_inoutline_id + "') and "
							+ "(enviado = '0') and (codigo_vacio ='"+ codigo +"'))";
			break;
		case CODIGOS_LLENOS:
			consultaIncidenciasCodigos = "select m_inoutline_id, codigo_lleno, incidencia from "
					+ "table_codigos_llenos_" + stringCompraOVenta 
					+ " where ((m_inoutline_id = '" + m_inoutline_id + "') and "
							+ "(enviado = '0') and (codigo_lleno ='"+ codigo +"'))";
			break;
		default:
			break;
		}

    	Cursor curs = db.rawQuery(consultaIncidenciasCodigos, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			int incidenciaInteger = curs.getInt(curs.getColumnIndex("incidencia"));
			String incidenciaString = Integer.toBinaryString(incidenciaInteger);
			int posicion = incidenciaString.length() - 1;
			for (int j = 0; j < incidenciaString.length(); j++) {
				if (incidenciaString.substring(posicion, posicion+1).equals("1")){
//						String numeroIncidenciaString = String.format("%2d", j);
//						int indiceIncidencia = indiceArrayIncidencias(INCIDENCIAS, numeroIncidenciaString);
//						listaIncidencias.add(INCIDENCIAS[indiceIncidencia]);
					listaIncidencias.add(j);
				}
				posicion--;
			}
		}
		curs.close();
		return listaIncidencias;
	}
	
	
	public int cuentaIncidenciasCodigo(SQLiteDatabase db, String m_inoutline_id, 
			String codigo, String stringCompraOVenta, int llenos_o_vacios){
		int cantidad = 0;
		List<Integer> listaIncidencias = 
				recuperaIncidenciasCodigo(db, m_inoutline_id, codigo, stringCompraOVenta, llenos_o_vacios);
		cantidad = listaIncidencias.size();
		return cantidad;
		
	}
	
	
	public void grabaIncidenciasCodigo(SQLiteDatabase db, String m_inoutline_id, 
			String codigo, String stringCompraOVenta, int llenos_o_vacios, 
			List<Integer> listaIncidenciasActivas){
		String consultaString = "";
		String llenoVacioString = "";
		switch (llenos_o_vacios) {
		case CODIGOS_VACIOS:
			consultaString = "table_codigos_vacios_" + stringCompraOVenta;
			llenoVacioString = "vacio";
			break;
		case CODIGOS_LLENOS:
			consultaString = "table_codigos_llenos_" + stringCompraOVenta;
			llenoVacioString = "lleno";
			break;
		default:
			break;
		}
		String[] lineaCodigo = new String[2];
		lineaCodigo[0] = m_inoutline_id;
		lineaCodigo[1] = codigo;
		int valorIncidencia = 0;
		for (int intIncidencia: listaIncidenciasActivas) {
			valorIncidencia = valorIncidencia + (int) Math.pow(2, intIncidencia);
		}

		ContentValues values = new ContentValues();
    	values.put("incidencia", valorIncidencia);
		db.update(consultaString, 
				values, "m_inoutline_id = ? and codigo_" + llenoVacioString + 
				" = ? and enviado = '0'", lineaCodigo);
		//Toast.makeText(context, String.valueOf(valorIncidencia), Toast.LENGTH_LONG).show();
	}
	
	public void borraCodigosAntiguos(SQLiteDatabase db, String stringLlenosVacios,
		String stringCompraOVenta, String diasActivo){
        Calendar calendar = Calendar.getInstance();
		long timeNow = calendar.getTimeInMillis();
		long timeGap = Long.valueOf(diasActivo) * 24 * 60 * 60 * 1000;
		long timeMax = timeNow - timeGap;
		String timeMaxString = String.valueOf(timeMax);
		String whereString = "";
		if (diasActivo.equals("-1")) {
			whereString = " where enviado = '0'";
		} else {
			whereString = " where (enviado != '0') and (cast(enviado as integer) < " + timeMaxString + ")";
		}
		db.execSQL("delete from table_codigos_" + stringLlenosVacios + "_" + stringCompraOVenta + 
				whereString);
	}
	
	public boolean existeLineaAlbaranActiva (SQLiteDatabase db, String m_inoutline_id){
		boolean existeLinea = false;
		String consultaLineaAlbaranActiva = "select m_inoutline_id from table_lineas_albaran_activas "
				+ "where m_inoutline_id = '" + m_inoutline_id + "'";
		Cursor curs = db.rawQuery(consultaLineaAlbaranActiva, null);
		int numFilas = curs.getCount();
		curs.close();
		if (numFilas > 0) {
			existeLinea = true;
		}
		return existeLinea;
	}
	
	public void insertaLineaAlbaranActiva (SQLiteDatabase db, String m_inoutline_id){
		ContentValues values = new ContentValues();
    	values.put("m_inoutline_id", m_inoutline_id);
    	db.insertWithOnConflict("table_lineas_albaran_activas",
			null, values, SQLiteDatabase.CONFLICT_IGNORE);
	}
	
	public void borraTablaLineasAlbaranActivas (SQLiteDatabase db) {
		db.execSQL("delete from table_lineas_albaran_activas");
	}
	
	//**  INCIDENCIAS **//
	
	public String getStringIncidenciasPost(SQLiteDatabase db, String lineaId, String stringCompraOVenta){
		List<String[]> listaCodigosLlenos = recuperaCodigosLlenos(db, lineaId, stringCompraOVenta);
		List<String[]> listaCodigosVacios = recuperaCodigosVacios(db, lineaId, stringCompraOVenta);
		List<String> listaIncidencias = new ArrayList<String>();
		for (String[] cadaCodigo : listaCodigosLlenos) {
			List<Integer> incidenciasCodigo = 
					recuperaIncidenciasCodigo(db, lineaId, cadaCodigo[1], stringCompraOVenta, CODIGOS_LLENOS);
			for (Integer id_incidencia : incidenciasCodigo) {
				String incidenciaString = LETRAS[id_incidencia] + "|" +
						INCIDENCIAS[id_incidencia] + "|" + cadaCodigo;
				listaIncidencias.add(incidenciaString);
			}
		}
		for (String[] cadaCodigo : listaCodigosVacios) {
			List<Integer> incidenciasCodigo = 
					recuperaIncidenciasCodigo(db, lineaId, cadaCodigo[1], stringCompraOVenta, CODIGOS_VACIOS);
			for (Integer id_incidencia : incidenciasCodigo) {
				String incidenciaString = LETRAS[id_incidencia] + "|" +
						INCIDENCIAS[id_incidencia] + "|" + cadaCodigo[1];
				listaIncidencias.add(incidenciaString);
			}
		}
		return listToStringCharSV(listaIncidencias, "#");
	}
	
	public String[] recuperaTablaIncidencias(){
		return INCIDENCIAS;
	}
	
	
	//**  SERVIDORES **//
	
	public List<String[]> recuperaServidores(SQLiteDatabase db, String locales_remotos){
		List<String[]> listaServidores = new ArrayList<String[]>();
		String consultaServidores = "select _id, direccion_servidor, puerto_servidor, activo, ssl from "
		+ "table_servidores_" + locales_remotos;
		Cursor curs = db.rawQuery(consultaServidores, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			String[] lineaServidor = new String[5];
			lineaServidor[0] = String.valueOf(curs.getInt(curs.getColumnIndex("_id")));
			lineaServidor[1] = curs.getString(curs.getColumnIndex("direccion_servidor"));
			lineaServidor[2] = curs.getString(curs.getColumnIndex("puerto_servidor"));
			lineaServidor[3] = String.valueOf(curs.getInt(curs.getColumnIndex("activo")));
			lineaServidor[4] = String.valueOf(curs.getInt(curs.getColumnIndex("ssl")));
			listaServidores.add(lineaServidor);
		}
		curs.close();

		return listaServidores;
	}
	
	public String[] recuperaServidorPorId(SQLiteDatabase db, String locales_remotos, int id_servidor){
		String[] lineaServidor = new String[4];
		List<String[]> listaServidores = recuperaServidores(db, locales_remotos);
		for (String[] stringLinea : listaServidores) {
			if (stringLinea[0].equals(String.valueOf(id_servidor))){
				lineaServidor = stringLinea;
				break;
			}
		}
		return lineaServidor;
	}
	
	public String recuperaServidorActivo(SQLiteDatabase db, String locales_remotos){
		String[] lineaServidor = new String[5];
		List<String[]> listaServidores = recuperaServidores(db, locales_remotos);
		String protocoloString = "http://";
		for (String[] stringLinea : listaServidores) {
			if (stringLinea[3].equals("1")){
				lineaServidor = stringLinea;
				break;
			}
		}
		if (lineaServidor[4].equals("1")){
			protocoloString = "https://";
		}
		return protocoloString + lineaServidor[1] + ":" + lineaServidor[2];
	}
	
	public void actualizaServidor(SQLiteDatabase db, int id_servidor, String locales_remotos, 
			String direccion, String puerto, int activo, int ssl){
				String tabla_servidor = "table_servidores_" + locales_remotos;
				String id_servidorString = String.valueOf(id_servidor);
				String[] idServidor = { id_servidorString };
		    	ContentValues values = new ContentValues();
		    	values.put("direccion_servidor", direccion);
		    	values.put("puerto_servidor", puerto);
		    	values.put("activo", activo);
		    	values.put("ssl", ssl);
		    	db.update(tabla_servidor, values, "_id = ?", idServidor);
	}
	
	public long insertaServidor(SQLiteDatabase db, String locales_remotos, 
			String direccion, String puerto, int ssl){
				String tabla_servidor = "table_servidores_" + locales_remotos;
		    	ContentValues values = new ContentValues();
		    	values.put("direccion_servidor", direccion);
		    	values.put("puerto_servidor", puerto);
		    	values.put("ssl", ssl);
		    	return db.insert(tabla_servidor, null, values);
	}
	
	public void borraServidor(SQLiteDatabase db, int id_servidor, String locales_remotos){
				String tabla_servidor = "table_servidores_" + locales_remotos;
				String id_servidorString = String.valueOf(id_servidor);
				String[] idServidor = { id_servidorString };
		    	db.delete(tabla_servidor, "_id = ? and activo = 0", idServidor);
	}
	
	public void desactivaServidores(SQLiteDatabase db, String locales_remotos){
				String tabla_servidor = "table_servidores_" + locales_remotos;
		    	ContentValues values = new ContentValues();
		    	values.put("activo", 0);
		    	db.update(tabla_servidor, values, "1 = 1", null);
	}
	
	public void activaServidor(SQLiteDatabase db, long id_servidor, String locales_remotos){
				String tabla_servidor = "table_servidores_" + locales_remotos;
				String id_servidorString = String.valueOf(id_servidor);
				String[] idServidor = { id_servidorString };
		    	ContentValues values = new ContentValues();
		    	values.put("activo", 1);
		    	db.update(tabla_servidor, values, "_id = ?", idServidor);
		    	String consultaServidor = "select _id, direccion_servidor, puerto_servidor, ssl from "
		    			+ "table_servidores_" + locales_remotos + " where _id = " + 
		    			String.valueOf(id_servidor);
		    	Cursor curs = db.rawQuery(consultaServidor, null);
				int numFilas = curs.getCount();
				String servidor = "";
				String puerto = "";
				int ssl = 0;
				String local_remoto = locales_remotos.equals("locales")?"local":"remoto";
				for (int i = 0; i < numFilas; i++) {
					curs.moveToPosition(i);
					servidor = curs.getString(curs.getColumnIndex("direccion_servidor"));
					puerto = curs.getString(curs.getColumnIndex("puerto_servidor"));
					ssl = curs.getInt(curs.getColumnIndex("ssl"));
				}
				curs.close();
		    	MySQLiteHelperSync myHelperSync = new MySQLiteHelperSync(context);
				SQLiteDatabase dbsync =  myHelperSync.getWritableDatabase();
				myHelperSync.actualizaDatosServidor(dbsync, local_remoto, servidor, puerto, ssl);
				dbsync.close();
				myHelperSync.close();
	}
	
	//**  PASSWORD **//

	public String recuperaPassword(SQLiteDatabase db) {
		String consultaPassword = 
				"select password "
				+ "from table_passwords";
		Cursor curs = db.rawQuery(consultaPassword, null);
		curs.moveToPosition(0);
		String password = curs.getString(curs.getColumnIndex("password"));
		curs.close();
		return password;
	}
	
	public boolean actualizaPassword(SQLiteDatabase db, String password){
		boolean exito = false;
		ContentValues values = new ContentValues();
		values.put("password", password);
		String consultaPassword = 
				"select _id, password "
				+ "from table_passwords";
		Cursor curs = db.rawQuery(consultaPassword, null);
		curs.moveToPosition(0);
		int  idFila = curs.getInt(curs.getColumnIndex("_id"));
		String[] idFilaString = { String.valueOf(idFila)};
		curs.close();
    	int filas = db.update("table_passwords", values, "_id = ?", idFilaString);
    	if (filas > 0){
    		exito = true;
    	}
    	return exito;
	}
	
	//**  IMPRESORAS **//
	
	public List<String[]> recuperaImpresoras(SQLiteDatabase db){
		List<String[]> listaImpresoras = new ArrayList<String[]>();
		String consultaImpresoras = "select _id, etiqueta_impresora, nombre_impresora, direccion_impresora, activa from "
		+ "table_impresoras";
		Cursor curs = db.rawQuery(consultaImpresoras, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			String[] lineaImpresora = new String[5];
			lineaImpresora[0] = String.valueOf(curs.getInt(curs.getColumnIndex("_id")));
			lineaImpresora[1] = curs.getString(curs.getColumnIndex("etiqueta_impresora"));
			lineaImpresora[2] = curs.getString(curs.getColumnIndex("nombre_impresora"));
			lineaImpresora[3] = curs.getString(curs.getColumnIndex("direccion_impresora"));
			lineaImpresora[4] = String.valueOf(curs.getInt(curs.getColumnIndex("activa")));
			listaImpresoras.add(lineaImpresora);
		}
		curs.close();

		return listaImpresoras;
	}
	
	public String[] recuperaImpresoraPorId(SQLiteDatabase db, int id_printer){
		String[] lineaPrinter = new String[5];
		List<String[]> listaPrinters = recuperaImpresoras(db);
		for (String[] stringLinea : listaPrinters) {
			if (stringLinea[0].equals(String.valueOf(id_printer))){
				lineaPrinter = stringLinea;
				break;
			}
		}
		return lineaPrinter;
	}
	
	public String[] recuperaImpresoraActiva(SQLiteDatabase db){
		String[] lineaImpresora = new String[5];
		List<String[]> listaImpresoras = recuperaImpresoras(db);
		for (String[] stringLinea : listaImpresoras) {
			if (stringLinea[4].equals("1")){
				lineaImpresora = stringLinea;
				break;
			}
		}
		return lineaImpresora;
	}

	public void actualizaImpresora(SQLiteDatabase db, int id_impresora, String etiqueta, 
			String nombre, String direccion, int activa){
				String tabla_impresora = "table_impresoras";
				String id_impresoraString = String.valueOf(id_impresora);
				String[] idImpresora = { id_impresoraString };
		    	ContentValues values = new ContentValues();
		    	values.put("etiqueta_impresora", etiqueta);
		    	values.put("nombre_impresora", nombre);
		    	values.put("direccion_impresora", direccion);
		    	values.put("activa", activa);
		    	db.update(tabla_impresora, values, "_id = ?", idImpresora);
	}
	
	public long insertaImpresora(SQLiteDatabase db, String etiqueta, String nombre, String direccion){
				String tabla_impresoras = "table_impresoras";
		    	ContentValues values = new ContentValues();
		    	values.put("etiqueta_impresora", etiqueta);
		    	values.put("nombre_impresora", nombre);
		    	values.put("direccion_impresora", direccion);
		    	return db.insert(tabla_impresoras, null, values);
	}
	
	public void borraImpresora(SQLiteDatabase db, int id_impresora){
		String tabla_impresoras = "table_impresoras";
		String id_impresoraString = String.valueOf(id_impresora);
		String[] idImpresora = { id_impresoraString };
    	db.delete(tabla_impresoras, "_id = ? and activa = 0", idImpresora);
	}

	public void desactivaImpresoras(SQLiteDatabase db){
		String tabla_impresoras = "table_impresoras";
    	ContentValues values = new ContentValues();
    	values.put("activa", 0);
    	db.update(tabla_impresoras, values, "1 = 1", null);
	}
	
	public void activaImpresora(SQLiteDatabase db, long id_impresora){
		desactivaImpresoras(db);
		String tabla_impresoras = "table_impresoras";
		String id_impresoraString = String.valueOf(id_impresora);
		String[] idImpresora = { id_impresoraString };
    	ContentValues values = new ContentValues();
    	values.put("activa", 1);
    	db.update(tabla_impresoras, values, "_id = ?", idImpresora);
	}
	
	
	//**  REGLAS DE PORTES **//
	
	public void actualizaPortes(SQLiteDatabase db, List<String[]> listaPortes){
		String tabla_portes = "table_portes";
		db.delete(tabla_portes, "1=1", null);
		for (String[] porte : listaPortes) {
	    	ContentValues values = new ContentValues();
	    	values.put("m_freightcostrule_id", porte[0]);
	    	values.put("ad_client_id", porte[1]);
	    	values.put("ad_org_id", porte[2]);
	    	values.put("isactive", porte[3]);
	    	values.put("created", porte[4]);
	    	values.put("createdby", porte[5]);
	    	values.put("updated", porte[6]);
	    	values.put("updatedby", porte[7]);
	    	values.put("description", porte[8]);
	    	values.put("value", porte[9]);
	    	values.put("name", porte[10]);
	    	db.insert(tabla_portes, null, values);
		}
	}
	
	public void actualizaReglasPortes(SQLiteDatabase db, List<String[]> listaReglasPortes){
		String tabla_reglas = "table_reglas_portes";
		db.delete(tabla_reglas, "1=1", null);
		for (String[] regla : listaReglasPortes) {
	    	ContentValues values = new ContentValues();
	    	values.put("m_freightcostruleline_id", regla[0]);
	    	values.put("ad_client_id", regla[1]);
	    	values.put("ad_org_id", regla[2]);
	    	values.put("isactive", regla[3]);
	    	values.put("created", regla[4]);
	    	values.put("createdby", regla[5]);
	    	values.put("updated", regla[6]);
	    	values.put("updatedby", regla[7]);
	    	values.put("description", regla[8]);
	    	values.put("fromnumber", regla[9]);
	    	values.put("tonumber", regla[10]);
	    	values.put("isbottlecontrolled", regla[11]);
	    	values.put("isoneinstance", regla[12]);
	    	values.put("freightamt", regla[13]);
	    	values.put("m_freightcostrule_id", regla[14]);
	    	db.insert(tabla_reglas, null, values);
		}
	}
	
	public double[] calculaPortes(SQLiteDatabase db, String reglaId, int cantidadBotellas ){
		double cantidadPortes = (double) cantidadBotellas;
		double precioPorte = 0;
		String consultaPortes = "select m_freightcostrule_id, isactive, fromnumber, tonumber, "
				+ "isoneinstance, freightamt from table_reglas_portes "
				+ "where isactive='Y' and cast(m_freightcostrule_id as integer)=" + reglaId + "";
		Cursor curs = db.rawQuery(consultaPortes, null);
		int numFilas = curs.getCount();
		for (int i = 0; i < numFilas; i++) {
			curs.moveToPosition(i);
			double cantidadMin = Double.valueOf(curs.getString(curs.getColumnIndex("fromnumber")));
			double cantidadMax = Double.valueOf(curs.getString(curs.getColumnIndex("tonumber")));
			double estePrecio = Double.valueOf(curs.getString(curs.getColumnIndex("freightamt")));
			String porteUnico = curs.getString(curs.getColumnIndex("isoneinstance"));
			if (porteUnico.equals("Y")){
				precioPorte = estePrecio;
				cantidadPortes = 1;
				break;
			} else if ((cantidadBotellas >= cantidadMin) 
						& ((cantidadBotellas <= cantidadMax) | (cantidadMax == 0))){
					precioPorte = estePrecio;
					cantidadPortes = (double) cantidadBotellas;
					break;
				}
		}
		curs.close();
		double[] portesAux = {cantidadPortes, precioPorte};
		return portesAux;
	}
	

	
	//**  FUNCIONES PRIVADAS **//
	
	private String listToStringCharSV(List<String> lista, String caracter){
		String stringRet = "";
		int dimLista = lista.size();
		int count = 0;
		for (String codigo : lista) {
			stringRet = stringRet + codigo;
			if (count < (dimLista-1)) {
				stringRet = stringRet + caracter;
			}
			count++;
		}
		return stringRet;
	}
	
	private int indiceArrayIncidencias(String[] stringArray, String valorIncidencia){
		int indice = 0;
		for (int i = 0; i < stringArray.length; i++) {
			if (stringArray[i].startsWith(valorIncidencia)){
				indice = i;
				break;
			}
		}
		return indice;
	}

	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		String[] consultasDropTables ={
		"DROP TABLE IF EXISTS table_codigos_vacios_venta", 
		"DROP TABLE IF EXISTS table_codigos_llenos_venta", 
		"DROP TABLE IF EXISTS table_codigos_vacios_compra", 
		"DROP TABLE IF EXISTS table_codigos_llenos_compra",
		"DROP TABLE IF EXISTS table_codigos_vacios_provisional", 
		"DROP TABLE IF EXISTS table_codigos_llenos_provisional", 
		"DROP TABLE IF EXISTS table_lineas_albaran_activas", 
		"DROP TABLE IF EXISTS table_servidores_locales", 
		"DROP TABLE IF EXISTS table_servidores_remotos", 
		"DROP TABLE IF EXISTS table_passwords", 
		"DROP TABLE IF EXISTS table_impresoras", 
		"DROP TABLE IF EXISTS table_portes", 
		"DROP TABLE IF EXISTS table_reglas_portes"
		};
		for (int i = 0; i < consultasDropTables.length; i++) {
			if (TABLAS_A_ACTUALIZAR[i]){
				db.execSQL(consultasDropTables[i]);
				db.execSQL(CONSULTA_CREAR_TABLAS[i]);
			}
		}
		
		if (TABLAS_A_ACTUALIZAR[7]){
			long servidorActLocal = insertaServidor(db, "locales", "192.168.0.2", "443", 1);
			servidorActLocal = insertaServidor(db, "locales", "192.168.0.2", "80", 0);
	//		servidorActLocal = insertaServidor(database, "locales", "192.168.200.2", "80", 0);
			if (servidorActLocal > -1){
				activaServidor(db, servidorActLocal, "locales");
			}
		}
		
		if (TABLAS_A_ACTUALIZAR[8]){
			long servidorActRemoto = insertaServidor(db, "remotos", "85.152.57.14", "7447", 1);
			servidorActRemoto = insertaServidor(db, "remotos", "85.152.57.14", "80", 0);
	//		servidorActRemoto = insertaServidor(database, "remotos", "tg2k.no-ip.org", "7007");
			if (servidorActRemoto > -1){
				activaServidor(db, servidorActRemoto, "remotos");
			}
		}
		
		if (TABLAS_A_ACTUALIZAR[9]){
			ContentValues valuesPass = new ContentValues();
			valuesPass.put("password", "0000");
	    	db.insert("table_passwords", null, valuesPass);
		}
		if (TABLAS_A_ACTUALIZAR[10]){
	    	ContentValues valuesPrinters = new ContentValues();
			valuesPrinters.put("etiqueta_impresora", "I1");
			valuesPrinters.put("nombre_impresora", "PB51");
			valuesPrinters.put("direccion_impresora", "00066608A5C3");
			valuesPrinters.put("activa", 1);
	    	db.insert("table_impresoras", null, valuesPrinters);
	    	valuesPrinters.put("etiqueta_impresora", "I2");
			valuesPrinters.put("nombre_impresora", "PB51");
			valuesPrinters.put("direccion_impresora", "00066608A57D");
			valuesPrinters.put("activa", 0);
	    	db.insert("table_impresoras", null, valuesPrinters);
			//onCreate(db);
		}
	}




	
	
}