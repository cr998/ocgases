package es.paumancar.ocgases;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;



public class ActivityPreferenciasPrinters extends Activity {

	List<String[]> listPrinters;
	MyListView listviewPrinters;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_printers);
        MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
		SQLiteDatabase db = myHelper.getWritableDatabase();
        listPrinters = myHelper.recuperaImpresoras(db);
        db.close();
        myHelper.close();

		listviewPrinters = (MyListView) findViewById(R.id.listviewPrinters);
        listviewPrinters.setAdapter(new AdapterImpresoras(ActivityPreferenciasPrinters.this, listPrinters));
        Button botonAddPrinter = (Button) findViewById(R.id.buttonPrintersNuevo);
        
        botonAddPrinter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final TextView TextEtiqueta = new TextView(ActivityPreferenciasPrinters.this);
				final TextView TextNombre = new TextView(ActivityPreferenciasPrinters.this);
				final TextView TextDireccion = new TextView(ActivityPreferenciasPrinters.this);
				final EditText editTextEtiqueta = new EditText(ActivityPreferenciasPrinters.this);
				final EditText editTextNombre = new EditText(ActivityPreferenciasPrinters.this);
				final EditText editTextDireccion = new EditText(ActivityPreferenciasPrinters.this);
				TextEtiqueta.setText("Etiqueta:   ");
				TextNombre.setText("Nombre:   ");
				TextDireccion.setText("Dirección: ");
				editTextEtiqueta.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				editTextNombre.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				editTextDireccion.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				
				LinearLayout layoutTextos = new LinearLayout(ActivityPreferenciasPrinters.this);
				LinearLayout.LayoutParams layoutForInner = 
						new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
								LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutTextos.setLayoutParams(layoutForInner);
				layoutTextos.setOrientation(LinearLayout.VERTICAL);
				
				//LayoutParams params5Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 5f);
				LayoutParams params3Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3f);
				LayoutParams params7Weight = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 7f);
				
				
				LinearLayout layoutEtiqueta = new LinearLayout(ActivityPreferenciasPrinters.this);
				layoutEtiqueta.setLayoutParams(layoutForInner);
				layoutEtiqueta.setOrientation(LinearLayout.HORIZONTAL);
				TextEtiqueta.setLayoutParams(params3Weight);
				editTextEtiqueta.setLayoutParams(params7Weight);
				layoutEtiqueta.addView(TextEtiqueta);
				layoutEtiqueta.addView(editTextEtiqueta);
				
				LinearLayout layoutNombre = new LinearLayout(ActivityPreferenciasPrinters.this);
				layoutNombre.setLayoutParams(layoutForInner);
				layoutNombre.setOrientation(LinearLayout.HORIZONTAL);
				TextNombre.setLayoutParams(params3Weight);
				editTextNombre.setLayoutParams(params7Weight);
				layoutNombre.addView(TextNombre);
				layoutNombre.addView(editTextNombre);
				
				LinearLayout layoutDireccion = new LinearLayout(ActivityPreferenciasPrinters.this);
				layoutDireccion.setLayoutParams(layoutForInner);
				layoutDireccion.setOrientation(LinearLayout.HORIZONTAL);
				TextDireccion.setLayoutParams(params3Weight);
				editTextDireccion.setLayoutParams(params7Weight);
				layoutDireccion.addView(TextDireccion);
				layoutDireccion.addView(editTextDireccion);
				
				layoutTextos.addView(layoutEtiqueta);
				layoutTextos.addView(layoutNombre);
				layoutTextos.addView(layoutDireccion);
				
				
		       
				AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityPreferenciasPrinters.this);
				
				dialog.setTitle("NUEVA IMPRESORA");
				dialog.setView(layoutTextos);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				    	String textoEtiqueta = editTextEtiqueta.getText().toString();
				    	String textoNombre = editTextNombre.getText().toString();
				    	String textoDireccion = editTextDireccion.getText().toString();  
				    	MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
						SQLiteDatabase db = myHelper.getWritableDatabase();
						myHelper.insertaImpresora(db, textoEtiqueta, textoNombre, textoDireccion);
						listPrinters = myHelper.recuperaImpresoras(db);
						db.close();
						myHelper.close();
				        listviewPrinters.setAdapter(new AdapterImpresoras(ActivityPreferenciasPrinters.this, 
				        		listPrinters));
						//listviewServidores.getAdapter().notifyAll();
						dialog.cancel();

				    }
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int id) {
				        dialog.cancel();
				    }
				});
				dialog.create();
				dialog.show();
			}
		});
        
	}


}
