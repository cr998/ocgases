package es.paumancar.ocgases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.honeywell.decodemanager.DecodeManager;
import com.honeywell.decodemanager.barcode.DecodeResult;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

@SuppressLint("NewApi")
public class ActivityCodigosBarras extends FragmentActivity {
	

	private final int ID_SCANSETTING = 0x12;
	private final int ID_CLEAR_SCREEN = 0x13;

	private DecodeManager mDecodeManager = null;
	
    private final int SCANKEY = 0x94;
	private final int SCANTIMEOUT = 5000;
	long mScanAccount = 0;
	private boolean mbKeyDown = true;
	
	private String cantidadOrd;
	private String cantidadRet;
	private String descripcion;
	private String cuentaLlenos;
	private String cuentaVacios;
	
	private boolean pidecodigobarras = true;
	
	SectionsPagerAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;
	
	private Fragment fragmentActive;
	private int posicionFragmentActive = 0;
	
	List<String[]> codigosLlenos;
	List<String[]> codigosVacios;
	String m_inoutline_id;
	String compra_venta;
	String documento;
	
	private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    
    SharedPreferences sharedPref;
    boolean escanearConCCD = false;
    
    Button botonEscanea;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.codigos_compras_ventas_layout);
        
        botonEscanea = (Button) findViewById(R.id.buttonCodigosBarrasEscanea);
     
        Intent intentReceived = getIntent();
        String[] valorCabecera = intentReceived.getStringArrayExtra("valoresCabecera");
        documento = intentReceived.getStringExtra("valorDocumento");
        compra_venta = intentReceived.getStringExtra("compra_venta");
        m_inoutline_id = valorCabecera[1];
        cantidadOrd = valorCabecera[3];
        cantidadRet = valorCabecera[4];
        if (compra_venta.equals("venta")){
        	pidecodigobarras = valorCabecera[10].equals("1000000")?true:false;
        	descripcion = valorCabecera[9];
        	cuentaLlenos = "ENTREGADOS: ";
        	cuentaVacios = "RECOGIDOS: ";
        } else {
        	descripcion = valorCabecera[6];
        	cuentaLlenos = "RECOGIDOS: ";
        	cuentaVacios = "ENTREGADOS: ";
        }
        
     
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        
//        String[] valorCabecera  = {
//       		  "c_order_id",
//       		  "c_orderline_id",
//       		  "m_product_id",
//       		  "qtyordered",
//       		  "qtyreturn",
//       		  "value",
//				  "name",
//       		  "LADFADF3,L33ADFA,L35435,LJDAFDA3,L34JKDFASI",
//       		  "VDJAS8F,V9JFUE0F,V9SJREI9,V93JFAS"
//	        };
        
        

        MySQLiteHelper myHelper = new MySQLiteHelper(getApplicationContext());
        SQLiteDatabase db = myHelper.getWritableDatabase();
        codigosLlenos = myHelper.recuperaCodigosLlenos(db, m_inoutline_id, compra_venta);
        codigosVacios = myHelper.recuperaCodigosVacios(db, m_inoutline_id, compra_venta);
        db.close();
        myHelper.close();
        

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());
		
		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pagerCodigosCompras);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		fragmentActive = mSectionsPagerAdapter.getFragment(0);
		
		mViewPager.addOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				posicionFragmentActive = position;
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {	
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		initializeUI();
		
		
    }
    

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		switch (keyCode) {
		case KeyEvent.KEYCODE_ENTER:
			if (escanearConCCD){
				try {
					if (mbKeyDown) {
						DoScan();
						mbKeyDown = false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return true;
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			return true;			
        case KeyEvent.KEYCODE_UNKNOWN:	
        	if(event.getScanCode() == SCANKEY || event.getScanCode() == 87 || event.getScanCode() == 88) {
        		if (escanearConCCD){
	        		try {
						if (mbKeyDown) {
							DoScan();
							mbKeyDown = false;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
        		}
        	}
        	return true;
		default:
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		int aus;
		aus =4;
		switch (keyCode) {
		case KeyEvent.KEYCODE_ENTER:
			if (escanearConCCD){
				try {
					mbKeyDown = true;
					if (escanearConCCD){
						cancelScan();
					} else {
						//iniciaCamara();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return true;
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			return true;
			
        case KeyEvent.KEYCODE_UNKNOWN:	
        	if(event.getScanCode() == SCANKEY  || event.getScanCode() == 87 || event.getScanCode() == 88) {
        		if (escanearConCCD){
	        		try {
	    				mbKeyDown = true;
	    				cancelScan();
	    			} catch (Exception e) {
	    				e.printStackTrace();
	    			}
        		} else {
					iniciaCamara();
				}
        	}
        	return true;
		default:
			return super.onKeyUp(keyCode, event);
		}
	}
    
    
	@Override
	protected void onResume() {
		super.onResume();
        String escanerDefecto = sharedPref.getString(TagsPreferencias.ESCANER, "");
        escanearConCCD = escanerDefecto.equals(TagsPreferencias.ESCANER_VALOR_LECTOR)?true:false;
        if (mDecodeManager != null) {
			Log.e(TAG, "Existe DecodeManager onResume");
		}
		if (escanearConCCD){
			if (mDecodeManager == null) {
				mDecodeManager = new DecodeManager(this,ScanResultHandler);
			}
			
			SoundManager.getInstance();
			SoundManager.initSounds(getBaseContext());
			SoundManager.loadSounds();
		} else {
			if (mDecodeManager != null) {
				try {
					mDecodeManager.release();
					mDecodeManager = null;
				} catch (IOException e) {
					Log.e(TAG, "Error al cerrar DecodeManager onResume con Camara");
					e.printStackTrace();
				}
			}
		}
	}

	

	@Override
	protected void onPause() {
		super.onPause();
		if (mDecodeManager != null) {
			try {
				mDecodeManager.release();
				mDecodeManager = null;
			} catch (IOException e) {
				Log.e(TAG, "Error al cerrar DecodeManager onPause");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (escanearConCCD){
			SoundManager.cleanup();
		}
		if (mDecodeManager != null) {
			try {
				mDecodeManager.release();
				mDecodeManager = null;
			} catch (IOException e) {
				Log.e(TAG, "Error al cerrar DecodeManager onDestroy");
				e.printStackTrace();
			}
		}
	}
    
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(ActivityBarcodeCapture.BarcodeObject);
                    //statusMessage.setText(R.string.barcode_success);
                    //barcodeValue.setText(barcode.displayValue);
                    String codigoDevuelto = barcode.displayValue;
                    fragmentActive = mSectionsPagerAdapter.getFragment(posicionFragmentActive);
                    switch (posicionFragmentActive) {
					case 0:
	                    ((FragmentCodigosLlenos) fragmentActive).insertaCodigoEnLista("1", codigoDevuelto);						
						break;
					case 1:
	                    ((FragmentCodigosVacios) fragmentActive).insertaCodigoEnLista("1", codigoDevuelto);						
						break;
					default:
						break;
					}

                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
                } else {
                    //statusMessage.setText(R.string.barcode_failure);
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                //statusMessage.setText(String.format(getString(R.string.barcode_error),
                //        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		if (escanearConCCD){
			menu.add(0, ID_SCANSETTING, 0, R.string.SymbologySettingMenu);
		}
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case ID_SCANSETTING: {
			mDecodeManager.getSymConfigActivityOpeartor().start();
			 return true;
		}

		default:
			return false;
		}
	}
    
    
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private Map<Integer, String> mFragmentTags;
        private FragmentManager mFragmentManager;

		public SectionsPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
	        mFragmentManager = fragmentManager;
	        mFragmentTags = new HashMap<Integer, String>();
		}

		@Override
		public Fragment getItem(int position) {
		    switch (position){
		    case 0:
		    FragmentCodigosLlenos FragmentLlenos = 
		    	new FragmentCodigosLlenos(codigosLlenos, m_inoutline_id, documento, compra_venta, 
		    			false, cantidadOrd, descripcion, cuentaLlenos);
		    return FragmentLlenos;
		    
		    case 1:
		    FragmentCodigosVacios FragmentVacios = 
		    	new FragmentCodigosVacios(codigosVacios, m_inoutline_id, documento, compra_venta, 
		    			false, cantidadRet, descripcion, cuentaVacios);
		    return FragmentVacios;
	    
		    default:
		    //this page does not exists
		    return null;
		    }
			//return PlaceholderFragment.newInstance(position + 1);
		}

		@Override
		public int getCount() {
			if (pidecodigobarras) {
				return 2;
			} else {
				return 1;
			}
//			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			}
			return null;
		}
		
	    @Override
	    public Object instantiateItem(ViewGroup container, int position) {
	        Object obj = super.instantiateItem(container, position);
	        if (obj instanceof Fragment) {
	            // record the fragment tag here.
	            Fragment f = (Fragment) obj;
	            String tag = f.getTag();
	            mFragmentTags.put(position, tag);
	        }
	        return obj;
	    }

	    public Fragment getFragment(int position) {
	        String tag = mFragmentTags.get(position);
	        if (tag == null)
	            return null;
	        return mFragmentManager.findFragmentByTag(tag);
	    }
	}



	private void initializeUI() {
		//final Button button = (Button) findViewById(R.id.scanbutton);
		//mDecodeResultEdit = (EditText) findViewById(R.id.edittext_scanresult);
		botonEscanea.setOnTouchListener(new Button.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				final int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					//button.setBackgroundResource(R.drawable.android_pressed);
					if (escanearConCCD){
						try {
							DoScan();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					break;
				case MotionEvent.ACTION_UP:
					if (escanearConCCD){
						try {
							//button.setBackgroundResource(R.drawable.android_normal);
							cancelScan();
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						iniciaCamara();
					}
					break;
				}
				return true;
			}
		});
	}

	private void DoScan() throws Exception {
		try {
			mDecodeManager.doDecode(SCANTIMEOUT);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	private void iniciaCamara(){
		Intent intent = new Intent(getApplicationContext(), ActivityBarcodeCapture.class);
        intent.putExtra(ActivityBarcodeCapture.AutoFocus, true);
        intent.putExtra(ActivityBarcodeCapture.UseFlash, false);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
	}

	private Handler ScanResultHandler = new Handler() {
		public void handleMessage(Message msg) {
			fragmentActive = mSectionsPagerAdapter.getFragment(posicionFragmentActive);
			switch (msg.what) {
			case DecodeManager.MESSAGE_DECODER_COMPLETE:
				mScanAccount++;
				DecodeResult decodeResult = (DecodeResult) msg.obj;
				SoundManager.playSound(1, 1);
		
//				byte codeid = decodeResult.codeId;
//				byte aimid = decodeResult.aimId;
//				int iLength = decodeResult.length;
				switch (posicionFragmentActive) {
				case 0:
					((FragmentCodigosLlenos) fragmentActive).editTextAddCodeBar.setText(decodeResult.barcodeData);
					((FragmentCodigosLlenos) fragmentActive).botonAddCodeBar.performClick();
					break;
				
				case 1:
					((FragmentCodigosVacios) fragmentActive).editTextAddCodeBar.setText(decodeResult.barcodeData);
					((FragmentCodigosVacios) fragmentActive).botonAddCodeBar.performClick();
					break;

				default:
					break;
				}
				 
				 break;

			case DecodeManager.MESSAGE_DECODER_FAIL: {
				SoundManager.playSound(2, 1);
				//mDecodeResultEdit.setText("Decode Result::Scan fail");

			}
			break;
			case DecodeManager.MESSAGE_DECODER_READY:
			{
				ArrayList<java.lang.Integer> arry =  mDecodeManager.getSymConfigActivityOpeartor().getAllSymbologyId();
				boolean b = arry.isEmpty();
			}
			break;
			default:
				super.handleMessage(msg);
				break;
			}
		}
	};
	
	private void cancelScan() throws Exception {
		mDecodeManager.cancelDecode();
	}
    
}
