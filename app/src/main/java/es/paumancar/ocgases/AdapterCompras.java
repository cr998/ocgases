package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterCompras  extends BaseAdapter {

    Context context;
    //Intent intentNext;
    Class<?> NextClass;
    int filtroId;
    int nextFiltroId;
    List<String[]> data;
    String[] cabecerasColumnas;
    private static LayoutInflater inflater = null;

    public AdapterCompras(Context context, List<String[]> data, String[] cabecerasColumnas, 
    		Class<?> NextClass, int filtroId, int nextFiltroId) {
        this.context = context;
        this.NextClass = NextClass;
        this.filtroId = filtroId;
        this.nextFiltroId = nextFiltroId;
        if (data != null) {
        	this.data = data;
        } else {
        	this.data = new ArrayList<String[]>();
        }
        this.cabecerasColumnas = cabecerasColumnas;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_compras, null);
        TextView textHeader = (TextView) vi.findViewById(R.id.rowTextHeaderCompras);
        TextView textFecha = (TextView) vi.findViewById(R.id.rowTextFechaCompras);
        TextView textDetail = (TextView) vi.findViewById(R.id.rowTextDetailCompras);
        final int positionAux = position;
    	
        vi.setOnClickListener(new OnClickListener() {
        	String[] cabeceraProv = data.get(positionAux);
        	int sigFiltroId = nextFiltroId;
        	String valorFiltro =(sigFiltroId < 0) ? "" : cabeceraProv[sigFiltroId];
        	Class<?> sigClass = NextClass;
			@Override
			public void onClick(View v) {
				Intent intentNext = new Intent(context.getApplicationContext(), sigClass);
				intentNext.putExtra("valoresColumnas", cabecerasColumnas);
				intentNext.putExtra("valoresCabecera", cabeceraProv);
				intentNext.putExtra("valorFiltro", valorFiltro);
				intentNext.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.getApplicationContext().startActivity(intentNext);
			}
		});
        
        textHeader.setText(data.get(position)[indiceArray(cabecerasColumnas, "documentno")]);
        textFecha.setText(data.get(position)[indiceArray(cabecerasColumnas, "dateordered")].substring(0, 10));
        textDetail.setText(data.get(position)[indiceArray(cabecerasColumnas, "nombreproveedor")]);
        textHeader.setTextColor(Color.BLACK);
        textFecha.setTextColor(Color.BLACK);
        textDetail.setTextColor(Color.BLACK);
        return vi;
    }
    
	private int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}
}
