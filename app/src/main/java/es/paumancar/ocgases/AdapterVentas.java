package es.paumancar.ocgases;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterVentas  extends BaseAdapter {

	int posicionActiva = -1;
    Context context;
    Class<?> NextClass;
    int filtroId;
    int nextFiltroId;
    List<String[]> data;
    String[] cabecerasColumnas;
    TextView textoDescripcion;
    
    private static LayoutInflater inflater = null;

    public AdapterVentas(Context context, List<String[]> data, String[] cabecerasColumnas, 
    		Class<?> NextClass, int filtroId, int nextFiltroId, TextView textoDescipcion) {
        // TODO Auto-generated constructor stub) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.NextClass = NextClass;
        this.filtroId = filtroId;
        this.nextFiltroId = nextFiltroId;
        this.cabecerasColumnas = cabecerasColumnas;
        this.textoDescripcion = textoDescipcion;
        if (data != null) {
        	this.data = data;
        } else {
        	this.data = new ArrayList<String[]>();
        }
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.list_row_ventas, null);
        TextView textHeader = (TextView) vi.findViewById(R.id.rowTextHeaderVentas);
        TextView textDetail = (TextView) vi.findViewById(R.id.rowTextDetailVentas);

        final int positionAux = position;
        
        if (positionAux == posicionActiva) {
        	vi.setBackgroundColor(Color.CYAN); 
        } else {
        	vi.setBackgroundColor(Color.TRANSPARENT);
        }
    	
        vi.setOnClickListener(new OnClickListener() {
        	String[] cabeceraProv = data.get(positionAux);
        	int sigFiltroId = nextFiltroId;
        	String valorFiltro =(sigFiltroId < 0) ? "" : cabeceraProv[sigFiltroId];
        	Class<?> sigClass = NextClass;
			@Override
			public void onClick(View v) {
				v.setBackgroundColor(Color.CYAN);
				if (positionAux == posicionActiva){
					Intent intentNext = new Intent(context.getApplicationContext(), sigClass);
					intentNext.putExtra("valoresColumnas", cabecerasColumnas);
					intentNext.putExtra("valoresCabecera", cabeceraProv);
					intentNext.putExtra("valorFiltro", valorFiltro);
					intentNext.putExtra("valorDocumento", 
							data.get(positionAux)[indiceArray(cabecerasColumnas, "documentno")]);
					intentNext.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.getApplicationContext().startActivity(intentNext);
				} else {
					posicionActiva = positionAux;
					textoDescripcion.setText(data.get(positionAux)[indiceArray(cabecerasColumnas, "direccion")]
							+ ". "
							+ data.get(positionAux)[indiceArray(cabecerasColumnas, "ciudad")]
							+ "\n"
							+ data.get(positionAux)[indiceArray(cabecerasColumnas, "ayuda_entrega")] 
							+ "\n"
							+ "Lat: " +data.get(positionAux)[indiceArray(cabecerasColumnas, "latitud")]
							+ ", "
							+ "Long: " +data.get(positionAux)[indiceArray(cabecerasColumnas, "longitud")]
							);
					notifyDataSetChanged();
				}
			}
		});
        
        
        textHeader.setText(data.get(position)[indiceArray(cabecerasColumnas, "documentno")]);
        textDetail.setText(data.get(position)[indiceArray(cabecerasColumnas, "nombrecomercial")] );
        textHeader.setTextColor(Color.BLACK);
        textDetail.setTextColor(Color.BLACK);
        return vi;
    }
	private int indiceArray(String[] stringCabeceras, String valorCabecera){
		int indice = 0;
		for (int i = 0; i < stringCabeceras.length; i++) {
			if (stringCabeceras[i].equals(valorCabecera)){
				indice = i;
				break;
			}
		}
		return indice;
	}
}